################################################################################
# Automatically-generated file. Do not edit!
################################################################################

-include ../makefile.local

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS_QUOTED += \
"../Sources/CAN2Gui.c" \
"../Sources/demo_config.c" \
"../Sources/main.c" \

C_SRCS += \
../Sources/CAN2Gui.c \
../Sources/demo_config.c \
../Sources/main.c \

OBJS_OS_FORMAT += \
./Sources/CAN2Gui.o \
./Sources/demo_config.o \
./Sources/main.o \

C_DEPS_QUOTED += \
"./Sources/CAN2Gui.d" \
"./Sources/demo_config.d" \
"./Sources/main.d" \

OBJS += \
./Sources/CAN2Gui.o \
./Sources/demo_config.o \
./Sources/main.o \

OBJS_QUOTED += \
"./Sources/CAN2Gui.o" \
"./Sources/demo_config.o" \
"./Sources/main.o" \

C_DEPS += \
./Sources/CAN2Gui.d \
./Sources/demo_config.d \
./Sources/main.d \


# Each subdirectory must supply rules for building sources it contributes
Sources/CAN2Gui.o: ../Sources/CAN2Gui.c
	@echo 'Building file: $<'
	@echo 'Executing target #32 $<'
	@echo 'Invoking: Standard S32DS C Compiler'
	arm-none-eabi-gcc "@Sources/CAN2Gui.args" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "Sources/CAN2Gui.o" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Sources/demo_config.o: ../Sources/demo_config.c
	@echo 'Building file: $<'
	@echo 'Executing target #33 $<'
	@echo 'Invoking: Standard S32DS C Compiler'
	arm-none-eabi-gcc "@Sources/demo_config.args" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "Sources/demo_config.o" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Sources/main.o: ../Sources/main.c
	@echo 'Building file: $<'
	@echo 'Executing target #34 $<'
	@echo 'Invoking: Standard S32DS C Compiler'
	arm-none-eabi-gcc "@Sources/main.args" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "Sources/main.o" "$<"
	@echo 'Finished building: $<'
	@echo ' '


