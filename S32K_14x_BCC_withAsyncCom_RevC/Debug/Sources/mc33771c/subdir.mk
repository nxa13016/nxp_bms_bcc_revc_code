################################################################################
# Automatically-generated file. Do not edit!
################################################################################

-include ../../makefile.local

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS_QUOTED += \
"../Sources/mc33771c/mc33771c.c" \
"../Sources/mc33771c/mc33771c_communication.c" \
"../Sources/mc33771c/mc33771c_peripheries.c" \

C_SRCS += \
../Sources/mc33771c/mc33771c.c \
../Sources/mc33771c/mc33771c_communication.c \
../Sources/mc33771c/mc33771c_peripheries.c \

OBJS_OS_FORMAT += \
./Sources/mc33771c/mc33771c.o \
./Sources/mc33771c/mc33771c_communication.o \
./Sources/mc33771c/mc33771c_peripheries.o \

C_DEPS_QUOTED += \
"./Sources/mc33771c/mc33771c.d" \
"./Sources/mc33771c/mc33771c_communication.d" \
"./Sources/mc33771c/mc33771c_peripheries.d" \

OBJS += \
./Sources/mc33771c/mc33771c.o \
./Sources/mc33771c/mc33771c_communication.o \
./Sources/mc33771c/mc33771c_peripheries.o \

OBJS_QUOTED += \
"./Sources/mc33771c/mc33771c.o" \
"./Sources/mc33771c/mc33771c_communication.o" \
"./Sources/mc33771c/mc33771c_peripheries.o" \

C_DEPS += \
./Sources/mc33771c/mc33771c.d \
./Sources/mc33771c/mc33771c_communication.d \
./Sources/mc33771c/mc33771c_peripheries.d \


# Each subdirectory must supply rules for building sources it contributes
Sources/mc33771c/mc33771c.o: ../Sources/mc33771c/mc33771c.c
	@echo 'Building file: $<'
	@echo 'Executing target #35 $<'
	@echo 'Invoking: Standard S32DS C Compiler'
	arm-none-eabi-gcc "@Sources/mc33771c/mc33771c.args" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "Sources/mc33771c/mc33771c.o" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Sources/mc33771c/mc33771c_communication.o: ../Sources/mc33771c/mc33771c_communication.c
	@echo 'Building file: $<'
	@echo 'Executing target #36 $<'
	@echo 'Invoking: Standard S32DS C Compiler'
	arm-none-eabi-gcc "@Sources/mc33771c/mc33771c_communication.args" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "Sources/mc33771c/mc33771c_communication.o" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Sources/mc33771c/mc33771c_peripheries.o: ../Sources/mc33771c/mc33771c_peripheries.c
	@echo 'Building file: $<'
	@echo 'Executing target #37 $<'
	@echo 'Invoking: Standard S32DS C Compiler'
	arm-none-eabi-gcc "@Sources/mc33771c/mc33771c_peripheries.args" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "Sources/mc33771c/mc33771c_peripheries.o" "$<"
	@echo 'Finished building: $<'
	@echo ' '


