/*
 * CAN2Gui.h
 *
 *  Created on: 2018��11��28��
 *      Author: nxa13016
 */

#ifndef CAN2GUI_H_
#define CAN2GUI_H_

#include "demo_config.h"

void CAN_SendData2GUI(uint8_t Daughter);
void CAN_SendStatus2GUI(uint8_t u8Daughter);
void CAN_SendSM2GUI(uint8_t u8Daughter, uint32_t u32DiagData, uint16_t u16SMitemNum);
void CAN_SendCtrlReg2GUI(uint8_t u8Daughter, uint16_t StatusPtr[][112], uint8_t *StatusAddr);
void CAN_SendSTSReg2GUI(uint8_t u8Daughter, uint16_t StatusPtr[][112], uint8_t *StatusAddr);
void CAN_SendError2GUI(uint8_t u8Daughter, bcc_status_t res);
status_t CAN_RevCmdProcess(uint8_t* pu8Command, uint8_t* pu8Data, uint16_t* Subpu16Data);

#endif /* CAN2GUI_H_ */
