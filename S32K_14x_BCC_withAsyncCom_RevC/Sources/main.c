/* ###################################################################
**     Filename    : main.c
**     Processor   : S32K1xx
**     Abstract    :
**         Main module.
**         This module contains user's application code.
**     Settings    :
**     Contents    :
**         No public methods
**
** ###################################################################*/
/*!
** @file main.c
** @version 01.00
** @brief
**         Main module.
**         This module contains user's application code.
*/
/*!
**  @addtogroup main_module main module documentation
**  @{
*/
/* MODULE main */


/* Including necessary module. Cpu.h contains other modules needed for compiling.*/
#include "Cpu.h"

  volatile int exit_code = 0;

/* User includes (#include below this line is not maintained by Processor Expert) */
#include "mc33771c/mc33771c.h"
#include "mc33771c/mc33771c_peripheries.h"
#include "demo_config.h"
#include "CAN2Gui.h"
#include "sbc_uja1169_driver.h"
#include "lpit1.h"

/*!
  \brief The main function for the project.
  \details The startup initialization sequence is the following:
 * - startup asm routine
 * - main()
*/



/*******************************************************************************
* global variables definition
******************************************************************************/
static TYPE_BCCmode BCCmode;   /*MC33771C task mode*/
static bcc_drv_config_t drvConfig; /*Store MC33771C Driver configuration*/
static uint8_t BCC_TASK_FLAG = 0, BCC_TASK_CNT = 0, CAN_TASK_FLAG = 0, UJA_WD_FLAG = 0; /*Flag of tasks in main-loop*/
uint16_t BCC_ALLREG[BCC_DEVICECNT][112]; /*registers array storing all registers content*/
/*Status related registers address*/
const uint8_t Status_RegMap[15] ={
		BCC_REG_CELL_OV_FLT_ADDR, BCC_REG_CELL_UV_FLT_ADDR, BCC_REG_CB_OPEN_FLT_ADDR,
		BCC_REG_CB_SHORT_FLT_ADDR, BCC_REG_CB_DRV_STS_ADDR, BCC_REG_GPIO_STS_ADDR,
		BCC_REG_AN_OT_UT_FLT_ADDR, BCC_REG_GPIO_SHORT_ADDR, BCC_REG_I_STATUS_ADDR,
		BCC_REG_COM_STATUS_ADDR, BCC_REG_FAULT1_STATUS_ADDR, BCC_REG_FAULT2_STATUS_ADDR,
		BCC_REG_FAULT3_STATUS_ADDR, BCC_REG_MEAS_ISENSE2_ADDR, BCC_REG_SILICON_REV_ADDR,
};
/*Configuration related registers address*/
const uint8_t Config_RegMap[31] ={
		BCC_REG_INIT_ADDR, BCC_REG_SYS_CFG_GLOBAL_ADDR, BCC_REG_SYS_CFG1_ADDR,
		BCC_REG_SYS_CFG2_ADDR, BCC_REG_SYS_DIAG_ADDR, BCC_REG_ADC_CFG_ADDR,
		BCC_REG_OV_UV_EN_ADDR, BCC_REG_GPIO_CFG1_ADDR, BCC_REG_GPIO_CFG2_ADDR,
		BCC_REG_FAULT_MASK1_ADDR, BCC_REG_FAULT_MASK2_ADDR, BCC_REG_FAULT_MASK3_ADDR,
		BCC_REG_WAKEUP_MASK1_ADDR, BCC_REG_WAKEUP_MASK2_ADDR, BCC_REG_WAKEUP_MASK3_ADDR,
		BCC_REG_TPL_CFG_ADDR,BCC_REG_ADC2_OFFSET_COMP_ADDR, BCC_REG_CB1_CFG_ADDR, BCC_REG_CB2_CFG_ADDR,
		BCC_REG_CB3_CFG_ADDR, BCC_REG_CB4_CFG_ADDR, BCC_REG_CB5_CFG_ADDR,
		BCC_REG_CB6_CFG_ADDR, BCC_REG_CB7_CFG_ADDR, BCC_REG_CB8_CFG_ADDR,
		BCC_REG_CB9_CFG_ADDR, BCC_REG_CB10_CFG_ADDR, BCC_REG_CB11_CFG_ADDR,
		BCC_REG_CB12_CFG_ADDR, BCC_REG_CB13_CFG_ADDR, BCC_REG_CB14_CFG_ADDR,

};

static uint8_t SMitem = 0;   /*safety mechanism which is to be performed */
static bcc_adc1x_res_t SM7result[BCC_DEVICECNT];
static bcc_vpwr_res_t  SM45result[BCC_DEVICECNT];
static bcc_diag_ov_uv_res_t SM1result[BCC_DEVICECNT];
static bcc_diag_ctx_open_res_t SM2result[BCC_DEVICECNT];
static bcc_diag_cell_volt_res_t SM3result[BCC_DEVICECNT];
static bcc_diag_cell_leak_res_t SM4result[BCC_DEVICECNT];
static bcc_diag_ot_ut_res_t SM5result[BCC_DEVICECNT];
static uint16_t SM6result[BCC_DEVICECNT];
static bcc_diag_cbx_open_res_t SM40result[BCC_DEVICECNT];

/*******************************************************************************
* functions declaration
******************************************************************************/
void BCC_TASK(void);
void BCC_STATEPACK_PREPARE(void);
void CAN2Gui_TASK(void);
void Gui2CAN_TASK(void);
void LPIT_ISR(void);

/*******************************************************************************
* Initial BCC configuration
******************************************************************************/

/* Initial configuration of Battery Cell Controller devices. */
static const uint16_t BCC_INIT_CONF[1][BCC_INIT_CONF_REG_CNT] = {
  {
	  BCC_CONF1_SYS_CFG1_VALUE,
	  BCC_CONF1_SYS_CFG2_VALUE,
	  BCC_CONF1_ADC_CFG_VALUE,
	  BCC_CONF1_ADC2_OFFSET_COMP_VALUE,
	  BCC_CONF1_OV_UV_EN_VALUE,
	  BCC_CONF1_CELL_TPL_CFG_VALUE,
	  BCC_CONF1_GPIO_CFG1_VALUE,
	  BCC_CONF1_GPIO_CFG2_VALUE,
	  BCC_CONF1_FAULT_MASK1_VALUE,
	  BCC_CONF1_FAULT_MASK2_VALUE,
	  BCC_CONF1_FAULT_MASK3_VALUE,
	  BCC_CONF1_WAKEUP_MASK1_VALUE,
	  BCC_CONF1_WAKEUP_MASK2_VALUE,
	  BCC_CONF1_WAKEUP_MASK3_VALUE,
	  BCC_CONF1_TH_ALL_CT_VALUE,
	  BCC_CONF1_TH_CT14_VALUE,
	  BCC_CONF1_TH_CT13_VALUE,
	  BCC_CONF1_TH_CT12_VALUE,
	  BCC_CONF1_TH_CT11_VALUE,
	  BCC_CONF1_TH_CT10_VALUE,
	  BCC_CONF1_TH_CT9_VALUE,
	  BCC_CONF1_TH_CT8_VALUE,
	  BCC_CONF1_TH_CT7_VALUE,
	  BCC_CONF1_TH_CT6_VALUE,
	  BCC_CONF1_TH_CT5_VALUE,
	  BCC_CONF1_TH_CT4_VALUE,
	  BCC_CONF1_TH_CT3_VALUE,
	  BCC_CONF1_TH_CT2_VALUE,
	  BCC_CONF1_TH_CT1_VALUE,
	  BCC_CONF1_TH_AN6_OT_VALUE,
	  BCC_CONF1_TH_AN5_OT_VALUE,
	  BCC_CONF1_TH_AN4_OT_VALUE,
	  BCC_CONF1_TH_AN3_OT_VALUE,
	  BCC_CONF1_TH_AN2_OT_VALUE,
	  BCC_CONF1_TH_AN1_OT_VALUE,
	  BCC_CONF1_TH_AN0_OT_VALUE,
	  BCC_CONF1_TH_AN6_UT_VALUE,
	  BCC_CONF1_TH_AN5_UT_VALUE,
	  BCC_CONF1_TH_AN4_UT_VALUE,
	  BCC_CONF1_TH_AN3_UT_VALUE,
	  BCC_CONF1_TH_AN2_UT_VALUE,
	  BCC_CONF1_TH_AN1_UT_VALUE,
	  BCC_CONF1_TH_AN0_UT_VALUE,
	  BCC_CONF1_TH_ISENSE_OC_VALUE,
	  BCC_CONF1_TH_COULOMB_CNT_MSB_VALUE,
	  BCC_CONF1_TH_COULOMB_CNT_LSB_VALUE,
  }
};

int main(void)
{
  /* Write your local variable definition here */
//	status_t error;
	bcc_status_t Error;
    uint16_t u16RegVal;
    sbc_wtdog_status_t sbc_wd_status;
    sbc_status_group_t sbc_status;
    uint8_t SBC_CAN[8];

    /*identified BCC CID whose data is transmitted by CAN */
//    static can_bcc_cid = 1;
  /*** Processor Expert internal initialization. DON'T REMOVE THIS CODE!!! ***/
  #ifdef PEX_RTOS_INIT
    PEX_RTOS_INIT();                   /* Initialization of the selected RTOS. Macro is defined by the RTOS component. */
  #endif
  /*** End of Processor Expert internal initialization.                    ***/

  /* Write your code here */
    /*CLC,PORT,DMA initialization*/
  CLOCK_SYS_Init(g_clockManConfigsArr, CLOCK_MANAGER_CONFIG_CNT,g_clockManCallbacksArr, CLOCK_MANAGER_CALLBACK_CNT);
  CLOCK_SYS_UpdateConfiguration(0U,CLOCK_MANAGER_POLICY_FORCIBLE);
  PINS_DRV_Init(NUM_OF_CONFIGURED_PINS, g_pin_mux_InitConfigArr);
  EDMA_DRV_Init(&dmaController1_State, &dmaController1_InitConfig0, edmaChnStateArray, edmaChnConfigArray, EDMA_CONFIGURED_CHANNELS_COUNT);
  FlexCANInit();
  /*SPI for uja1169 initialization*/
  LPSPI_DRV_MasterInit(LPSPICOM3,&lpspiCom3State,&lpspiCom3_MasterConfig0);
  /*SBC initialization*/
  SBC_Init(&sbc_uja11691_InitConfig0, LPSPICOM3);

  SBC_GetAllStatus(&sbc_status);
  SBC_CAN[0] = sbc_status.mainS.nms;
  SBC_CAN[1] = sbc_status.mainS.otws;
  SBC_CAN[2] = sbc_status.mainS.rss;
  SBC_CAN[3] = sbc_status.wtdog.fnms;
  SBC_CAN[4] = sbc_status.wtdog.sdms;
  SBC_CAN[5] = sbc_status.wtdog.wds;
  SendCANData(0x19900000, SBC_CAN);


#if 1
  /* Initialize LPIT instance 0
   *  -   Reset and enable peripheral
   */
  LPIT_DRV_Init(INST_LPIT1, &lpit1_InitConfig);
  /* Initialize LPIT channel 0 and configure it as a periodic counter
   * which is used to generate an interrupt every second.
   */
  LPIT_DRV_InitChannel(INST_LPIT1, 0, &lpit1_ChnConfig0);
  LPIT_DRV_InitChannel(INST_LPIT1, 1, &lpit1_ChnConfig1);
  /* Install LPIT_ISR as LPIT interrupt handler */
  INT_SYS_InstallHandler(LPIT0_Ch0_IRQn, &LPIT_ISR, (isr_t *)0);
  /* Start LPIT0 channel 0 counter */
  LPIT_DRV_StartTimerChannels(INST_LPIT1, (1 << 0));
#endif
  /*UJA Init*/
//  UJA_ConfigureLPSPI();
//  UJA_Setting();
//  UJA_WD_WindowMode();
//  UJA_enterNomalMode();

  /*Set the priority of the following interrupt*/
  INT_SYS_SetPriority(LPIT0_Ch0_IRQn, 3); /*pit timer*/
  INT_SYS_SetPriority(DMA0_IRQn, 5);      /*DMA0 used for Master SPI TX transfer*/
  INT_SYS_SetPriority(DMA1_IRQn, 7);      /*DMA1 used for Slave SPI RX transfer*/
  INT_SYS_SetPriority(DMA2_IRQn, 6);      /*DMA2 used for Master SPI RX transfer*/
  INT_SYS_SetPriority(DMA3_IRQn, 8);      /*DMA3 used for Slave SPI TX transfer*/
  INT_SYS_SetPriority(CAN0_ORed_0_15_MB_IRQn, 3);  /*CAN buffer interrupt*/

  /*BCC parameter init*/
  fillBccDriverConfiguration(&drvConfig, BCC_DEVICECNT);
  BCC_ConfigureLPSPI();
  BCCmode = BCC_INIT;

#if 1
  while(1)
  {
	  /*feed SBC watchdog*/
	  if(UJA_WD_FLAG)
	  {
		  UJA_WD_FLAG = 0;
          SBC_GetWatchdogStatus(&sbc_wd_status);
          /*watchdog is in second half of the nominal period, feed WD*/
          if(sbc_wd_status.wds == SBC_UJA_WTDOG_STAT_WDS_SEH)
          {
		      SBC_FeedWatchdog();
          }
	  }
	  /*execute BCC task every 10ms*/
	  if(BCC_TASK_FLAG)
	  {
		  BCC_TASK();
		  BCC_TASK_FLAG = 0;
	  }
	  /*execute CAN communication with Gui task every 50ms*/
	  if(CAN_TASK_FLAG)
	  {
		  /*CAN send BCC data to Gui and Gui command to MCU*/
		  CAN2Gui_TASK();
		  CAN_TASK_FLAG = 0;
	  }
  }
#endif

  /*** Don't write any code pass this line, or it will be deleted during code generation. ***/
  /*** RTOS startup code. Macro PEX_RTOS_START is defined by the RTOS component. DON'T MODIFY THIS CODE!!! ***/
  #ifdef PEX_RTOS_START
    PEX_RTOS_START();                  /* Startup of the selected RTOS. Macro is defined by the RTOS component. */
  #endif
  /*** End of RTOS startup code.  ***/
  /*** Processor Expert end of main routine. DON'T MODIFY THIS CODE!!! ***/
  for(;;) {
    if(exit_code != 0) {
      break;
    }
  }
  return exit_code;
  /*** Processor Expert end of main routine. DON'T WRITE CODE BELOW!!! ***/
} /*** End of main routine. DO NOT MODIFY THIS TEXT!!! ***/

/*******************************************************************************
* MC33771C driver task functions
******************************************************************************/

void BCC_TASK(void)
{
	bcc_status_t Error;
    uint8_t CidNodes = 1;
	static uint16_t idlecnt = 0; /*record the time TASK_mode in idle*/
	static uint8_t Refresh_En = 0; /*Refresh BCC register ENABLE data*/

	  switch(BCCmode){
	  /*BCC initialization£¬including assign CID, initial registers, clear fault status*/
	  case BCC_INIT:
		  /*disable async TPL transfer, callback of SPI will not work*/
		  RDCMD_Pack.Async_Transfer_running = false;
    	  RDCMD_Pack.auto_enable = false;
    	  RDCMD_Pack.Async_Transfer_enbale = false;
    	  RDCMD_Pack.NoResponse_Cnt = 0;
          /*assign CID, initial registers*/
		  Error = BCC_Init(&drvConfig, BCC_INIT_CONF);
		  /*initialization successes, clear fault status, start ADC conversion*/
		  if(Error == BCC_STATUS_SUCCESS)
		  {
			  /*using SM2,4,6,40,45, diagnostic connection, cable or soldering */
			  BCC_DiagCTxOpen(&drvConfig, BCC_BATT_T, SM2result); /*diagnostic if CT sense line is open*/
			  BCC_DiagCellLeak(&drvConfig, SM4result);  /*diagnostic if CT circuit has leakage and connector resistance*/
			  BCC_DiagGPIOxOpen(&drvConfig, SM6result); /*diagnostic if GPIO pin is open*/
			  BCC_DiagVPWR(&drvConfig, SM45result);  /*diagnostic if Vpwr line is connected well*/
			  BCC_DiagCBxOpen(&drvConfig, SM40result );  /*diagnostic if balance circuit is open*/

		      for(CidNodes = 1; CidNodes <=drvConfig.devicesCnt; CidNodes++)
		      {
		    	  Error = BCC_ClearFaultStatus(&drvConfig, CidNodes);
			  }
		      Error = BCC_StartConversion(&drvConfig);
		      Refresh_En = 1;
		      BCCmode = BCC_Refresh;
		  }
		  /*initialization fails, enter idle mode*/
		  else
		  {
		      BCCmode = BCC_Idle;
		  }
		  break;

      /*BCC start measurement£¬MCU sends ADC conversion commands*/
	  case BCC_Measure:

		  /*check if CID exists*/
		  for(CidNodes = 1; CidNodes <=drvConfig.devicesCnt; CidNodes++)
		  {
			  Error = BCC_CheckCid(&drvConfig, CidNodes);
			  if(Error != BCC_STATUS_SUCCESS)
			  {
				  Error = BCC_CheckCid(&drvConfig, CidNodes); //double check
			   /*if request CID unsuccessfully continuously twice, restart initialization*/
				  if(Error != BCC_STATUS_SUCCESS)
				  {
					  RDCMD_Pack.error[CidNodes-1] = Error;
					  BCCmode = BCC_Idle;
					  break;
				  }
			  }
		  }

		  /*If communication is normal, enter next mode*/
		  if(BCCmode == BCC_Measure)
		  {
			  BCCmode = BCC_Refresh;
			  Refresh_En = 1;
			  /*create pack of BCC information, including measurement result, and status*/
			  BCC_STATEPACK_PREPARE();
			  /**Start ADC conversion*/
			  Error = BCC_StartConversion(&drvConfig);
		  }


		  break;

	  case BCC_Refresh:

         /*******using asynchronous transfer to get all registers data***********/
		  if((RDCMD_Pack.Async_Transfer_running == false) && (Refresh_En == 1))
		  {
			  RDCMD_Pack.Async_Transfer_enbale = true;
			  RDCMD_Pack.auto_enable = 1;
			  RDCMD_Pack.error[0] = BCC_ReadRegisters_Async(&drvConfig, BCC_CID_DEV1, BCC_REG_INIT_ADDR, BCC_RX_LIMIT_TPL, BCC_ALLREG);
//			  RDCMD_Pack.error[1] = BCC_ReadRegisters_Async(&drvConfig, BCC_CID_DEV4, BCC_REG_MEAS_STACK_ADDR, 22, &BCC_ALLREG[0][BCC_REG_MEAS_STACK_ADDR-1]);
			  RDCMD_Pack.Async_Transfer_running = true;
		  }
		  /*MCU begin TPL Async-transfer mode, MCU could perform other tasks*/
		  Refresh_En = 0;
		  BCCmode = BCC_Refresh;
		  break;

	  case BCC_Diag:

		  /*disable async TPL transfer, callback of SPI will not work*/
		  RDCMD_Pack.Async_Transfer_enbale = false;
		  RDCMD_Pack.auto_enable = false;
          /*if asynchronous TPL transfer is running, it is needed to wait until transferring ends */
		  if((RDCMD_Pack.Async_Transfer_running == true) && (RDCMD_Pack.NoResponse_Cnt<20))
		  {
			  BCCmode = BCC_Diag;
		  }

		  /*if asynchronous TPL transfer timeout happens, it is needed to force transferring to end */
		  else if((RDCMD_Pack.Async_Transfer_running == true) && (RDCMD_Pack.NoResponse_Cnt>=20))
		  {
			  RDCMD_Pack.Async_Transfer_running = false;
			  BCCmode = BCC_Diag;
			  if((BCC_ReadRegisters_AsyncStatus())!=BCC_STATUS_SUCCESS)
				  RDCMD_Pack.error[RDCMD_Pack.cid-1] = BCC_STATUS_COM_TIMEOUT;
		  }
		  /*asynchronous TPL transfer ends, measurements start*/
		  else if(RDCMD_Pack.Async_Transfer_running == false)
		  {

			  switch(SMitem++)
			  {
			  case 0:
				  Error = BCC_DiagCTxOpen(&drvConfig, BCC_BATT_T, SM2result);/*diagnostic if CT sense line is open*/
				  break;
			  case 1:
				  Error = BCC_DiagOvUv(&drvConfig, BCC_BATT_T, SM1result); /* ov/uv function verification*/
				  break;
			  case 2:
				  Error = BCC_DiagCellVolt(&drvConfig, SM3result ); /* ct channel function verification*/
				  break;
			  case 3:
				  Error = BCC_DiagCBxOpen(&drvConfig, SM40result );/*diagnostic if balance circuit is open*/
				  Error = BCC_DiagGPIOxOtUt(&drvConfig, SM5result );/* ot/ut function verification*/
				  break;
			  case 4:
				  BCC_DiagADC1(&drvConfig, SM7result);
				  break;
			  case 5:
				  Error = BCC_DiagGPIOxOpen(&drvConfig, SM6result);/*diagnostic if GPIO pin is open*/
				  Error = BCC_DiagVPWR(&drvConfig, SM45result);/*diagnostic if Vpwr line is connected well*/
				  break;
			  case 6:

				  Error = BCC_DiagCellLeak(&drvConfig, SM4result);/*diagnostic if CT circuit has leakage and connector resistance*/

				  break;
			  default:
				  SMitem = 0;
				  break;
			  }

			  BCCmode = BCC_Measure;
		  }
		  break;
	  case BCC_Idle:

		  /*Controller waits for 1.2s until BCC reset automatically*/
		  if(idlecnt++ >120)
		  {
		      idlecnt = 0;
		      BCCmode = BCC_INIT;
		  }
		  break;
	  case BCC_Sleeping:
		  break;
	  default:
		  BCCmode = BCC_Measure;
		  break;
	  }
}

/*collect and create the pack of BCC measurements and fault status...*/
void BCC_STATEPACK_PREPARE(void)
{
	uint8_t cid, i;

	for(cid = BCC_CID_DEV1; cid<= drvConfig.devicesCnt; cid++)
	{
	    /* Mask bits. */
	    /* Nothing to mask in CC_NB_SAMPLES, COULOMB_CNT1 and COULOMB_CNT2
	     * registers. */
	    BatPack.u16CCSamples[cid] = BCC_ALLREG[cid-1][BCC_REG_CC_NB_SAMPLES_ADDR-1];
	    BatPack.s32CCCounter[cid ]=(BCC_ALLREG[cid-1][BCC_REG_COULOMB_CNT1_ADDR-1]<<16)|BCC_ALLREG[cid-1][BCC_REG_COULOMB_CNT2_ADDR-1];
	    BCC_ALLREG[cid-1][BCC_REG_MEAS_ISENSE1_ADDR-1] &= BCC_R_MEAS1_I_MASK;
	    BCC_ALLREG[cid-1][BCC_REG_MEAS_ISENSE2_ADDR-1] &= BCC_R_MEAS2_I_MASK;

	    /*store I_SENSEN measurent voltage raw value*/
		BatPack.i32RawCurrent[cid] = BCC_GET_ISENSE_RAW_SIGN(BCC_GET_ISENSE_RAW(BCC_ALLREG[cid-1][BCC_REG_MEAS_ISENSE1_ADDR-1], BCC_ALLREG[cid-1][BCC_REG_MEAS_ISENSE2_ADDR-1]));

	    /* Mask the other registers (starting at 5th register). */
	    for (i = 0U; i < BCC_MEAS_CNT-5; i++)
	    {
	        BatPack.u16RawCell[cid][i] =(BCC_ALLREG[cid-1][BCC_REG_MEAS_STACK_ADDR+i-1] & BCC_R_MEAS_MASK);
	    }
         /**/
	    BCC_DecodeRawMeasurements(cid);

	    BatPack.u16FautStatus[cid][0] = BCC_ALLREG[cid-1][BCC_REG_CELL_OV_FLT_ADDR-1];
	    BatPack.u16FautStatus[cid][1] = BCC_ALLREG[cid-1][BCC_REG_CELL_UV_FLT_ADDR-1];
	    BatPack.u16FautStatus[cid][2] = BCC_ALLREG[cid-1][BCC_REG_CB_OPEN_FLT_ADDR-1];
	    BatPack.u16FautStatus[cid][3] = BCC_ALLREG[cid-1][BCC_REG_CB_SHORT_FLT_ADDR-1];
	    BatPack.u16FautStatus[cid][4] = BCC_ALLREG[cid-1][BCC_REG_AN_OT_UT_FLT_ADDR-1];
	    BatPack.u16FautStatus[cid][5] = BCC_ALLREG[cid-1][BCC_REG_GPIO_SHORT_ADDR-1];
	    BatPack.u16FautStatus[cid][6] = BCC_ALLREG[cid-1][BCC_REG_COM_STATUS_ADDR-1];
	    BatPack.u16FautStatus[cid][7] = BCC_ALLREG[cid-1][BCC_REG_FAULT1_STATUS_ADDR-1];
	    BatPack.u16FautStatus[cid][8] = BCC_ALLREG[cid-1][BCC_REG_FAULT2_STATUS_ADDR-1];
	    BatPack.u16FautStatus[cid][9] = BCC_ALLREG[cid-1][BCC_REG_FAULT3_STATUS_ADDR-1];

	}
}

/*******************************************************************************
* CAN communication with Gui task functions
******************************************************************************/


void CAN2Gui_TASK(void)
{
	uint8_t bcc_cid = 1;
   /*comand from PC Gui*/
    static uint8_t u8Command,u8Data, u8SeclecCID = 0;
    static uint16_t u16SubData;

#if 1
    /*receive command from Gui and handle it*/
	if(0 == CAN_RevCmdProcess(&u8Command, &u8Data, &u16SubData))
	{
		switch(u8Command)
		{
		case 0xC1: //Global Reset
			if(BCC_STATUS_SUCCESS == BCC_SoftwareReset(&drvConfig))
			{
				BCCmode = BCC_Idle;
			}
			break;
		case 0xC2: //initialization
			drvConfig.devicesCnt = u8Data;
			fillBccDriverConfiguration(&drvConfig, u8Data);
			BCCmode = BCC_INIT;
			break;
		case 0xC3: //MCU reset
			break;
		case 0xC4: //BCC enter sleep mode
			if(BCC_STATUS_SUCCESS == BCC_Sleep(&drvConfig))
			{
				BCCmode = BCC_Sleeping;
			}
			break;
		case 0xC5: //BCC wake-up
			/* Wake-up BCC (in case of IDLE mode). */
			if (BCC_STATUS_SUCCESS == BCC_WakeUp(&drvConfig))
			{
				BCCmode = BCC_Measure;
			}
			break;
		case 0xC7: //configure TPL_CFG

             BCC_WriteRegisterGlobal(&drvConfig, BCC_REG_TPL_CFG_ADDR, u16SubData);

		    break;
		case 0xCE: //CID whose registers is sent is determined
			u8SeclecCID = u16SubData&0xF;
			break;

		case 0xCD://update register
			BCC_WriteRegister(&drvConfig, u8SeclecCID, u8Data, u16SubData);
			break;
		case 0xCC://update register
			BCC_WriteRegister(&drvConfig, u8SeclecCID, u8Data, u16SubData);
			break;
		default:
			break;
		}
	}
#endif

	/*Send BCC data and status to Gui by CAN-bus*/
	for(bcc_cid = 1; bcc_cid<=drvConfig.devicesCnt;bcc_cid++)
	{
		CAN_SendError2GUI(bcc_cid, RDCMD_Pack.error[bcc_cid-1]);
		CAN_SendData2GUI(bcc_cid);
		CAN_SendStatus2GUI(bcc_cid);
	}

	/*Send specified BCC status to Gui by CAN-bus*/
	if( u8SeclecCID>0 )
	{
		CAN_SendSTSReg2GUI(u8SeclecCID, BCC_ALLREG, Status_RegMap);
		CAN_SendCtrlReg2GUI(u8SeclecCID, BCC_ALLREG, Config_RegMap);
	}
}
/*!
* @brief: LPIT interrupt handler.
*         When an interrupt occurs clear channel flag and toggle LED0
*/
void LPIT_ISR(void)
{
  static uint16_t cnt = 0;
  /* Clear LPIT channel flag */
  LPIT_DRV_ClearInterruptFlagTimerChannels(INST_LPIT1, (1 << 0));
  cnt++;

  /*enable BCC task every 10ms*/
  if(cnt%10 == 0)
  {
	  BCC_TASK_FLAG = 1;
	  RDCMD_Pack.NoResponse_Cnt++;
	  BCC_TASK_CNT++;
	  /*do measurement every 100 ms*/
	  if(BCC_TASK_CNT%10 == 0)
	  {
	      if((BCCmode!= BCC_INIT)&&(BCCmode!= BCC_Idle)&&(BCCmode!= BCC_Sleeping))
	      {
	          BCCmode = BCC_Diag;   // do diagnostic before ADC conversion
//	    	  BCCmode = BCC_Measure;// without diagnostic before ADC conversion
	      }
	      CAN_TASK_FLAG = 1;
	  }
  }
  /* Toggle LED0 */
  if(cnt%500==0)
  {
      PINS_DRV_TogglePins(PTD, (1<<15));
  }
  /*check watchdog of uja1169 every 300ms*/
  if(cnt%300==0)
  {
	  UJA_WD_FLAG = 1;
  }

}


/* END main */
/*!
** @}
*/
/*
** ###################################################################
**
**     This file was created by Processor Expert 10.1 [05.21]
**     for the NXP S32K series of microcontrollers.
**
** ###################################################################
*/
