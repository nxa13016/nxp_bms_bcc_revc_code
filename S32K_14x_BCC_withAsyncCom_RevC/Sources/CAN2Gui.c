/*
 * CAN2Gui.c
 *
 *  Created on: 2018��11��28��
 *      Author: nxa13016
 */

#include "CAN2Gui.h"


void CAN_SendData2GUI(uint8_t u8Daughter)
{

	uint16_t u16bagID = 0;
	uint16_t FCData[4];
	uint8_t flexcan_senddata[8];

	if(u8Daughter > 15)
		return;
	//StackVoltage & CellVoltage & Temperature
	for(u16bagID = 0; u16bagID < 24; u16bagID+=4)
	{
		FCData[0] = BatPack.u16RawCell[u8Daughter][u16bagID+0];
		FCData[1] = BatPack.u16RawCell[u8Daughter][u16bagID+1];
		FCData[2] = BatPack.u16RawCell[u8Daughter][u16bagID+2];
		FCData[3] = BatPack.u16RawCell[u8Daughter][u16bagID+3];

		flexcan_senddata[0] = (FCData[0]>>8)&0xFF;
		flexcan_senddata[1] = (FCData[0]>>0)&0xFF;
		flexcan_senddata[2] = (FCData[1]>>8)&0xFF;
		flexcan_senddata[3] = (FCData[1]>>0)&0xFF;
		flexcan_senddata[4] = (FCData[2]>>8)&0xFF;
		flexcan_senddata[5] = (FCData[2]>>0)&0xFF;
		flexcan_senddata[6] = (FCData[3]>>8)&0xFF;
		flexcan_senddata[7] = (FCData[3]>>0)&0xFF;

		SendCANData(0x18801000 +(u8Daughter+0)*0x100 +u16bagID , flexcan_senddata);
		Delayus(500);
	}
	//Current
	FCData[0] = (BatPack.i32RawCurrent[u8Daughter] >> 16)& 0xFFFF;
	FCData[1] = BatPack.i32RawCurrent[u8Daughter] & 0xFFFF;

	flexcan_senddata[0] = (FCData[0]>>8)&0xFF;
	flexcan_senddata[1] = (FCData[0]>>0)&0xFF;
	flexcan_senddata[2] = (FCData[1]>>8)&0xFF;
	flexcan_senddata[3] = (FCData[1]>>0)&0xFF;
	flexcan_senddata[4] = (FCData[2]>>8)&0xFF;
	flexcan_senddata[5] = (FCData[2]>>0)&0xFF;
	flexcan_senddata[6] = (FCData[3]>>8)&0xFF;
	flexcan_senddata[7] = (FCData[3]>>0)&0xFF;

	SendCANData(0x18802000 + (u8Daughter+0)*0x100, flexcan_senddata);
	Delayus(500);

}

void CAN_SendStatus2GUI(uint8_t u8Daughter){

	uint16_t FCData[4];
	uint8_t flexcan_senddata[8];

	FCData[0] = BatPack.u16FautStatus[u8Daughter][7];
	FCData[1] = BatPack.u16FautStatus[u8Daughter][8];
	FCData[2] = BatPack.u16FautStatus[u8Daughter][9];
	FCData[3] = BatPack.u16FautStatus[u8Daughter][6];

	flexcan_senddata[0] = (FCData[0]>>8)&0xFF;
	flexcan_senddata[1] = (FCData[0]>>0)&0xFF;
	flexcan_senddata[2] = (FCData[1]>>8)&0xFF;
	flexcan_senddata[3] = (FCData[1]>>0)&0xFF;
	flexcan_senddata[4] = (FCData[2]>>8)&0xFF;
	flexcan_senddata[5] = (FCData[2]>>0)&0xFF;
	flexcan_senddata[6] = (FCData[3]>>8)&0xFF;
	flexcan_senddata[7] = (FCData[3]>>0)&0xFF;

	SendCANData(0x18804000 +(u8Daughter+0)*0x100 , flexcan_senddata);

	Delayus(500);
}

void CAN_SendSM2GUI(uint8_t u8Daughter, uint32_t u32DiagData, uint16_t u16SMitemNum)
{
    uint16_t FCData[4];
    uint8_t flexcan_senddata[8];

    FCData[0] = u32DiagData >> 16;
    FCData[1] = u32DiagData;
    FCData[2] = 0;
    FCData[3] = 0;

	flexcan_senddata[0] = (FCData[0]>>8)&0xFF;
	flexcan_senddata[1] = (FCData[0]>>0)&0xFF;
	flexcan_senddata[2] = (FCData[1]>>8)&0xFF;
	flexcan_senddata[3] = (FCData[1]>>0)&0xFF;
	flexcan_senddata[4] = (FCData[2]>>8)&0xFF;
	flexcan_senddata[5] = (FCData[2]>>0)&0xFF;
	flexcan_senddata[6] = (FCData[3]>>8)&0xFF;
	flexcan_senddata[7] = (FCData[3]>>0)&0xFF;

	SendCANData(0x18805000 +u8Daughter*0x100 + u16SMitemNum, flexcan_senddata);

	Delayus(500);
}


void CAN_SendCtrlReg2GUI(uint8_t u8Daughter, uint16_t StatusPtr[][112], uint8_t *StatusAddr)
{

	uint16_t FCData[4];
	uint16_t u16bagID = 0;
	uint8_t flexcan_senddata[8];

	for(u16bagID = 0; u16bagID<31; u16bagID+=2){

		FCData[0] = StatusAddr[u16bagID+0]&0xFF;
		FCData[1] = StatusPtr[u8Daughter-1][FCData[0]-1];
		if(u16bagID<30){
		    FCData[2] = StatusAddr[u16bagID+1]&0xFF;
		    FCData[3] = StatusPtr[u8Daughter-1][FCData[2]-1];
		}
		else{
		    FCData[2] = 0;
		    FCData[3] = 0;
		}

		flexcan_senddata[0] = (FCData[0]>>8)&0xFF;
		flexcan_senddata[1] = (FCData[0]>>0)&0xFF;
		flexcan_senddata[2] = (FCData[1]>>8)&0xFF;
		flexcan_senddata[3] = (FCData[1]>>0)&0xFF;
		flexcan_senddata[4] = (FCData[2]>>8)&0xFF;
		flexcan_senddata[5] = (FCData[2]>>0)&0xFF;
		flexcan_senddata[6] = (FCData[3]>>8)&0xFF;
		flexcan_senddata[7] = (FCData[3]>>0)&0xFF;

		SendCANData(0x18806000 + u8Daughter*0x100, flexcan_senddata);

		Delayus(500);
	}
}

void CAN_SendSTSReg2GUI(uint8_t u8Daughter, uint16_t StatusPtr[][112], uint8_t *StatusAddr){

	uint16_t FCData[4];
	uint16_t u16bagID = 0;
	uint8_t flexcan_senddata[8];

	for(u16bagID = 0; u16bagID<15; u16bagID+=2){

		FCData[0] = StatusAddr[u16bagID+0]&0xFF;
		FCData[1] = StatusPtr[u8Daughter-1][FCData[0]-1];
		if((u16bagID+1)<15){
			FCData[2] = StatusAddr[u16bagID+1]&0xFF;
			FCData[3] = StatusPtr[u8Daughter-1][FCData[2]-1];
		}
		else{
			FCData[2] = 0;
			FCData[3] = 0;
		}

		flexcan_senddata[0] = (FCData[0]>>8)&0xFF;
		flexcan_senddata[1] = (FCData[0]>>0)&0xFF;
		flexcan_senddata[2] = (FCData[1]>>8)&0xFF;
		flexcan_senddata[3] = (FCData[1]>>0)&0xFF;
		flexcan_senddata[4] = (FCData[2]>>8)&0xFF;
		flexcan_senddata[5] = (FCData[2]>>0)&0xFF;
		flexcan_senddata[6] = (FCData[3]>>8)&0xFF;
		flexcan_senddata[7] = (FCData[3]>>0)&0xFF;

		SendCANData(0x18807000 + u8Daughter*0x100, flexcan_senddata);

		Delayus(500);
	}
}

void CAN_SendError2GUI(uint8_t u8Daughter, bcc_status_t res){

	uint16_t FCData[4];
	uint8_t flexcan_senddata[8];

	if ((res == BCC_STATUS_COM_TIMEOUT)
			|| (res == BCC_STATUS_CRC)
			|| (res == BCC_STATUS_COM_TAG_ID)
			|| (res == BCC_STATUS_COM_TX)
			|| (res == BCC_STATUS_COM_RC)
			||(res == BCC_STATUS_SUCCESS))
	{
		FCData[0] = res|0xFF00;

		flexcan_senddata[0] = (FCData[0]>>8)&0xFF;
		flexcan_senddata[1] = (FCData[0]>>0)&0xFF;

		SendCANData(0x18803000 +(u8Daughter+0)*0x100 , flexcan_senddata);

		Delayus(500);
	}

}

status_t CAN_RevCmdProcess(uint8_t* pu8Command, uint8_t* pu8Data, uint16_t* Subpu16Data){

	status_t res;

	res = FLEXCAN_DRV_GetTransferStatus(INST_CANCOM1, RX_MailBox);
    if(res == STATUS_SUCCESS){
        /* Start receiving data in RX_MAILBOX. */
		if(RecvBuff.msgId != 0x18800000)
			return STATUS_ERROR; //it's not a command

		*pu8Command = RecvBuff.data[0];
		*pu8Data    = RecvBuff.data[1];
		*Subpu16Data = ((RecvBuff.data[2]<<8)&0xFF00)|(RecvBuff.data[3]&0xFF);

//        FLEXCAN_DRV_ConfigRxMb(INST_CANCOM1, RX_MailBox, &dataInfo, GuiCmdID);
        FLEXCAN_DRV_Receive(INST_CANCOM1, RX_MailBox, &RecvBuff);
    }

	return res;
}
