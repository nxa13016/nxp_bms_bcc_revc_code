/*
 * demo_config.c
 *
 *  Created on: 2018��11��21��
 *      Author: nxa13016
 */


#include "cpu.h"
#include "mc33771c/mc33771c.h"
#include "demo_config.h"

/*!
 * @brief This function loads default driver configuration.
 */
void fillBccDriverConfiguration(bcc_drv_config_t* bccDrvConfig, uint8_t DeviceNodes)
{
    static bcc_ntc_config_t ntcConfig;
    static bcc_ct_filter_t ctFilterComp;
    uint8_t idx;

    bccDrvConfig->devicesCnt = DeviceNodes;
    for(idx = 0; idx<bccDrvConfig->devicesCnt; idx++){
        bccDrvConfig->device[idx] = BCC_DEVICE_MC33771;
        bccDrvConfig->cellCnt[idx] = 14U;
    }
#ifdef TPL
    bccDrvConfig->commMode = BCC_MODE_TPL;
#else
    /* #ifdef SPI or SPI_TRANSLT */
    bccDrvConfig->commMode = BCC_MODE_SPI;
#endif

    /* NTC configuration. */
    ntcConfig.rntc = 6800U;               /* NTC pull-up 6.8kOhm */
    ntcConfig.refTemp = 25U;              /* NTC resistance 10kOhm at 25 degC */
    ntcConfig.refRes = 10000U;            /* NTC resistance 10kOhm at 25 degC */
    ntcConfig.beta = 3900U;
    bccDrvConfig->compConfig.ntcConfig = &ntcConfig;

    /* CT filter components. */
    ctFilterComp.rLpf1 = 3000U;           /* R_LPF-1 3kOhm */
    ctFilterComp.rLpf2 = 2000U;           /* R_LPF-2 2kOhm */
    ctFilterComp.cLpf = 470U;             /* C_LPF 470nF */
    ctFilterComp.cIn = 47U;               /* C_IN 47nF */
    bccDrvConfig->compConfig.ctFilterComp = &ctFilterComp;

    /* ISENSE filter components. */
    bccDrvConfig->compConfig.isenseComp.rLpfi = 127U;     /* R_LPFI 127Ohm */
    bccDrvConfig->compConfig.isenseComp.cD = 6800U;       /* C_D 6.8uF */
    bccDrvConfig->compConfig.isenseComp.cLpfi = 47U;      /* C_LPFI 47nF */
    bccDrvConfig->compConfig.isenseComp.rShunt = 100000U; /* R_SHUNT 100mOhm */
    bccDrvConfig->compConfig.isenseComp.iMax = 24570U;    /* I_MAX 24.57A */
}

void Delayus(uint16_t uscnt)
{
	BCC_WaitUs(uscnt);
}

void Delayms(uint16_t mscnt)
{
	BCC_WaitMs(mscnt);
}

/*
 * @brief Initialize FlexCAN driver and configure the bit rate
 */

flexcan_msgbuff_t RecvBuff;
void FlexCANInit(void)
{
    /*
     * Initialize FlexCAN driver
     *  - 8 byte payload size
     *  - FD enabled
     *  - Bus clock as peripheral engine clock
     */
    FLEXCAN_DRV_Init(INST_CANCOM1, &canCom1_State, &canCom1_InitConfig0);

    flexcan_data_info_t dataInfo =
    {
            .data_length = 8U,
            .msg_id_type = FLEXCAN_MSG_ID_EXT,
            .enable_brs  = false,
            .fd_enable   = false,
            .fd_padding  = 0U
    };

    /* Configure RX message buffer with index RX_MSG_ID and RX_MAILBOX */
    /* Start receiving data in RX_MAILBOX. */
    FLEXCAN_DRV_ConfigRxMb(INST_CANCOM1, RX_MailBox, &dataInfo, GuiCmdID);
    FLEXCAN_DRV_Receive(INST_CANCOM1, RX_MailBox, &RecvBuff);
}

/*
 * @brief: Send data via CAN to the specified mailbox with the specified message id
 * @param mailbox   : Destination mailbox number
 * @param messageId : Message ID
 * @param data      : Pointer to the TX data
 * @return          : None
 */
void SendCANData(uint32_t messageId, uint8_t * data)
{
    /* Set information about the data to be sent
     *  - 1 byte in length
     *  - Standard message ID
     *  - Bit rate switch enabled to use a different bitrate for the data segment
     *  - Flexible data rate enabled
     *  - Use zeros for FD padding
     */
    flexcan_data_info_t dataInfo =
    {
            .data_length = 8,
            .msg_id_type = FLEXCAN_MSG_ID_EXT,
            .enable_brs  = false,
            .fd_enable   = false,
            .fd_padding  = 0U
    };

    /* Configure TX message buffer with index TX_MSG_ID and TX_MAILBOX*/
    FLEXCAN_DRV_ConfigTxMb(INST_CANCOM1, TX_MailBox, &dataInfo, messageId);

    /* Execute send non-blocking */
    FLEXCAN_DRV_Send(INST_CANCOM1, TX_MailBox, &dataInfo, messageId, data);
}

status_t ReceiveCANData(flexcan_msgbuff_t *recvBuff)
{
	static status_t can_rxstatus = STATUS_SUCCESS;

	if(can_rxstatus == STATUS_SUCCESS){
	    /* Set information about the data to be received
	     *  - 1 byte in length
	     *  - Standard message ID
	     *  - Bit rate switch enabled to use a different bitrate for the data segment
	     *  - Flexible data rate enabled
	     *  - Use zeros for FD padding
	     */
	    flexcan_data_info_t dataInfo =
	    {
	            .data_length = 8U,
	            .msg_id_type = FLEXCAN_MSG_ID_EXT,
	            .enable_brs  = false,
	            .fd_enable   = false,
	            .fd_padding  = 0U
	    };

        /* Start receiving data in RX_MAILBOX. */
    	FLEXCAN_DRV_ConfigRxMb(INST_CANCOM1, RX_MailBox, &dataInfo, GuiCmdID);
        FLEXCAN_DRV_Receive(INST_CANCOM1, RX_MailBox, recvBuff);
	}

	can_rxstatus = FLEXCAN_DRV_GetTransferStatus(INST_CANCOM1, RX_MailBox);

	return can_rxstatus;
}
