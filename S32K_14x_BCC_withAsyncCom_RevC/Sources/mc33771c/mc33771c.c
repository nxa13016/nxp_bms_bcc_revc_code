/*
 * Copyright 2016 - 2018 NXP
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * o Redistributions of source code must retain the above copyright notice, this list
 *   of conditions and the following disclaimer.
 *
 * o Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * o Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from this
 *   software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*!
 * @file bcc.c
 *
 * Battery cell controller SW driver.
 * Supports boards based on MC33771C.
 *
 * This module is common for all supported models.
 */

/*******************************************************************************
 * Includes
 ******************************************************************************/

#include "mc33771c.h"
#include "mc33771c_communication.h"
#include "mc33771c_peripheries.h"
#include <math.h>

/*******************************************************************************
 * Definitions
 ******************************************************************************/

/*! @brief Cell map when 7 cells are connected to MC33771. */
#define BCC_CM_MC33771_7CELLS     0x380FU

/* NTC configuration and minimal & maximal values. */
/*! @brief Size of NTC look-up table. */
#define BCC_NTC_TABLE_SIZE        (BCC_NTC_MAXTEMP - BCC_NTC_MINTEMP + 1)
/*! @brief 0 degree Celsius converted to Kelvin. */
#define BCC_NTC_DEGC_0            273.15
/*! @brief Maximal voltage (5V). */
#define BCC_NTC_VCOM              5.0
/*! @brief Resolution of measured voltage in Volts (U = 152.58789 uV *
 *  register_value), with 5V maximal voltage. */
#define BCC_NTC_REGISTER_RES      0.00015258789

/** Diagnostics time constants. **/
/*!
 * @brief Measurement time constant tau in [us].
 *
 * See formula in MC33771C datasheet.
 *
 * @param drvConfig Pointer to driver instance configuration.
 * @return Tau constant in [us].
 */
#define BCC_DIAG_TAU(drvConfig) ( \
    (drvConfig->compConfig.ctFilterComp->rLpf1 *       \
     drvConfig->compConfig.ctFilterComp->cLpf) / 1000U \
)

/*!
 * @brief Measurement time constant tau_I in [us].
 *
 * See formula in MC33771C datasheet.
 *
 * @param drvConfig Pointer to driver instance configuration.
 * @return Tau_I constant in [us].
 */
#define BCC_DIAG_TAU_I(drvConfig) ( \
    (drvConfig->compConfig.isenseComp.rLpfi *              \
     (drvConfig->compConfig.isenseComp.cLpfi +             \
      (2U * drvConfig->compConfig.isenseComp.cD))) / 1000U \
)

/*!
 * @brief Diagnostic time t_diag to detect an open from the shunt to the
 * current filter in [us].
 *
 * See formula in MC33771C datasheet.
 *
 * @param drvConfig Pointer to driver instance configuration.
 * @return T_diag constant in [us].
 */
#define BCC_DIAG_T_DIAG(drvConfig) ( \
    ((drvConfig->compConfig.isenseComp.cLpfi +                            \
      drvConfig->compConfig.isenseComp.cD) *                              \
     ((BCC_DIAG_V_ISENSE_OL + ((drvConfig->compConfig.isenseComp.rShunt * \
         drvConfig->compConfig.isenseComp.iMax) / 1000U)) /               \
      (BCC_DIAG_ISENSE_OL))) / 1000U                                      \
)

/*!
 * @brief Returns a non-zero value when desired cell (cellNo) is connected
 * to the BCC specified by CID. Otherwise returns zero.
 *
 * @param drvConfig Pointer to driver instance configuration.
 * @param cid Cluster Identification Address.
 * @param cellNo Number of a cell (range is {1, ..., 14} for MC33771.
 * @return Non-zero value if cell is connected, zero otherwise.
 */
#define BCC_IS_CELL_CONN(drvConfig, cid, cellNo) \
    (drvConfig->drvData.cellMap[(cid) - 1] & (1U << ((cellNo) - 1U)))

/*!
 * @brief Calculates final temperature value.
 *
 * @param tblIdx Index of value in NTC table which is close
 *        to the register value provided by user.
 * @param degTenths Fractional part of temperature value.
 * @return Temperature.
 */
#define BCC_COMP_TEMP(tblIdx, degTenths) \
    ((((tblIdx) + BCC_NTC_MINTEMP) * 10) + (degTenths))

/*!
 * Returns true if value VAL is in the range defined by MIN and MAX values
 * (range includes the border values).
 *
 * @param val Comparison value.
 * @param min Minimal value of the range.
 * @param max Maximal value of the range.
 * @return True if value is the range. False otherwise.
 */
#define BCC_IS_IN_RANGE(val, min, max)   (((val) >= (min)) && ((val) <= (max)))

/*******************************************************************************
 * Global variables (constants)
 ******************************************************************************/

/**
 * NTC look up table intended for resistance to temperature conversion. After
 * driver initialization, array item contains raw value from a register.
 * Index of the item is temperature value.
 */
uint16_t g_ntcTable[BCC_NTC_TABLE_SIZE];

/** Addresses of configurable registers.
 *
 * TH_CT14 - TH_CT7 (0x4c - 0x53) registers. These values are ignored
 * in the initialization. */
static const uint8_t BCC_INIT_CONF_REG_ADDR[BCC_INIT_CONF_REG_CNT] = {
    /* Note: INIT register is initialized automatically. SYS_CFG_GLOBAL register
     *       contains only command GO2SLEEP (no initialization needed).
     *       EEPROM_CTRL, FUSE_MIRROR_DATA and FUSE_MIRROR_CNTL registers are
     *       not initialized. */

    BCC_REG_SYS_CFG1_ADDR,
    BCC_REG_SYS_CFG2_ADDR,
    BCC_REG_ADC_CFG_ADDR,
    BCC_REG_ADC2_OFFSET_COMP_ADDR,
    BCC_REG_OV_UV_EN_ADDR,
	BCC_REG_TPL_CFG_ADDR,
    BCC_REG_GPIO_CFG1_ADDR,
    BCC_REG_GPIO_CFG2_ADDR,
    BCC_REG_FAULT_MASK1_ADDR,
    BCC_REG_FAULT_MASK2_ADDR,
    BCC_REG_FAULT_MASK3_ADDR,
    BCC_REG_WAKEUP_MASK1_ADDR,
    BCC_REG_WAKEUP_MASK2_ADDR,
    BCC_REG_WAKEUP_MASK3_ADDR,
    BCC_REG_TH_ALL_CT_ADDR,
    BCC_REG_TH_CT14_ADDR,
    BCC_REG_TH_CT13_ADDR,
    BCC_REG_TH_CT12_ADDR,
    BCC_REG_TH_CT11_ADDR,
    BCC_REG_TH_CT10_ADDR,
    BCC_REG_TH_CT9_ADDR,
    BCC_REG_TH_CT8_ADDR,
    BCC_REG_TH_CT7_ADDR,
    BCC_REG_TH_CT6_ADDR,
    BCC_REG_TH_CT5_ADDR,
    BCC_REG_TH_CT4_ADDR,
    BCC_REG_TH_CT3_ADDR,
    BCC_REG_TH_CT2_ADDR,
    BCC_REG_TH_CT1_ADDR,
    BCC_REG_TH_AN6_OT_ADDR,
    BCC_REG_TH_AN5_OT_ADDR,
    BCC_REG_TH_AN4_OT_ADDR,
    BCC_REG_TH_AN3_OT_ADDR,
    BCC_REG_TH_AN2_OT_ADDR,
    BCC_REG_TH_AN1_OT_ADDR,
    BCC_REG_TH_AN0_OT_ADDR,
    BCC_REG_TH_AN6_UT_ADDR,
    BCC_REG_TH_AN5_UT_ADDR,
    BCC_REG_TH_AN4_UT_ADDR,
    BCC_REG_TH_AN3_UT_ADDR,
    BCC_REG_TH_AN2_UT_ADDR,
    BCC_REG_TH_AN1_UT_ADDR,
    BCC_REG_TH_AN0_UT_ADDR,
    BCC_REG_TH_ISENSE_OC_ADDR,
    BCC_REG_TH_COULOMB_CNT_MSB_ADDR,
    BCC_REG_TH_COULOMB_CNT_LSB_ADDR,
};

TYPE_BatteryPack BatPack;

bcc_rdcmd_pack RDCMD_Pack;
/*******************************************************************************
 * Prototypes of internal functions
 ******************************************************************************/

/*!
 * @brief This function calculates the tau_diag time constant.
 *
 * @param drvConfig Pointer to driver instance configuration.
 */
static void BCC_CalcTauDiag(bcc_drv_config_t* const drvConfig);

/*!
 * @brief This function enables MC33664 TPL device. It uses EN and
 * INTB pins.
 *
 * @param drvConfig Pointer to driver instance configuration.
 *
 * @return bcc_status_t Error code.
 */
static bcc_status_t BCC_EnableTPL(bcc_drv_config_t* const drvConfig);


/*!
 * @brief This function initializes a BCC device or all devices in daisy chain.
 *
 * @param drvConfig Pointer to driver instance configuration.
 * @param devConf Initialization values of BCC device registers.
 *                devConf[0][x] belongs to device with CID 1,
 *                devConf[1][x] belongs to device with CID 2, etc.
 *
 * @return bcc_status_t Error code.
 */
static bcc_status_t BCC_InitRegisters(bcc_drv_config_t* const drvConfig,
    const uint16_t devConf[][BCC_INIT_CONF_REG_CNT]);

/*!
 * @brief This function assigns CID to a BCC device that has CID equal to zero.
 * It closes bus switch to allow communication with the next BCC.
 *
 * @param drvConfig Pointer to driver instance configuration.
 * @param cid Cluster ID of BCC device.
 *
 * @return bcc_status_t Error code.
 */
static bcc_status_t BCC_AssignCid(bcc_drv_config_t* const drvConfig,
    bcc_cid_t cid);

/*!
 * @brief This function initializes all connected BCC devices.
 *
 * @param drvConfig Pointer to driver instance configuration.
 * @param devConf Initialization values of BCC device registers.
 *                devConf[0][x] belongs to device with CID 1,
 *                devConf[1][x] belongs to device with CID 2, etc.
 *
 * @return bcc_status_t Error code.
 */
static bcc_status_t BCC_InitDevices(bcc_drv_config_t* const drvConfig,
    const uint16_t devConf[][BCC_INIT_CONF_REG_CNT]);

/* Internal functions used by diagnostics. */
/*!
 * @brief This function enters diagnostic mode.
 *
 * @param drvConfig Pointer to driver instance configuration.
 * @param cid Cluster ID of BCC device.
 *
 * @return bcc_status_t Error code.
 */
static bcc_status_t BCC_EnterDiagnostics(bcc_drv_config_t* const drvConfig,
    bcc_cid_t cid);

/*!
 * @brief This function calculates exits diagnostic mode.
 *
 * @param drvConfig Pointer to driver instance configuration.
 * @param devConf Initialization values of BCC registers.
 *
 * @return bcc_status_t Error code.
 */
static bcc_status_t BCC_ExitDiagnostics(bcc_drv_config_t* const drvConfig,
    bcc_cid_t cid);

/*!
 * @brief This function starts on-demand conversion and waits for completion.
 *
 * @param drvConfig Pointer to driver instance configuration.
 * @param cid Cluster ID of BCC device.
 *
 * @return bcc_status_t Error code.
 */
static bcc_status_t BCC_RunCOD(bcc_drv_config_t* const drvConfig,
    bcc_cid_t cid);

/*!
 * @brief This function commands CT or CB diagnostic switches.
 *
 * @param drvConfig Pointer to driver instance configuration.
 * @param cid Cluster ID of BCC device.
 * @param switches Selection of CT or CB switches.
 * @param odd State of odd diagnostic switches.
 * @param even State of even diagnostic switches.
 *
 * @return bcc_status_t Error code.
 */
static bcc_status_t BCC_CommandSwitches(bcc_drv_config_t* const drvConfig,
    bcc_cid_t cid, bcc_diag_switch_sel_t switches,
    bcc_diag_switch_pos_t odd, bcc_diag_switch_pos_t even);

/*!
 * @brief This function computes voltage in uV from register raw values.
 *
 * @param rawMeas Array containing raw values from registers.
 * @param size Size of the array.
 * @param convMeas Resulting array containing converted values.
 */
static inline void BCC_ConvertVoltageArray(const uint16_t rawMeas[],
    uint8_t size, uint32_t convMeas[]);

/*!
 * @brief This function is part overvoltage and undervoltage functional
 * verification. It commands CT switches and measures voltage on cells.
 *
 * @param drvConfig Pointer to driver instance configuration.
 * @param cid Cluster ID of BCC device.
 * @param odd State of odd switches.
 * @param even State of even switches.
 * @param fltOvrv Result of the function containing CELL_OV_FLT register value.
 * @param fltUndv Result of the function containing CELL_UV_FLT register value.
 *
 * @return bcc_status_t Error code.
 */
static bcc_status_t BCC_DiagOvuvPart(bcc_drv_config_t* const drvConfig,
	    bcc_diag_switch_pos_t odd, bcc_diag_switch_pos_t even,
		bcc_diag_ov_uv_res_t *results);

/*!
 * @brief This function is part of CTX open detect. It clear OV and UV
 * fault registers, commands CT switches and measures voltage on cells.
 *
 * @param drvConfig Pointer to driver instance configuration.
 * @param odd State of odd switches.
 * @param even State of even switches.
 * @param measVal Result of the function containing measured values for
 *                CT 14 - 1 (MC33771).
 *
 * @return bcc_status_t Error code.
 */
static bcc_status_t BCC_DiagCtxopenPart(bcc_drv_config_t* const drvConfig,
		bcc_diag_switch_pos_t odd, bcc_diag_switch_pos_t even, uint16_t *measVal);

/* Internal function used by DiagCurrentMeas function only. */
/*!
 * @brief This function is part of current measurement diagnostics (the first
 * procedure). It resets Coulomb counter, initiates conversion and reads
 * measured current.
 *
 * @param drvConfig Pointer to driver instance configuration.
 * @param cid Cluster ID of BCC device.
 * @param current Result of the diagnostic containing measured ISENSE
 *                voltage in [uV].
 *
 * @return bcc_status_t Error code.
 */
static bcc_status_t BCC_DiagRunImeas(bcc_drv_config_t* const drvConfig,
    bcc_cid_t cid, int32_t *current);

/* Internal function used by DiagGPIOxOtUt function only. */
/*!
 * @brief This function is part of GPIO OT and UT functional verification.
 *
 * Writes value given in parameter to GPIO_CFG2 register, initiates conversion,
 * reads AN_OT_UT_FLT register and clears OT/UT faults.
 *
 * @param drvConfig Pointer to driver instance configuration.
 * @param cid Cluster ID of BCC device.
 * @param gpioCFG2Val Value of GPIO_CFG2 GPIO configuration register.
 * @param otUtStat Over/Under-temperature status for GPIOs (expected for all
 *                 GPIOs). It contains value of AN_OT_UT_FLT register.
 *
 * @return bcc_status_t Error code.
 */
static bcc_status_t BCC_GpioOtUtPart(bcc_drv_config_t* const drvConfig,  uint8_t otut, bcc_diag_ot_ut_res_t *result);

/*******************************************************************************
 * Internal function
 ******************************************************************************/

/*FUNCTION**********************************************************************
 *
 * Function Name : BCC_CalcTauDiag
 * Description   : This function calculates the tau_diag time constant.
 *
 *END**************************************************************************/
static void BCC_CalcTauDiag(bcc_drv_config_t* const drvConfig)
{
    double rLpf1 = (double)(drvConfig->compConfig.ctFilterComp->rLpf1);
    double rLpf2 = (double)(drvConfig->compConfig.ctFilterComp->rLpf2);
    double cLpf = ((double)(drvConfig->compConfig.ctFilterComp->cLpf)) / 1000000000;
    double cIn = ((double)(drvConfig->compConfig.ctFilterComp->cIn)) / 1000000000;
    double rPd = (double)(BCC_RPD);
    double tauDiag, c, k;

    c = rPd + 2 * (rLpf1 + rLpf2);
    c = c / (2 * cLpf * cIn * rLpf1 * rLpf2 * rPd);

    k = cLpf * rLpf1 * (rPd + 2 * rLpf2) + 2 * cIn * rPd * (rLpf1 + rLpf2);
    k = k / (4 * cLpf * cIn * rLpf1 * rLpf2 * rPd);

    tauDiag = k * (1 - sqrt(1 - (c / (k * k))));

    drvConfig->drvData.tauDiagn = (uint16_t)(1000000 / tauDiag);

    tauDiag = (rPd+2*rLpf2)*cLpf/2;
    tauDiag+= (2*rLpf1)*(2*rLpf2+rPd)/(2*rLpf1+2*rLpf2+rPd)*cLpf/2;
    tauDiag+= (rPd*(2*rLpf1+2*rLpf2))/(2*rLpf1+2*rLpf2+rPd)*cIn;
    tauDiag+= ((rLpf1+rLpf2)*(rLpf1+rLpf2+rPd)/(2*rLpf1+2*rLpf2+rPd)+(rLpf1+rLpf2+rPd))*cIn;
    tauDiag+= rPd*cIn;

    drvConfig->drvData.tauDiag = (uint16_t)( tauDiag*1000000 );
}

/*FUNCTION**********************************************************************
 *
 * Function Name : BCC_EnableTPL
 * Description   : This function enables MC33664 TPL device. It uses EN and
 *                 INTB pins.
 *
 *END**************************************************************************/
static bcc_status_t BCC_EnableTPL(bcc_drv_config_t* const drvConfig)
{
    uint32_t timeout = BCC_WAKEUP_TIMEOUT; /* Wake-up timeout. */

    /* Set normal state (transition from low to high). */
    BCC_WriteEnPin(drvConfig->drvInstance, 0);
    /* Wait at least 100 us. */
    BCC_WaitUs(150);
    BCC_WriteEnPin(drvConfig->drvInstance, 1);

    /* Note: MC33664 has time tReady (max. 100 us, equal to the LOW level)
     * to take effect.  */
    while ((BCC_ReadIntbPin(drvConfig->drvInstance) > 0) && (timeout > 0))
    {
        /* Wait for INTB transition from high to low (max. 100 us). */
        /* Timeout. */
        timeout--;
    }
    if (timeout == 0)
    {
        return BCC_STATUS_COM_TIMEOUT;
    }

    timeout = BCC_WAKEUP_TIMEOUT;
    while ((BCC_ReadIntbPin(drvConfig->drvInstance) == 0) && (timeout > 0))
    {
        /* Wait for INTB transition from low to high (typ. 100 us). */
        /* Timeout. */
        timeout--;
    }

    /* Now the device should be in normal mode (i.e. after INTB low to high
    * transition). For sure wait for 150 us. */
    BCC_WaitUs(150);

    return (timeout == 0) ? BCC_STATUS_COM_TIMEOUT : BCC_STATUS_SUCCESS;
}

/*FUNCTION**********************************************************************
 *
 * Function Name : BCC_InitRegisters
 * Description   : This function initializes a BCC device or all devices in
 *                 daisy chain.
 *
 *END**************************************************************************/
static bcc_status_t BCC_InitRegisters(bcc_drv_config_t* const drvConfig,
    const uint16_t devConf[][BCC_INIT_CONF_REG_CNT])
{
    uint8_t i;
    bcc_status_t error;

    /* Initialize all registers according to according to the user values. */

	for (i = 0; i < BCC_INIT_CONF_REG_CNT; i++)
	{
		error = BCC_WriteRegisterGlobal(drvConfig, BCC_INIT_CONF_REG_ADDR[i], devConf[0][i]);
		if (error != BCC_STATUS_SUCCESS)
		{
			return error;
		}
	}


    return error;
}

/*FUNCTION**********************************************************************
 *
 * Function Name : BCC_AssignCid
 * Description   : This function assigns CID to a BCC device that has CID equal
 *                 to zero.
 *
 *END**************************************************************************/
static bcc_status_t BCC_AssignCid(bcc_drv_config_t* const drvConfig,
    bcc_cid_t cid)
{
    uint16_t regVal = 0U;   /* Value of a register. */
    bcc_status_t error;

    /* Check if unassigned node replies. This is the first reading after device
     * reset. */
    error = BCC_ReadRegisters(drvConfig, BCC_CID_UNASSIG, BCC_REG_INIT_ADDR, 1U,
                &regVal);

    /* Note: in SPI communication mode the device responds with all zero and the
     * correct CRC (null response) during the very first message. */
    if ((error != BCC_STATUS_SUCCESS) && (error != BCC_STATUS_NULL_RESP))
    {
        return error;
    }

    /* Assign CID to be able to initialize next BCC device.
     * Note: It is forbidden to use global write command to assign CID (writing
     * into INIT register). */
    regVal = BCC_SET_CID(regVal, cid);

    error = BCC_WriteRegister(drvConfig, BCC_CID_UNASSIG, BCC_REG_INIT_ADDR, regVal);
    if (error != BCC_STATUS_SUCCESS)
    {
        return error;
    }

    /* Check if assigned node replies. */
    return BCC_ReadRegisters(drvConfig, cid, BCC_REG_INIT_ADDR, 1U, &regVal);
}



/*FUNCTION**********************************************************************
 *
 * Function Name : BCC_InitDevices
 * Description   : This function initializes all connected BCC devices.
 *
 *END**************************************************************************/
static bcc_status_t BCC_InitDevices(bcc_drv_config_t* const drvConfig,
    const uint16_t devConf[][BCC_INIT_CONF_REG_CNT])
{
    uint8_t i;
    bcc_status_t error;

    /* Assign CID and close bus switch of all configured BCC devices. */
    for (i = 0U; i < drvConfig->devicesCnt; i++)
    {
    	BCC_WakeUp(drvConfig);

        if ((error = BCC_AssignCid(drvConfig, BCC_CID_DEV1 + i)) != BCC_STATUS_SUCCESS)
        {
            return error;
        }
    }

    /* Initialize registers of device(s). */
    if (devConf != NULL)
    {
        if ((error = BCC_InitRegisters(drvConfig, devConf)) != BCC_STATUS_SUCCESS)
        {
            return error;
        }
    }

    return BCC_STATUS_SUCCESS;
}

/*FUNCTION**********************************************************************
 *
 * Function Name : BCC_EnterDiagnostics
 * Description   : This function enters diagnostic mode.
 *
 *END**************************************************************************/
static bcc_status_t BCC_EnterDiagnostics(bcc_drv_config_t* const drvConfig,
    bcc_cid_t cid)
{
    uint16_t retVal;    /* Value of SYS_CFG1 register. */
    bcc_status_t error;

    error = BCC_UpdateRegister(drvConfig, cid, BCC_REG_SYS_CFG1_ADDR,
                BCC_W_GO2DIAG_MASK, BCC_DIAG_MODE_ENABLED);
    if (error != BCC_STATUS_SUCCESS)
    {
        return error;
    }

    /* Check that the device is in diagnostic mode. */
    error = BCC_ReadRegisters(drvConfig, cid, BCC_REG_SYS_CFG1_ADDR, 1U, &retVal);
    if (error != BCC_STATUS_SUCCESS)
    {
        return error;
    }

    return (retVal & BCC_R_DIAG_ST_MASK) ? BCC_STATUS_SUCCESS : BCC_STATUS_DIAG_FAIL;
}  

/*FUNCTION**********************************************************************
 *
 * Function Name : BCC_ExitDiagnostics
 * Description   : This function exits diagnostic mode.
 *
 *END**************************************************************************/
static bcc_status_t BCC_ExitDiagnostics(bcc_drv_config_t* const drvConfig,
    bcc_cid_t cid)
{
    return BCC_UpdateRegister(drvConfig, cid, BCC_REG_SYS_CFG1_ADDR,
            BCC_W_GO2DIAG_MASK, BCC_DIAG_MODE_DISABLED);
}

/*FUNCTION**********************************************************************
 *
 * Function Name : BCC_RunCOD
 * Description   : This function starts on-demand conversion and waits for
 *                 completion.
 *END**************************************************************************/
static bcc_status_t BCC_RunCOD(bcc_drv_config_t* const drvConfig, bcc_cid_t cid)
{
    bool complete;      /* Conversion complete flag. */
    bcc_status_t error;

    error = BCC_StartConversion(drvConfig);
    if (error != BCC_STATUS_SUCCESS)
    {
        return error;
    }
    BCC_WaitUs(600);
    do
    {
        error = BCC_IsConverting(drvConfig, cid, &complete);
        if (error != BCC_STATUS_SUCCESS)
        {
            return error;
        }
    } while (!complete);

    return BCC_STATUS_SUCCESS;
}
  
/*FUNCTION**********************************************************************
 *
 * Function Name : BCC_CommandSwitches
 * Description   : This function commands CT or CB diagnostic switches.
 *
 *END**************************************************************************/
static bcc_status_t BCC_CommandSwitches(bcc_drv_config_t* const drvConfig,
    bcc_cid_t cid, bcc_diag_switch_sel_t switches, bcc_diag_switch_pos_t odd,
    bcc_diag_switch_pos_t even)
{
    uint16_t mask;     /* Register mask. */
    uint16_t command;  /* Command of diagnostic switches (data field of a frame). */

    /* Create content of a frame to command CB outputs */
    if (switches == BCC_SWITCH_SEL_CT)
    {
        mask = BCC_RW_CT_OL_ODD_MASK | BCC_RW_CT_OL_EVEN_MASK;
        command = (even == BCC_SWITCH_POS_OPEN) ? BCC_CT_OL_EVEN_OPEN : BCC_CT_OL_EVEN_CLOSED;
        command |= (odd == BCC_SWITCH_POS_OPEN) ? BCC_CT_OL_ODD_OPEN : BCC_CT_OL_ODD_CLOSED;
    }
    else
    {
        mask = BCC_RW_CB_OL_ODD_MASK | BCC_RW_CB_OL_EVEN_MASK;
        command = (even == BCC_SWITCH_POS_OPEN) ? BCC_CB_OL_EVEN_OPEN : BCC_CB_OL_EVEN_CLOSED;
        command |= (odd == BCC_SWITCH_POS_OPEN) ? BCC_CB_OL_ODD_OPEN : BCC_CB_OL_ODD_CLOSED;
    }

    return BCC_UpdateRegister(drvConfig, cid, BCC_REG_SYS_DIAG_ADDR, mask, command);
}

/*FUNCTION**********************************************************************
 *
 * Function Name : BCC_ConvertVoltageArray
 * Description   : This function computes voltage in uV from register raw values.
 *
 *END**************************************************************************/
static inline void BCC_ConvertVoltageArray(const uint16_t rawMeas[],
    uint8_t size, uint32_t convMeas[])
{
    uint8_t i;

    for (i = 0U; i < size; i++)
    {
        convMeas[i] = BCC_GET_VOLT(rawMeas[i]);
    }
}

/*FUNCTION**********************************************************************
 *
 * Function Name : BCC_DiagOvuvPart
 * Description   : This function is part overvoltage and undervoltage functional
 *                 verification. It commands CT switches and measures voltage on
 *                 cells.
 *
 *END**************************************************************************/
static bcc_status_t BCC_DiagOvuvPart(bcc_drv_config_t* const drvConfig,
    bcc_diag_switch_pos_t odd, bcc_diag_switch_pos_t even,
	bcc_diag_ov_uv_res_t *results)
{
    uint16_t fault[2];  /* Value of OV and UV fault registers. */
    bcc_status_t error;
    uint8_t cid = 1;

    /* Clear OV, UV faults. */
    error = BCC_WriteRegisterGlobal(drvConfig, BCC_REG_CELL_OV_FLT_ADDR, 0x0000U);
    error |= BCC_WriteRegisterGlobal(drvConfig, BCC_REG_CELL_UV_FLT_ADDR, 0x0000U);
    if (error != BCC_STATUS_SUCCESS)
    {
        return error;
    }

    /* Command CTx switches. */
    for(cid = 1;cid <= drvConfig->devicesCnt; cid++)
    {
		error = BCC_CommandSwitches(drvConfig, cid, BCC_SWITCH_SEL_CT, odd, even);
		if (error != BCC_STATUS_SUCCESS)
		{
			return error;
		}
    }

    /* Wait for 5 times diagnostic time constant. */
    BCC_WaitUs(5U * drvConfig->drvData.tauDiagn);

    /* Initiate conversion. */
    error = BCC_RunCOD(drvConfig, BCC_CID_DEV1);
    if (error != BCC_STATUS_SUCCESS)
    {
        return error;
    }

    /* Read OV & UV flags. */
    for(cid = 1;cid <= drvConfig->devicesCnt; cid++)
    {
        error = BCC_ReadRegisters(drvConfig, cid, BCC_REG_CELL_OV_FLT_ADDR, 2U, fault);

        if(even == BCC_SWITCH_POS_OPEN)
            results[cid-1].ovEven = fault[0];
        else if(even == BCC_SWITCH_POS_CLOSED)
        	results[cid-1].uvEven = fault[1];

        if(odd == BCC_SWITCH_POS_OPEN)
            results[cid-1].ovOdd = fault[0];
        else if(odd == BCC_SWITCH_POS_CLOSED)
        	results[cid-1].uvOdd = fault[1];
    }
    return error;
}

/*FUNCTION**********************************************************************
 *
 * Function Name : BCC_DiagCtxopenPart
 * Description   : This function is part of CTX open detect. It clear OV and UV
**                 fault registers, commands CT switches and measures voltage on
**                 cells.
 *
 *END**************************************************************************/
static bcc_status_t BCC_DiagCtxopenPart(bcc_drv_config_t* const drvConfig,
		bcc_diag_switch_pos_t odd, bcc_diag_switch_pos_t even, uint16_t *measVal)
{
    bcc_status_t error;
    uint8_t cid = 1;

    /* Clear OV, UV faults. */
    error = BCC_WriteRegisterGlobal(drvConfig, BCC_REG_CELL_OV_FLT_ADDR, 0x0000U);
    error |= BCC_WriteRegisterGlobal(drvConfig, BCC_REG_CELL_UV_FLT_ADDR, 0x0000U);
    if (error != BCC_STATUS_SUCCESS)
    {
        return error;
    }

    /* Command fault detection switches. */
    for(cid = 1;cid <= drvConfig->devicesCnt; cid++)
    {
		error = BCC_CommandSwitches(drvConfig, cid, BCC_SWITCH_SEL_CT, odd, even);
		if (error != BCC_STATUS_SUCCESS)
		{
			return error;
		}
    }

    /* Wait for 5 times diagnostic time constant. */
    BCC_WaitUs(5U * drvConfig->drvData.tauDiag);

    /* Initiate conversion. */
    if ((error = BCC_RunCOD(drvConfig, BCC_CID_DEV1)) != BCC_STATUS_SUCCESS)
    {
        return error;
    }

    /* Read conversion results. */
    for(cid = 1;cid <= drvConfig->devicesCnt; cid++)
    {
    	error = BCC_ReadRegisters(drvConfig, cid, BCC_REG_MEAS_CELLX_ADDR_START_MC33771,
                    BCC_MAX_CELLS_MC33771, measVal);
    	measVal += BCC_MAX_CELLS_MC33771;
		if (error != BCC_STATUS_SUCCESS)
		{
			return error;
		}
    }

    return error;
}

/*FUNCTION**********************************************************************
 *
 * Function Name : BCC_DiagRunImeas
 * Description   : This function is part of current measurement diagnostics
 *                 (the first procedure). It resets Coulomb counter, initiates
 *                 conversion and reads measured current.
 *
 *END**************************************************************************/
static bcc_status_t BCC_DiagRunImeas(bcc_drv_config_t* const drvConfig,
    bcc_cid_t cid, int32_t *current)
{
    uint16_t measCurrentRaw[2];  /* Value of MEAS_ISENSE 1 and 2 registers */
    bcc_status_t error;

    /* 7a. Write ADC_CFG[CC_RST] = 1 to reset the coulomb counter COULOMB_CNT. */
    /* Reset Coulomb counter. */
    error = BCC_UpdateRegister(drvConfig, cid, BCC_REG_ADC_CFG_ADDR, BCC_W_CC_RST_MASK, BCC_CC_RESET);
    if (error != BCC_STATUS_SUCCESS)
    {
        return error;
    }

    /* 7b. Initiate conversion. */
    error = BCC_RunCOD(drvConfig, cid);
    if (error != BCC_STATUS_SUCCESS)
    {
        return error;
    }

    /* 8. Read conversion results. */
    error = BCC_ReadRegisters(drvConfig, cid, BCC_REG_MEAS_ISENSE1_ADDR, 2U, measCurrentRaw);

    /* Extracts current from measured raw value. */
    (*current) = BCC_GET_ISENSE_VOLT(BCC_GET_ISENSE_RAW(measCurrentRaw[0], measCurrentRaw[1]));

    return error;
}

/*FUNCTION**********************************************************************
 *
 * Function Name : BCC_GpioOtUtPart
 * Description   : This function is part of GPIO OT and UT functional
 *                 verification.
 *
 *END**************************************************************************/
static bcc_status_t BCC_GpioOtUtPart(bcc_drv_config_t* const drvConfig,  uint8_t otut, bcc_diag_ot_ut_res_t *result)
{
    bcc_status_t error;
    uint8_t cid = 1;
    uint16_t gpioCFG2Val;


    /* 3. Set GPIO_CFG2[GPIOx_DR] register to drive output for overtemperature
     * or undertemperature. */
    if(otut == BCC_UT_VERIFICATION)
    {
    	gpioCFG2Val = BCC_GPIOx_HIGH(0U) | BCC_GPIOx_HIGH(1U) | BCC_GPIOx_HIGH(2U) |
                    BCC_GPIOx_HIGH(3U) | BCC_GPIOx_HIGH(4U) | BCC_GPIOx_HIGH(5U) |
                    BCC_GPIOx_HIGH(6U);

    }
    else if (otut == BCC_OT_VERIFICATION)
    {
    	gpioCFG2Val = BCC_GPIOx_LOW(0U) | BCC_GPIOx_LOW(1U) | BCC_GPIOx_LOW(2U) |
                    BCC_GPIOx_LOW(3U) | BCC_GPIOx_LOW(4U) | BCC_GPIOx_LOW(5U) |
                    BCC_GPIOx_LOW(6U);
    }
    error = BCC_WriteRegisterGlobal(drvConfig, BCC_REG_GPIO_CFG2_ADDR, gpioCFG2Val);
    if (error != BCC_STATUS_SUCCESS)
    {
        return error;
    }

    /* 5. Initiate conversion. */
    error = BCC_RunCOD(drvConfig, BCC_CID_DEV1);
    if (error != BCC_STATUS_SUCCESS)
    {
        return error;
    }

    /* 6. Evaluate / Publish results.
    * Conversion data < TH_ANx_OT -> OT (ANx_OT) and > TH_ANx_UT -> UT (ANx_UT). */
    if(otut == BCC_UT_VERIFICATION)
    {
		for(cid = 1; cid <= drvConfig->devicesCnt; cid++)
		{
			error |= BCC_ReadRegisters(drvConfig, cid, BCC_REG_AN_OT_UT_FLT_ADDR, 1U, &result[cid-1].untStat);
			if (error != BCC_STATUS_SUCCESS)
			{
				return error;
			}
		}
    }
    else if(otut == BCC_OT_VERIFICATION)
    {
		for(cid = 1; cid <= drvConfig->devicesCnt; cid++)
		{
			error |= BCC_ReadRegisters(drvConfig, cid, BCC_REG_AN_OT_UT_FLT_ADDR, 1U, &result[cid-1].ovtStat);
			if (error != BCC_STATUS_SUCCESS)
			{
				return error;
			}
		}
    }

    /* Clear OT/UT faults before exit. */
    return BCC_WriteRegisterGlobal(drvConfig, BCC_REG_AN_OT_UT_FLT_ADDR, 0x0000U);
}

/******************************************************************************
 * API
 ******************************************************************************/

/*FUNCTION**********************************************************************
 *
 * Function Name : BCC_Init
 * Description   : This function initializes the Battery Cell Controller
 *                 device(s), configures its registers, assigns CID and
 *                 initializes internal driver data.
 *
 *END**************************************************************************/
bcc_status_t BCC_Init(bcc_drv_config_t* const drvConfig,
    const uint16_t devConf[][BCC_INIT_CONF_REG_CNT])
{
    bcc_status_t error;
    uint8_t dev;
    uint8_t cell;
    uint8_t cid;

    if ((drvConfig->devicesCnt == 0) ||(drvConfig->devicesCnt > ((drvConfig->commMode == BCC_MODE_SPI) ? BCC_DEVICE_CNT_MAX_SPI : BCC_DEVICE_CNT_MAX_TPL)))
    {
        return BCC_STATUS_PARAM_RANGE;
    }
    /*calculate constant τ for Diag safety mechanism 1 and 2*/
    if (drvConfig->compConfig.ctFilterComp != NULL)
    {
        BCC_CalcTauDiag(drvConfig);
    }
    /*mask un used cells*/
    for (dev = 0; dev < drvConfig->devicesCnt; dev++)
    {

		drvConfig->drvData.cellMap[dev] = BCC_CM_MC33771_7CELLS;
		for (cell = BCC_MIN_CELLS_MC33771; cell < drvConfig->cellCnt[dev]; cell++)
		{
			drvConfig->drvData.cellMap[dev] |= (0x0400U >> (cell - BCC_MIN_CELLS_MC33771));
		}

    }

    /* Initialize TAG ID. */
    for (cid = 0; cid < drvConfig->devicesCnt; cid++)
    {
        drvConfig->drvData.rcTbl[cid] = 0U;
    }
    drvConfig->drvData.tagId = 0U;

    /* RESET -> 0, if SPI mode used */
    BCC_WriteRstPin(drvConfig->drvInstance, 0);

    if (drvConfig->commMode == BCC_MODE_TPL)
    {
        /* EN -> 0 */
        BCC_WriteEnPin(drvConfig->drvInstance, 0);
    }

    /* Wake-up BCC (in case of IDLE mode). */
    if ((error = BCC_WakeUp(drvConfig)) != BCC_STATUS_SUCCESS)
    {
        return error;
    }

    /* Reset (soft reset) BCC to assign CID (in case of CID was already assigned). */
    (void)BCC_SoftwareReset(drvConfig);

    /* Wait for 5 ms (tvpwr(ready)) - for the IC to be ready for initialization. */
    BCC_WaitMs(5U);

    return BCC_InitDevices(drvConfig, devConf);
}

/*FUNCTION**********************************************************************
 *
 * Function Name : BCC_WriteRegister
 * Description   : This function writes a value to addressed register of
 *                 selected Battery Cell Controller device.
 *
 *END**************************************************************************/
bcc_status_t BCC_WriteRegister(bcc_drv_config_t* const drvConfig, bcc_cid_t cid, uint8_t regAddr, uint16_t regVal)
{

    if (drvConfig->commMode == BCC_MODE_SPI)
    {
        return BCC_WriteRegisterSpi(drvConfig, cid, regAddr, regVal);
    }
    else
    {
        return BCC_WriteRegisterTpl(drvConfig, cid, regAddr, regVal);
    }
}

/*FUNCTION**********************************************************************
 *
 * Function Name : BCC_WriteRegisterGlobal
 * Description   : This function writes a value to addressed register of all
 *                 configured BCC devices. Intended for TPL mode only.
 *
 *END**************************************************************************/
bcc_status_t BCC_WriteRegisterGlobal(bcc_drv_config_t* const drvConfig,
     uint8_t regAddr, uint16_t regVal)
{

    return BCC_WriteRegisterGlobalTpl(drvConfig, regAddr, regVal);
}

/*FUNCTION**********************************************************************
 *
 * Function Name : BCC_ReadRegisters
 * Description   : This function reads a value from addressed register (or desired
 *                 number of registers) of selected Battery Cell Controller device.
 *
 *END**************************************************************************/
bcc_status_t BCC_ReadRegisters(bcc_drv_config_t* const drvConfig, bcc_cid_t cid,
    uint8_t regAddr, uint8_t regCnt, uint16_t* regVal)
{
    uint8_t curRegCnt = regCnt;    /* Current number of registers to read. */
    bcc_status_t error;

    if (drvConfig->commMode == BCC_MODE_SPI)
    {
        return BCC_ReadRegistersSpi(drvConfig, cid, regAddr, regCnt, regVal);
    }
    else
    {

            error = BCC_ReadRegistersTpl_Sync(drvConfig, cid, regAddr, curRegCnt, regVal);
            if (error != BCC_STATUS_SUCCESS)
            {
                return error;
            }
    }

    return BCC_STATUS_SUCCESS;
}

/*FUNCTION**********************************************************************
 *
 * Function Name : BCC_ReadRegisters_Async
 * Description   : This function sends read command to get a value from addressed register (or desired
 *                 number of registers) of selected Battery Cell Controller device.
 *                 And return result after command sent immediately.
 *END**************************************************************************/
bcc_status_t BCC_ReadRegisters_Async(bcc_drv_config_t* const drvConfig, bcc_cid_t cid,
    uint8_t regAddr, uint8_t regCnt, uint16_t regVal[][112])
{
    bcc_status_t error;

    RDCMD_Pack.drvConfig =  drvConfig;
    RDCMD_Pack.cid = cid;
    RDCMD_Pack.regAddr = regAddr;
    RDCMD_Pack.regCnt = regCnt;
    RDCMD_Pack.regVal = regVal;


    error = BCC_ReadRegistersTpl_Async(RDCMD_Pack.drvConfig, RDCMD_Pack.cid, RDCMD_Pack.regAddr, RDCMD_Pack.regCnt, RDCMD_Pack.regVal);

    return error;
}

/*FUNCTION**********************************************************************
 *
 * Function Name : BCC_ReadRegisters_AsyncStatus
 * Description   : This function returns the result whether the previous async
 *                 TPL transfer is completed. If TPL transfer did not success,
 *                 abort the transfer and return error.
 *END**************************************************************************/

bcc_status_t BCC_ReadRegisters_AsyncStatus(void)
{
	bcc_status_t error;

	error = BCC_GetTransferTpl_Status();

	return error;
}

/*FUNCTION**********************************************************************
 *
 * Function Name : BCC_Receive_EndNotification
 * Description   : This function is called once async TPL transfer is completed and
 *                 registers content are already stored. If the "auto_enable" is set 1,
 *                 an asynchronous read command will be sent for next MC33771C until
 *                 the last AFE.
 *END**************************************************************************/
void BCC_Receive_EndNotification(void *driverState, spi_event_t event, void *userData)
{
	bcc_status_t error;

	RDCMD_Pack.NoResponse_Cnt = 0;
	RDCMD_Pack.Async_Transfer_running = false;
	if(RDCMD_Pack.Async_Transfer_enbale)
	{
		error = BCC_Check_ReadRegistersTpl(RDCMD_Pack.drvConfig, RDCMD_Pack.cid,
				RDCMD_Pack.regAddr,  RDCMD_Pack.regCnt, (RDCMD_Pack.regVal + (RDCMD_Pack.cid-1)*112));

		RDCMD_Pack.error[RDCMD_Pack.cid-1] = error;

		if(RDCMD_Pack.error[RDCMD_Pack.cid-1] ==  BCC_STATUS_SUCCESS)
		{

			if(RDCMD_Pack.auto_enable)
			{
				/*if last CID is not read, increase CID number*/
				RDCMD_Pack.cid = (RDCMD_Pack.cid < RDCMD_Pack.drvConfig->devicesCnt)? (RDCMD_Pack.cid+1):1;
				/*stop TPL Async transfer*/
				if(RDCMD_Pack.cid == 1)
				{
					RDCMD_Pack.Async_Transfer_enbale = false;
					RDCMD_Pack.auto_enable = false;
				}
				/*continue TPL Aync transfer */
				else
				{
					RDCMD_Pack.error[RDCMD_Pack.cid-1] = BCC_ReadRegistersTpl_Async(RDCMD_Pack.drvConfig,RDCMD_Pack.cid,
							RDCMD_Pack.regAddr, RDCMD_Pack.regCnt, (RDCMD_Pack.regVal + (RDCMD_Pack.cid-1)*112));
					RDCMD_Pack.Async_Transfer_running = true;
				}
			}
		}
	}

}

/*FUNCTION**********************************************************************
 *
 * Function Name : BCC_UpdateRegister
 * Description   : This function updates content of a selected register. It
 *                 affects bits specified by a bit mask only.
 *
 *END**************************************************************************/
bcc_status_t BCC_UpdateRegister(bcc_drv_config_t* const drvConfig,
    bcc_cid_t cid, uint8_t regAddr, uint16_t regMask, uint16_t regVal)
{
    uint16_t regValTemp;
    bcc_status_t error;

    if (cid > drvConfig->devicesCnt)
    {
        return BCC_STATUS_PARAM_RANGE;
    }

    error = BCC_ReadRegisters(drvConfig, cid, regAddr, 1U, &regValTemp);
    if (error != BCC_STATUS_SUCCESS)
    {
        return error;
    }

    /* Update register value. */
    regValTemp = BCC_REG_UNSET_BIT_VALUE(regValTemp, regMask);
    regValTemp = BCC_REG_SET_BIT_VALUE(regValTemp, (regVal & regMask));

    return BCC_WriteRegister(drvConfig, cid, regAddr, regValTemp);
}

/*FUNCTION**********************************************************************
 *
 * Function Name : BCC_Sleep
 * Description   : This function sets sleep mode to all Battery Cell Controller
 *                 devices.
 *
 *END**************************************************************************/
bcc_status_t BCC_Sleep(bcc_drv_config_t* const drvConfig)
{
    bcc_status_t error;

    if (drvConfig->commMode == BCC_MODE_SPI)
    {
        return BCC_WriteRegister(drvConfig, BCC_CID_DEV1, BCC_REG_SYS_CFG_GLOBAL_ADDR, BCC_GO2SLEEP_ENABLED);
    }
    else
    {
        error = BCC_WriteRegisterGlobal(drvConfig, BCC_REG_SYS_CFG_GLOBAL_ADDR,
                    BCC_GO2SLEEP_ENABLED);

        /* Set MC33664 sleep mode. */
        BCC_WriteEnPin(drvConfig->drvInstance, 0);

        return error;
    }
}

/*FUNCTION**********************************************************************
 *
 * Function Name : BCC_WakeUp
 * Description   : This function sets normal mode to all Battery Cell Controller
 *                 devices.
 *
 *END**************************************************************************/
bcc_status_t BCC_WakeUp(bcc_drv_config_t *drvConfig)
{
    bcc_status_t error;

    BCC_CS2GPIO();
    if (drvConfig->commMode == BCC_MODE_SPI)
    {
        /* A transition of CSB from low to high. */
        /* CSB_TX low. */
        BCC_WriteCsbPin(drvConfig->drvInstance, 0);
        /* Wait for t1; 20 us < t1. */
        BCC_WaitUs(20U);

        /* CSB_TX high. */
        BCC_WriteCsbPin(drvConfig->drvInstance, 1);
        /* Wait for t2; 500 us < t2. */
        BCC_WaitUs(600U);
    }
    /*TPL mode*/
    else
    {
        /* Enable TPL device. */
        if ((error = BCC_EnableTPL(drvConfig)) != BCC_STATUS_SUCCESS)
        {
            return error;
        }

        /* Two consecutive transitions of CSB_TX from low to high. */
        /* CSB_TX low. */
        BCC_WriteCsbPin(drvConfig->drvInstance, 0);
        /* Wait for t1; 20 us < t1. */
        BCC_WaitUs(20U);

        /* CSB_TX high. */
        BCC_WriteCsbPin(drvConfig->drvInstance, 1);
        /* Wait for t2; 500 us < t2. */
        BCC_WaitUs(600U);

        /* CSB_TX low. */
        BCC_WriteCsbPin(drvConfig->drvInstance, 0);
        /* Wait for t1; 20 us < t1. */
        BCC_WaitUs(20U);

        /* CSB_TX high. */
        BCC_WriteCsbPin(drvConfig->drvInstance, 1);
        /* Only for sure. */
        BCC_WaitUs(1200U);
    }
    BCC_GPIO2CS();
    return BCC_STATUS_SUCCESS;
}

/*FUNCTION**********************************************************************
 *
 * Function Name : BCC_StartConversion
 * Description   : This function starts ADC conversion. It sets Start of
 *                 Conversion bit and new value of TAG ID in ADC_CFG register.
 *
 *END**************************************************************************/
bcc_status_t BCC_StartConversion(bcc_drv_config_t* const drvConfig)
{
    uint16_t regVal;     /* Value of ADC_CFG register. */

    regVal = BCC_CONF1_ADC_CFG_VALUE;
    /* Increment TAG ID (4 bit value). */
    drvConfig->drvData.tagId = (drvConfig->drvData.tagId + 1U) & 0x0FU;

    /* Set new TAG ID and Start of Conversion bit. */
    regVal = BCC_SET_TAG_ID(regVal, drvConfig->drvData.tagId);
    regVal = BCC_REG_SET_BIT_VALUE(regVal, BCC_W_SOC_MASK);

    if(drvConfig->commMode == BCC_MODE_TPL)
        return BCC_WriteRegisterGlobal(drvConfig, BCC_REG_ADC_CFG_ADDR, regVal);

    else if (drvConfig->commMode == BCC_MODE_SPI)
    	return BCC_WriteRegister(drvConfig, BCC_CID_DEV1, BCC_REG_ADC_CFG_ADDR, regVal);
}

/*FUNCTION**********************************************************************
 *
 * Function Name : BCC_IsConverting
 * Description   : This function checks status of conversion defined by End of
 *                 Conversion bit in ADC_CFG register.
 *
 *END**************************************************************************/
bcc_status_t BCC_IsConverting(bcc_drv_config_t* const drvConfig, bcc_cid_t cid,
    bool* completed)
{
    uint16_t regVal;     /* Value of ADC_CFG register. */
    bcc_status_t error;

    if ((cid == BCC_CID_UNASSIG) || (cid > drvConfig->devicesCnt))
    {
        return BCC_STATUS_PARAM_RANGE;
    }

    error = BCC_ReadRegisters(drvConfig, cid, BCC_REG_ADC_CFG_ADDR, 1U, &regVal);

    regVal = regVal & BCC_R_EOC_N_MASK;
    *(completed) = (regVal == 0x00U);

    return error;
}

/*FUNCTION**********************************************************************
 *
 * Function Name : BCC_GetRawMeasurements
 * Description   : This function reads the measurement registers and returns raw
 *                 values. You can use macros defined in BCC header file to
 *                 perform correct unit conversion.
 *
 *END**************************************************************************/
bcc_status_t BCC_GetRawMeasurements(bcc_drv_config_t* const drvConfig, bcc_cid_t cid)
{
    bcc_status_t error;
    uint8_t i;
    uint16_t measurements[30];

    if ((cid == BCC_CID_UNASSIG) || (cid > drvConfig->devicesCnt)||(drvConfig->device[cid - 1] != BCC_DEVICE_MC33771))
    {
        return BCC_STATUS_PARAM_RANGE;
    }

    /* Read all the measurement registers.
    * Note: the order and number of registers conforms to the order of measured
    * values in Measurements array, see enumeration bcc_measurements_t. */

    error = BCC_ReadRegisters(drvConfig, cid, BCC_REG_CC_NB_SAMPLES_ADDR, BCC_MEAS_CNT, &measurements[0]);

    /* Mask bits. */
    /* Nothing to mask in CC_NB_SAMPLES, COULOMB_CNT1 and COULOMB_CNT2
     * registers. */
    BatPack.u16CCSamples[cid] = measurements[BCC_MSR_CC_NB_SAMPLES];
    BatPack.s32CCCounter[cid ]=(measurements[BCC_MSR_COULOMB_CNT1]<<16)|measurements[BCC_MSR_COULOMB_CNT2];
    measurements[BCC_MSR_ISENSE1] &= BCC_R_MEAS1_I_MASK;
    measurements[BCC_MSR_ISENSE2] &= BCC_R_MEAS2_I_MASK;

    /*store I_SENSEN measurent voltage raw value*/
	BatPack.i32RawCurrent[cid] = BCC_GET_ISENSE_RAW_SIGN(BCC_GET_ISENSE_RAW(measurements[BCC_MSR_ISENSE1], measurements[BCC_MSR_ISENSE2]));

    /* Mask the other registers (starting at 5th register). */
    for (i = 5U; i < BCC_MEAS_CNT; i++)
    {
        BatPack.u16RawCell[cid][i-5] =(measurements[i] & BCC_R_MEAS_MASK);
    }

    return error;
}

/*******************************************************************************
* convert the raw value to true voltage value
******************************************************************************/
void BCC_DecodeRawMeasurements(bcc_cid_t cid)
{
	uint8_t idx = 0;

	BatPack.i32Current[cid] = BCC_GET_ISENSE_VOLT(BatPack.i32RawCurrent[cid]&BIT(18));
	for(idx = 0; idx<25; idx++){
		if(idx==0)
			BatPack.u16VoltCell[cid][idx] = BCC_GET_STACK_VOLT(BatPack.u16RawCell[cid][idx]);
		else if(idx < 22)
			BatPack.u16VoltCell[cid][idx] = BCC_GET_VOLT(BatPack.u16RawCell[cid][idx]);
		else if(idx == 22)
			BatPack.u16Temp[cid] = BCC_GET_IC_TEMP(BatPack.u16RawCell[cid][idx]);
		else if(idx<25)
			BatPack.u16VbgADC1[cid][idx-23] = BCC_GET_VOLT(BatPack.u16RawCell[cid][idx]);
	}
}

/*FUNCTION**********************************************************************
 *
 * Function Name : BCC_GetFaultStatus
 * Description   : This function reads the status registers and returns raw
 *                 values. You can use constants defined in bcc_mc3377x.h file.
 *
 *END**************************************************************************/
bcc_status_t BCC_GetFaultStatus(bcc_drv_config_t* const drvConfig,
    bcc_cid_t cid, uint16_t status[][11])
{
    bcc_status_t error;
    uint8_t FaultAddr[10]={
    		BCC_REG_CELL_OV_FLT_ADDR, BCC_REG_CELL_UV_FLT_ADDR,BCC_REG_CB_OPEN_FLT_ADDR,
			BCC_REG_CB_SHORT_FLT_ADDR,BCC_REG_AN_OT_UT_FLT_ADDR,BCC_REG_GPIO_SHORT_ADDR,
			BCC_REG_COM_STATUS_ADDR,BCC_REG_FAULT1_STATUS_ADDR,
			BCC_REG_FAULT2_STATUS_ADDR,BCC_REG_FAULT3_STATUS_ADDR
    };
    uint8_t i;

    if ((cid == BCC_CID_UNASSIG) || (cid > drvConfig->devicesCnt))
    {
        return BCC_STATUS_PARAM_RANGE;
    }
    for(i = 0; i<10;i++){
    	error = BCC_ReadRegisters(drvConfig, cid, FaultAddr[i], 1U, &status[cid][i]);
    	if(error)
    		break;
    }
    return error;
}

/*FUNCTION**********************************************************************
 *
 * Function Name : BCC_ClearFaultStatus
 * Description   : This function clears selected fault status register.
 *
 *END**************************************************************************/
bcc_status_t BCC_ClearFaultStatus(bcc_drv_config_t* const drvConfig, bcc_cid_t cid)
{
    /* This array is intended for conversion of bcc_fault_status_t value to
     * a BCC register address. */
	bcc_status_t error;
	uint8_t regidx;
    const uint8_t REG_ADDR_MAP[BCC_STAT_CNT] = {
        BCC_REG_CELL_OV_FLT_ADDR, BCC_REG_CELL_UV_FLT_ADDR,
        BCC_REG_CB_OPEN_FLT_ADDR, BCC_REG_CB_SHORT_FLT_ADDR,
        BCC_REG_AN_OT_UT_FLT_ADDR,BCC_REG_GPIO_SHORT_ADDR,
         BCC_REG_COM_STATUS_ADDR,BCC_REG_FAULT1_STATUS_ADDR,
         BCC_REG_FAULT2_STATUS_ADDR,BCC_REG_FAULT3_STATUS_ADDR
    };

    if ((cid == BCC_CID_UNASSIG) || (cid > drvConfig->devicesCnt))
    {
        return BCC_STATUS_PARAM_RANGE;
    }

    for(regidx = 0; regidx <10; regidx++){
    	error = BCC_WriteRegister(drvConfig, cid, REG_ADDR_MAP[regidx], 0x00U);
    	if(error!=BCC_STATUS_SUCCESS)
    		return error;
    }

    return BCC_STATUS_SUCCESS;
}

/*FUNCTION**********************************************************************
 *
 * Function Name : BCC_SoftwareReset
 * Description   : This function resets BCC device using software reset. It
 *                 enters reset via SPI or TPL interface.
 *
 *END**************************************************************************/
bcc_status_t BCC_SoftwareReset(bcc_drv_config_t* const drvConfig)
{
    bcc_status_t error;

    /* Note: it is not necessary to read content of SYS_CFG1 register
    * to change only RST bit, because registers are set to default values. */

    if (drvConfig->commMode == BCC_MODE_TPL)
    {
        /* TPL Global reset command. */
        error = BCC_WriteRegisterGlobal(drvConfig, BCC_REG_SYS_CFG1_ADDR, BCC_W_SOFT_RST_MASK);
    }
    else
    {
        error = BCC_WriteRegister(drvConfig, BCC_CID_DEV1, BCC_REG_SYS_CFG1_ADDR, BCC_W_SOFT_RST_MASK);
    }

    return error;
}

/*FUNCTION**********************************************************************
 *
 * Function Name : BCC_HardwareReset
 * Description   : This function resets BCC device using GPIO pin.
 *
 *END**************************************************************************/
void BCC_HardwareReset(bcc_drv_config_t* const drvConfig)
{
    BCC_WriteRstPin(drvConfig->drvInstance, 1);
    /* Wait at least tRESETFLT (100 us). */
    BCC_WaitUs(100);
    BCC_WriteRstPin(drvConfig->drvInstance, 0);
}

/*FUNCTION**********************************************************************
 *
 * Function Name : BCC_SetGPIOOutput
 * Description   : This function sets output value of one BCC GPIO pin.
 *
 *END**************************************************************************/
bcc_status_t BCC_SetGPIOOutput(bcc_drv_config_t* const drvConfig, uint8_t cid,
        uint8_t gpioSel, bool val)
{
    uint16_t regVal;    /* Value of GPIO_CFG2 register. */
    bcc_status_t error;

    if ((cid == BCC_CID_UNASSIG) || (cid > drvConfig->devicesCnt) ||
        (gpioSel >= BCC_GPIO_INPUT_CNT))
    {
        return BCC_STATUS_PARAM_RANGE;
    }

    /* Read and update content of GPIO_CFG2 register. */
    error = BCC_ReadRegisters(drvConfig, cid, BCC_REG_GPIO_CFG2_ADDR, 1U, &regVal);
    if (error != BCC_STATUS_SUCCESS)
    {
        return error;
    }

    /* Set GPIO output value. */
    regVal &= ~((uint16_t)BCC_RW_GPIOX_DR_MASK(gpioSel));
    regVal |= (val) ? BCC_GPIOx_HIGH(gpioSel) : BCC_GPIOx_LOW(gpioSel);

    return BCC_WriteRegister(drvConfig, cid, BCC_REG_GPIO_CFG2_ADDR, regVal);
}

/*FUNCTION**********************************************************************
 *
 * Function Name : BCC_SetCBIndividually
 * Description   : This function sets state of individual cell balancing driver.
 *
 *END**************************************************************************/
bcc_status_t BCC_SetCBIndividually(bcc_drv_config_t* const drvConfig,
    uint8_t cid, uint8_t cellIndex, bool enable, uint16_t timer)
{
    bcc_status_t error = BCC_STATUS_SUCCESS;
    uint16_t regVal;

    if ((cid == BCC_CID_UNASSIG) || (cid > drvConfig->devicesCnt))
    {
        return BCC_STATUS_PARAM_RANGE;
    }

    if (cellIndex > BCC_MAX_CELLS_MC33771)
    {
        return BCC_STATUS_PARAM_RANGE;
    }
    /*maximum timer is 511 min*/
    if(timer > 0x1FF)
    {
    	timer = 0x1FF;
    }

    if (enable)
    {
        if (BCC_IS_CELL_CONN(drvConfig, cid, cellIndex + 1U))
        {
        	regVal = BCC_SET_CB_TIMER(regVal, timer);
        	regVal|= BCC_CB_ENABLED;
            error = BCC_WriteRegister(drvConfig, cid, BCC_REG_CB1_CFG_ADDR + cellIndex - 1,regVal);
        }
    }
    else
    {
        /* Disable individual CB driver with use of CBx_CFG[CB_EN]. */
        error = BCC_UpdateRegister(drvConfig, cid, BCC_REG_CB1_CFG_ADDR + cellIndex - 1,
                    BCC_W_CB_EN_MASK, BCC_CB_DISABLED);
    }

    return error;
}

/*FUNCTION**********************************************************************
 *
 * Function Name : BCC_GetCBStatus
 * Description   : This function checks status cell balance by reading
 *                 CB_DRV_STS register.
 *
 *END**************************************************************************/
bcc_status_t BCC_GetCBStatus(bcc_drv_config_t* const drvConfig, bcc_cid_t cid,
		uint16_t *status)
{
    uint16_t regVal;     /* Value of ADC_CFG register. */
    bcc_status_t error;

    if ((cid == BCC_CID_UNASSIG) || (cid > drvConfig->devicesCnt))
    {
        return BCC_STATUS_PARAM_RANGE;
    }

    error = BCC_ReadRegisters(drvConfig, cid, BCC_REG_CB_DRV_STS_ADDR, 1U, &regVal);

    *(status) = regVal;

    return error;
}

/*FUNCTION**********************************************************************
 *
 * Function Name : BCC_GetNtcCelsius
 * Description   : This function calculates temperature from raw value of
 *                 MEAS_ANx register.
 *
 *END**************************************************************************/
bcc_status_t BCC_GetNtcCelsius(bcc_drv_config_t* const drvConfig,
    uint16_t regVal, int16_t* temp)
{
    int16_t left = 0;    /* Pointer (index) to the left border of interval (NTC table). */
    /* Pointer (index) to the right border of interval (NTC table). */
    int16_t right = BCC_NTC_TABLE_SIZE - 1;
    int16_t middle;      /* Pointer (index) to the middle of interval (NTC table). */
    int8_t degTenths;    /* Fractional part of temperature value. */

    /* Check range of NTC table. */
    if (g_ntcTable[BCC_NTC_TABLE_SIZE - 1] > regVal)
    {
        *temp = BCC_COMP_TEMP(BCC_NTC_TABLE_SIZE - 1, 0);
        return BCC_STATUS_PARAM_RANGE;
    }
    if (g_ntcTable[0] < regVal)
    {
        *temp = BCC_COMP_TEMP(0, 0);
        return BCC_STATUS_PARAM_RANGE;
    }

    regVal &= BCC_GET_MEAS_RAW(regVal);

    /* Search for an array item which is close to the register value provided
    * by user (regVal). Used method is binary search in sorted array. */
    while ((left + 1) != right)
    {
        /* Split interval into halves. */
        middle = (left + right) >> 1U;
        if (g_ntcTable[middle] <= regVal)
        {
            /* Select right half (array items are in descending order). */
            right = middle;
        }
        else
        {
            /* Select left half. */
            left = middle;
        }
    }

    /* Notes: found table item (left) is less than the following item in the
    * table (left + 1).
    * The last item cannot be found (algorithm property). */

    /* Calculate fractional part of temperature. */
    degTenths = (g_ntcTable[left] - regVal) / ((g_ntcTable[left] - g_ntcTable[left + 1]) / 10);
    (*temp) = BCC_COMP_TEMP(left, degTenths);

    return BCC_STATUS_SUCCESS;
}

/*FUNCTION**********************************************************************
 *
 * Function Name : BCC_PauseCBDrivers
 * Description   : This function can be used to manual pause cell balancing before
 *                 on demand conversion.
 *
 *END**************************************************************************/
bcc_status_t BCC_PauseCBDrivers(bcc_drv_config_t* const drvConfig, bcc_cid_t cid, bool pause)
{
    uint16_t regVal = (pause) ? BCC_CB_MAN_PAUSE_ENABLED : BCC_CB_MAN_PAUSE_DISABLED;

    if ((cid == BCC_CID_UNASSIG) || (cid > drvConfig->devicesCnt))
    {
        return BCC_STATUS_PARAM_RANGE;
    }

    return BCC_UpdateRegister(drvConfig, cid, BCC_REG_SYS_CFG1_ADDR,
                BCC_RW_CB_MANUAL_PAUSE_MASK, regVal);
}

/*FUNCTION**********************************************************************
 *
 * Function Name : BCC_CheckCid
 * Description   : This function read INIT register of NO.cid device to
 *                 ensure NO.cid device communication status normally.
 *
 *END**************************************************************************/

bcc_status_t BCC_CheckCid(bcc_drv_config_t* const drvConfig, bcc_cid_t cid)
{
    uint16_t regVal = 0U;   /* Value of a register. */
    bcc_status_t error = BCC_STATUS_SUCCESS;

    /* Check if assigned node replies. */
    error = BCC_ReadRegisters(drvConfig, cid, BCC_REG_INIT_ADDR, 1U, &regVal);

    return error;
}

/*FUNCTION**********************************************************************
 *
 * Function Name : BCC_GetCtrlRegMap
 * Description   : This function read some configurable register of NO.cid device.
 *
 *END**************************************************************************/
bcc_status_t BCC_GetCtrlRegMap(bcc_drv_config_t* const drvConfig, bcc_cid_t cid,
		uint16_t StatusPtr[][31], uint8_t *AddrMap)
{

	bcc_status_t error = BCC_STATUS_SUCCESS;
    uint8_t index = 0;
    uint8_t RegMap[31] ={
    		BCC_REG_INIT_ADDR, BCC_REG_SYS_CFG_GLOBAL_ADDR, BCC_REG_SYS_CFG1_ADDR,
			BCC_REG_SYS_CFG2_ADDR, BCC_REG_SYS_DIAG_ADDR, BCC_REG_ADC_CFG_ADDR,
			BCC_REG_OV_UV_EN_ADDR, BCC_REG_GPIO_CFG1_ADDR, BCC_REG_GPIO_CFG2_ADDR,
			BCC_REG_FAULT_MASK1_ADDR, BCC_REG_FAULT_MASK2_ADDR, BCC_REG_FAULT_MASK3_ADDR,
			BCC_REG_WAKEUP_MASK1_ADDR, BCC_REG_WAKEUP_MASK2_ADDR, BCC_REG_WAKEUP_MASK3_ADDR,
			BCC_REG_TPL_CFG_ADDR,BCC_REG_ADC2_OFFSET_COMP_ADDR, BCC_REG_CB1_CFG_ADDR, BCC_REG_CB2_CFG_ADDR,
			BCC_REG_CB3_CFG_ADDR, BCC_REG_CB4_CFG_ADDR, BCC_REG_CB5_CFG_ADDR,
			BCC_REG_CB6_CFG_ADDR, BCC_REG_CB7_CFG_ADDR, BCC_REG_CB8_CFG_ADDR,
			BCC_REG_CB9_CFG_ADDR, BCC_REG_CB10_CFG_ADDR, BCC_REG_CB11_CFG_ADDR,
			BCC_REG_CB12_CFG_ADDR, BCC_REG_CB13_CFG_ADDR, BCC_REG_CB14_CFG_ADDR,

    };

    if (StatusPtr == NULL) {
        return BCC_STATUS_PARAM_RANGE;
    }

    for(index = 0; index <31; index++){
    	AddrMap[index] = RegMap[index];
    	error = BCC_ReadRegisters(drvConfig, cid, RegMap[index], 1U, &StatusPtr[cid-1][index]);
    	if(error!= BCC_STATUS_SUCCESS)
    		return error;
    }

    return error;
}

/*FUNCTION**********************************************************************
 *
 * Function Name : BCC_GetCtrlRegMap
 * Description   : This function read some status register of NO.cid device.
 *
 *END**************************************************************************/
bcc_status_t BCC_GetSTSRegMap(bcc_drv_config_t* const drvConfig, bcc_cid_t cid,
		uint16_t StatusPtr[][15], uint8_t *AddrMap)
{
	bcc_status_t error = BCC_STATUS_SUCCESS;
    uint8_t index = 0;
    uint8_t RegMap[15] ={
    		BCC_REG_CELL_OV_FLT_ADDR, BCC_REG_CELL_UV_FLT_ADDR, BCC_REG_CB_OPEN_FLT_ADDR,
			BCC_REG_CB_SHORT_FLT_ADDR, BCC_REG_CB_DRV_STS_ADDR, BCC_REG_GPIO_STS_ADDR,
			BCC_REG_AN_OT_UT_FLT_ADDR, BCC_REG_GPIO_SHORT_ADDR, BCC_REG_I_STATUS_ADDR,
			BCC_REG_COM_STATUS_ADDR, BCC_REG_FAULT1_STATUS_ADDR, BCC_REG_FAULT2_STATUS_ADDR,
			BCC_REG_FAULT3_STATUS_ADDR, BCC_REG_MEAS_ISENSE2_ADDR, BCC_REG_SILICON_REV_ADDR,
    };

    if (StatusPtr == NULL) {
        return BCC_STATUS_PARAM_RANGE;
    }

    for(index = 0; index <15; index++){
    	AddrMap[index] = RegMap[index];
    	error = BCC_ReadRegisters(drvConfig, cid, RegMap[index], 1U, &StatusPtr[cid-1][index]);
    	if(error!= BCC_STATUS_SUCCESS)
    		return error;
    }

    return error;
}

/*FUNCTION**********************************************************************
 *
 * Function Name : BCC_Configfuse_I
 * Description   : This function provide example to calibrate I_PGA by change
 *                 fuse data.
 *END**************************************************************************/
bcc_status_t BCC_Configfuse_I(bcc_drv_config_t* const drvConfig, bcc_cid_t cid, uint8_t cali_data)
{

	uint16_t Regval = 0, idx = 0;
	uint16_t fusedata[24], fusedata1[24];
	uint16_t temp;
	bcc_status_t Error = BCC_STATUS_SUCCESS;

	/*1. set SYS_CFG2[HAMMENCOD] bit 1*/
	Error = BCC_UpdateRegister(drvConfig, cid, BCC_REG_SYS_CFG2_ADDR, BCC_RW_HAMM_ENCOD_MASK, BCC_HAMM_ENCOD_ENABLED);
	if(Error != BCC_STATUS_SUCCESS)
		return Error;

	/*2. Read all data from fuse mirror*/
	for(idx = 0; idx<=23; idx++){
		Regval = ((idx<<BCC_RW_FMR_ADDR_SHIFT)|BCC_FSTM_WRITE_DIS);
		Error = BCC_WriteRegister(drvConfig, cid, BCC_REG_FUSE_MIRROR_CTRL_ADDR,Regval);
		Error |= BCC_ReadRegisters(drvConfig, cid, BCC_REG_FUSE_MIRROR_DATA_ADDR, 1U, &fusedata[idx]);
		if(Error != BCC_STATUS_SUCCESS)
			return Error;
	}

	/*2. Write all data into fuse mirror*/
	Regval = ((0<<BCC_RW_FMR_ADDR_SHIFT)|BCC_FSTM_WRITE_EN);
	Error = BCC_WriteRegister(drvConfig, 1, BCC_REG_FUSE_MIRROR_CTRL_ADDR,Regval);
	for(idx = 0; idx<=21; idx++){
		/*calculate new fuse_data for I_PGA*/
		if((idx>=17)&&(idx<=20)){
			temp = fusedata[idx]&0x1FF;
			temp = (temp + cali_data)&0x1FF;
			fusedata1[idx] = (fusedata[idx]&0xFE00)|temp;
		}
		else
			fusedata1[idx] = fusedata[idx];
		/*2. Write into fuse mirror*/
		Regval = ((idx<<BCC_RW_FMR_ADDR_SHIFT)|BCC_FSTM_WRITE_EN);
		Error = BCC_WriteRegister(drvConfig,cid, BCC_REG_FUSE_MIRROR_CTRL_ADDR,Regval);
		Error |= BCC_WriteRegister(drvConfig, cid, BCC_REG_FUSE_MIRROR_DATA_ADDR, fusedata1[idx]);
		if(Error != BCC_STATUS_SUCCESS)
			return Error;
	}
	BCC_WaitUs(1000);
	/*3. Read DED_ENCODE1 and DED_ENCODE2 */
	Error = BCC_ReadRegisters(drvConfig, cid, BCC_REG_DED_ENCODE1_ADDR, 1U, &fusedata1[22]);
	Error |= BCC_ReadRegisters(drvConfig, cid, BCC_REG_DED_ENCODE2_ADDR, 1U, &fusedata1[23]);
	/*4. Write new DED_ENCODE1 and DED_ENCODE2 into fuse mirror*/
	Regval = ((22<<BCC_RW_FMR_ADDR_SHIFT)|BCC_FSTM_WRITE_EN);
	Error |= BCC_WriteRegister(drvConfig,cid, BCC_REG_FUSE_MIRROR_CTRL_ADDR,Regval);
	Delayus(10);
	Error |= BCC_WriteRegister(drvConfig, cid, BCC_REG_FUSE_MIRROR_DATA_ADDR, fusedata1[23]);

	Regval = ((23<<0x08)|(1<<0x03));
	Error |= BCC_WriteRegister(drvConfig,1, BCC_REG_FUSE_MIRROR_CTRL_ADDR,Regval);
	Delayus(10);
	Error |= BCC_WriteRegister(drvConfig, 1, BCC_REG_FUSE_MIRROR_DATA_ADDR, fusedata1[22]);

	Error |= BCC_WriteRegister(drvConfig,1, BCC_REG_FUSE_MIRROR_CTRL_ADDR,0x0C);
	if(Error != BCC_STATUS_SUCCESS)
		return Error;

	/*4. set SYS_CFG2[HAMMENCOD] bit 0*/
	Error = BCC_UpdateRegister(drvConfig, cid, BCC_REG_SYS_CFG2_ADDR, BCC_RW_HAMM_ENCOD_MASK, BCC_HAMM_ENCOD_DISABLED);

	return Error;
}

/*FUNCTION**********************************************************************
 *
 * Function Name : BCC_DiagADC1
 * Description   : This function implements the ADC1-A and ADC1-B functional
 *                 verification.
 *TIME: 9.283ms(1 node), 11.9ms(2 node)
 *END**************************************************************************/
bcc_status_t BCC_DiagADC1(bcc_drv_config_t* const drvConfig, bcc_adc1x_res_t *result)
{
    uint8_t i,cid;
    uint16_t adcCfgVal;
    uint16_t measVal[BCC_DIAG_ADC1_MEAS_NUM][drvConfig->devicesCnt][2];
    bcc_status_t error = BCC_STATUS_SUCCESS;

    /* Detection performance can be guaranteed only if ADC_CFG[ADC1_A_DEF] =
       ADC_CFG[ADC1_B_DEF] = 11 (16 bit resolution). */

    for(cid = 1; cid <= drvConfig->devicesCnt; cid++)
    {
		error = BCC_ReadRegisters(drvConfig, cid, BCC_REG_ADC_CFG_ADDR, 1U, &adcCfgVal);
		error |= BCC_UpdateRegister(drvConfig, cid, BCC_REG_ADC_CFG_ADDR,
					BCC_RW_ADC1_A_DEF_MASK | BCC_RW_ADC1_B_DEF_MASK,
					BCC_ADC1_A_RES_16BIT | BCC_ADC1_B_RES_16BIT);
		if (error != BCC_STATUS_SUCCESS)
		{
			return error;
		}
    }

    /* Measurement values (of an independent voltage reference) are be obtained
       by reading the MEAS_VBG_DIAG_ADC1A and MEAS_VBG_DIAG_ADC1B registers.
       It is necessary that the system controller performs a moving average of
       N consecutive values of MEAS_VBG_DIAG_ADC1A and N consecutive values of
       MEAS_VBG_DIAG_ADC1B before deciding about the fault. N shall be equal or
       greater than 6. */
    for (i = 0; i < BCC_DIAG_ADC1_MEAS_NUM; i++)
    {
        if ((error = BCC_RunCOD(drvConfig, BCC_CID_DEV1)) != BCC_STATUS_SUCCESS)
        {
            return error;
        }

        for(cid = 1; cid <= drvConfig->devicesCnt; cid++)
        {
            /* Read conversion results. */
            error |= BCC_ReadRegisters(drvConfig, cid, BCC_REG_MEAS_VBG_DIAG_ADC1A_ADDR, 2U, measVal[i][cid-1]);
        }

        if (error != BCC_STATUS_SUCCESS)
        {
            return error;
        }
    }

    /* Evaluate results. */
    for(cid = 1; cid <= drvConfig->devicesCnt; cid++)
    {
        result[cid-1].adc1aAvg = 0;
        result[cid-1].adc1bAvg = 0;
    }
    for (i = 0; i < BCC_DIAG_ADC1_MEAS_NUM; i++)
    {
    	for(cid = 1; cid <= drvConfig->devicesCnt; cid++)
    	{
            result[cid-1].adc1aAvg += BCC_GET_VOLT(measVal[i][cid-1][0]);
            result[cid-1].adc1bAvg += BCC_GET_VOLT(measVal[i][cid-1][1]);
    	}
    }

    for(cid = 1; cid <= drvConfig->devicesCnt; cid++)
    {
        result[cid-1].adc1aAvg /= BCC_DIAG_ADC1_MEAS_NUM;
        result[cid-1].adc1bAvg /= BCC_DIAG_ADC1_MEAS_NUM;
    }

    for(cid = 1; cid <= drvConfig->devicesCnt; cid++)
    {
		if (BCC_IS_IN_RANGE(result[cid-1].adc1aAvg, BCC_V_BGP - BCC_ADC1X_FV, BCC_V_BGP + BCC_ADC1X_FV) &&
			BCC_IS_IN_RANGE(result[cid-1].adc1bAvg, BCC_V_BGP - BCC_ADC1X_FV, BCC_V_BGP + BCC_ADC1X_FV))
		{
			result[cid-1].error = false;
		}
		else
		{
			result[cid-1].error = true;
		}
    }

    return error;
}

/*FUNCTION**********************************************************************
 *
 * Function Name : BCC_DiagADC1
 * Description   : This function implements the Vpower line diagnostic
 * Execute time:  1.938ms(1 node in daisy-chain),2.9ms(2 node in daisy-chain)
 *END**************************************************************************/
bcc_status_t BCC_DiagVPWR(bcc_drv_config_t* const drvConfig,
	bcc_vpwr_res_t *result)
{
	uint16_t measVolt[drvConfig->devicesCnt][BCC_MAX_CELLS + 1];
	uint32_t meascellsum = 0;
	uint8_t i, cid = 1;
	bcc_status_t error = BCC_STATUS_SUCCESS;

    /* Initiate conversion. */
    if ((error = BCC_RunCOD(drvConfig, cid)) != BCC_STATUS_SUCCESS)
    {
        return error;
    }

    /* Read conversion results. */
    for(cid = 1; cid <= drvConfig->devicesCnt; cid++)
    {
		error = BCC_ReadRegisters(drvConfig, cid, BCC_REG_MEAS_STACK_ADDR, 15U, measVolt[cid-1]);
		if (error != BCC_STATUS_SUCCESS)
		{
			return error;
		}
    }

    /* Evaluate / Publish results. */
    for(cid = 1; cid <= drvConfig->devicesCnt; cid++)
    {
    	meascellsum = 0;
        result[cid-1].stackmeas = BCC_GET_STACK_VOLT(measVolt[cid-1][0]);
        BCC_ConvertVoltageArray(&measVolt[cid-1][1], BCC_MAX_CELLS_MC33771, result[cid-1].cellmeas);

        for(i = 1; i <= BCC_MAX_CELLS_MC33771; i++)
        {
        	meascellsum += result[cid-1].cellmeas[1];
        }

        if((meascellsum - result[cid-1].stackmeas) >= BCC_V_stack_thre)
        {
        	 result[cid-1].error = true;
        }
        else
        {
        	result[cid-1].error = false;
        }
    }

    return error;
}

/*FUNCTION**********************************************************************
 *
 * Function Name : BCC_DiagOvUv
 * Description   : This function implements OV/UV functional verification
 *                 through digital comparators against tunable thresholds.
 *Execute time   : 29.6ms(1 node in daisy-chain)��33.3ms(2 node in daisy-chain)
 *END**************************************************************************/
bcc_status_t BCC_DiagOvUv(bcc_drv_config_t* const drvConfig,
    bcc_battery_type_t battType, bcc_diag_ov_uv_res_t* results)
{
	uint8_t cid = 1;
    uint16_t regVal;      /* Value of a register. */
    uint16_t thAllCtVal[drvConfig->devicesCnt], ovUvEnVal[drvConfig->devicesCnt];
    bcc_status_t error = BCC_STATUS_SUCCESS;

    /* a. If the number of cells on the cluster is odd, then write the bit
     * SYS_CFG2[NUMB_ODD] to logic 1, else write it to logic 0. */
    for(cid = 1; cid <= drvConfig->devicesCnt; cid++)
    {
		regVal = (drvConfig->cellCnt[cid - 1] & 0x01U) ? BCC_ODD_CELLS : BCC_EVEN_CELLS;
		error = BCC_UpdateRegister(drvConfig, cid, BCC_REG_SYS_CFG2_ADDR, BCC_RW_NUMB_ODD_MASK, regVal);
    }


    for(cid = 1; cid <= drvConfig->devicesCnt; cid++)
	{
    	/* 1. Enter diagnostic mode. */
		error = BCC_EnterDiagnostics(drvConfig, cid);
		/* Enable OV/UV. */
		error |= BCC_ReadRegisters(drvConfig, cid, BCC_REG_OV_UV_EN_ADDR, 1U, &ovUvEnVal[cid-1]);

		/* 2. Set the OV and the UV thresholds to diagnostic values (see CTx_OV_TH and
		 * CTx_UV_TH parameters in reference document) */
		error |= BCC_ReadRegisters(drvConfig, cid, BCC_REG_TH_ALL_CT_ADDR, 1U, &thAllCtVal[cid-1]);
		if (error != BCC_STATUS_SUCCESS)
		{
			return error;
		}
	}

	/* 3. Write OV_UV_EN[CTx_OVUV_EN] for x = 1..14 to enable OV/UV. */
	error = BCC_WriteRegisterGlobal(drvConfig, BCC_REG_OV_UV_EN_ADDR, 0xFFFFU);

	if (error != BCC_STATUS_SUCCESS)
	{
		return error;
	}


	if (error != BCC_STATUS_SUCCESS)
	{
		return error;
	}

	if (battType == BCC_BATT_T)
	{
		regVal = BCC_SET_ALL_CT_OV_TH(BCC_CTX_OV_TH_T) | BCC_SET_ALL_CT_UV_TH(BCC_CTX_UV_TH_T);
	}
	else if (battType == BCC_BATT_F)
	{
		regVal = BCC_SET_ALL_CT_OV_TH(BCC_CTX_OV_TH_F) | BCC_SET_ALL_CT_UV_TH(BCC_CTX_UV_TH_F);
	}
	else
	{
		regVal = BCC_SET_ALL_CT_OV_TH(BCC_CTX_OV_TH_N) | BCC_SET_ALL_CT_UV_TH(BCC_CTX_UV_TH_N);
	}

	error = BCC_WriteRegisterGlobal(drvConfig, BCC_REG_TH_ALL_CT_ADDR, regVal);
	if (error != BCC_STATUS_SUCCESS)
	{
		return error;
	}

	/* 4. Enter OV and UV functional verification. */
	error = BCC_WriteRegisterGlobal(drvConfig, BCC_REG_SYS_DIAG_ADDR, BCC_CT_OV_UV_ENABLED);
	if (error != BCC_STATUS_SUCCESS)
	{
		return error;
	}


	/* Command switches, run measurement and read results (odd switches opened,
	 * even closed). */
	error = BCC_DiagOvuvPart(drvConfig, BCC_SWITCH_POS_OPEN, BCC_SWITCH_POS_CLOSED, results);

	/* Command switches, run measurement and read results (odd switches closed,
	 * even opened). */
	error |= BCC_DiagOvuvPart(drvConfig, BCC_SWITCH_POS_CLOSED, BCC_SWITCH_POS_OPEN, results);

	if (error != BCC_STATUS_SUCCESS)
	{
		return error;
	}


	/* 5. Open all switches and leave OV and UV functional verification. */
	error = BCC_WriteRegisterGlobal(drvConfig, BCC_REG_SYS_DIAG_ADDR, 0x0000);
	if (error != BCC_STATUS_SUCCESS)
	{
		return error;
	}

	/* 6. Wait for 10 times measurement time constant. */
	BCC_WaitUs(10U * BCC_DIAG_TAU(drvConfig));

		/* 7. Restore normal functional values for the OV and UV thresholds. */
	for(cid = 1; cid <= drvConfig->devicesCnt; cid++)
	{
		error = BCC_WriteRegister(drvConfig, cid, BCC_REG_TH_ALL_CT_ADDR, thAllCtVal[cid-1]);
		error |= BCC_WriteRegister(drvConfig, cid, BCC_REG_OV_UV_EN_ADDR, ovUvEnVal[cid-1]);
		if (error != BCC_STATUS_SUCCESS)
		{
			return error;
		}
	}

		/* 7. Clear OV, UV faults. */
	error = BCC_WriteRegisterGlobal(drvConfig, BCC_REG_CELL_OV_FLT_ADDR, 0x0000U);
	error |= BCC_WriteRegisterGlobal(drvConfig, BCC_REG_CELL_UV_FLT_ADDR, 0x0000U);
	if (error != BCC_STATUS_SUCCESS)
	{
		return error;
	}

	/* 8. Exit diagnostic mode. Automatically open all switches. */
	for(cid = 1; cid <= drvConfig->devicesCnt; cid++)
	{
	    BCC_ExitDiagnostics(drvConfig, cid);
	}


    return BCC_STATUS_SUCCESS;
}

/*FUNCTION**********************************************************************
 *
 * Function Name : BCC_DiagCTxOpen
 * Description   : This function implements CTx open detection and functional
 *                 verification.
 *Execution: 47ms(1 node in daisy-chain)��52.2ms(2 node in daisy-chain)
 *END**************************************************************************/
bcc_status_t BCC_DiagCTxOpen(bcc_drv_config_t* const drvConfig,
    bcc_battery_type_t battType, bcc_diag_ctx_open_res_t* results)
{
    uint8_t i;
    /* Measured voltages with use of different register settings. */
    uint16_t measVolt[3][drvConfig->devicesCnt][BCC_MAX_CELLS];
    uint16_t regVal;      /* Value of a register. */
    bcc_status_t error = BCC_STATUS_SUCCESS;
    uint32_t vOlDetect;
    uint8_t cid;

    /* If the number of cells on the cluster is odd, then write the bit
     * SYS_CFG2[NUMB_ODD] to logic 1, else write it to logic 0. */
    for(cid = 1; cid <= drvConfig->devicesCnt; cid++)
    {
		regVal = (drvConfig->cellCnt[cid - 1] & 0x01U) ? BCC_ODD_CELLS : BCC_EVEN_CELLS;
		error = BCC_UpdateRegister(drvConfig, cid, BCC_REG_SYS_CFG2_ADDR, BCC_RW_NUMB_ODD_MASK, regVal);
    }

    /* 1. Enter diagnostic mode. */
    for(cid = 1; cid <= drvConfig->devicesCnt; cid++)
    {
		error = BCC_EnterDiagnostics(drvConfig, cid);
		if (error != BCC_STATUS_SUCCESS)
		{
			return error;
		}
    }

    /* Initiate conversion. */
    error = BCC_RunCOD(drvConfig, BCC_CID_DEV1);
    if (error != BCC_STATUS_SUCCESS)
    {
        return error;
    }

    /* Read conversion results (pre-closure measurements). */
    for(cid = 1; cid <= drvConfig->devicesCnt; cid++)
    {
		error = BCC_ReadRegisters(drvConfig, cid, BCC_REG_MEAS_CELLX_ADDR_START_MC33771, BCC_MAX_CELLS_MC33771, measVolt[0][cid-1]);

		if (error != BCC_STATUS_SUCCESS)
		{
			return error;
		}
    }

    /* 2 - 6. Command switches, run measurement and read results. */
    error = BCC_DiagCtxopenPart(drvConfig, BCC_SWITCH_POS_CLOSED,
                BCC_SWITCH_POS_OPEN, measVolt[1]);
    if (error != BCC_STATUS_SUCCESS)
    {
        return error;
    }

    /* 8 - 12. Command switches, run measurement and read results. */
    error = BCC_DiagCtxopenPart(drvConfig, BCC_SWITCH_POS_OPEN,
                BCC_SWITCH_POS_CLOSED, measVolt[2]);
    if (error != BCC_STATUS_SUCCESS)
    {
        return error;
    }

    /* 14. Open all switches. */
    error = BCC_WriteRegisterGlobal(drvConfig, BCC_REG_SYS_DIAG_ADDR, 0x0000);
    if (error != BCC_STATUS_SUCCESS)
    {
        return error;
    }

    /* 15. Wait for 10 times measurement constant. */
    BCC_WaitUs(10U * BCC_DIAG_TAU(drvConfig));

    /* 16. Clear CELL_OV_FLT and CELL_UV_FLT fault registers, as well as
     * FAULT1_STATUS[CT_OV_FLT,CT_UV_FLT] bits. */
    error = BCC_WriteRegisterGlobal(drvConfig, BCC_REG_CELL_OV_FLT_ADDR, 0x0000U);
    error |= BCC_WriteRegisterGlobal(drvConfig, BCC_REG_CELL_UV_FLT_ADDR, 0x0000U);
    if (error != BCC_STATUS_SUCCESS)
    {
        return error;
    }

    /* 17. Exit diagnostic mode. */
    for(cid = 1; cid <= drvConfig->devicesCnt; cid++)
    {
		error = BCC_ExitDiagnostics(drvConfig, cid);
		if (error != BCC_STATUS_SUCCESS)
		{
			return error;
		}
    }

    /* Evaluate / Publish results. */
    for(cid = 1; cid <= drvConfig->devicesCnt; cid++)
    {
		BCC_ConvertVoltageArray(measVolt[0][cid-1], BCC_MAX_CELLS_MC33771, results[cid-1].measPreClosure);
		BCC_ConvertVoltageArray(measVolt[1][cid-1], BCC_MAX_CELLS_MC33771, results[cid-1].measOddClosed);
		BCC_ConvertVoltageArray(measVolt[2][cid-1], BCC_MAX_CELLS_MC33771, results[cid-1].measEvenClosed);
		results[cid-1].ctxOpen = 0x0000U;
    }

    /* Determine fault mode:
     * Normal condition
     *   SWx OFF: V >= min(Vcell)
     *   SWx ON: V < TH_CTx[CTx_UV_TH]
     * Open line condition during test execution
     *   SWx OFF: No decision
     *   SWx ON: V < VOL_DETECT
     */

    if (battType == BCC_BATT_T)
    {
        vOlDetect = BCC_V_OL_DETECT_T;
    }
    else if (battType == BCC_BATT_F)
    {
        vOlDetect = BCC_V_OL_DETECT_F;
    }
    else
    {
        vOlDetect = BCC_V_OL_DETECT_N;
    }

    /* Open detection on odd cells - CT1, CT3,CT5,.. */
    for(cid = 1; cid <= drvConfig->devicesCnt; cid++)
    {
		for (i = 1U; i <= BCC_MAX_CELLS_MC33771; i += 2U)
		{
			/* Voltage on cell should not be below specified value in normal condition. */
			if (results[cid-1].measOddClosed[BCC_MAX_CELLS_MC33771 - i] < vOlDetect)
			{
				results[cid-1].ctxOpen |= (0x0001U << (i - 1U));
			}
		}

		/* Open detection on even cells - CT2, CT4, CT6,.. */
		for (i = 2U; i <= BCC_MAX_CELLS_MC33771; i += 2U)
		{
			if (results[cid-1].measEvenClosed[BCC_MAX_CELLS_MC33771 - i] < vOlDetect)
			{
				results[cid-1].ctxOpen |= (0x0001U << (i - 1U));
			}
		}
    }

    return BCC_STATUS_SUCCESS;
}

/*FUNCTION**********************************************************************
 *
 * Function Name : BCC_DiagCellVolt
 * Description   : This function implements Cell Voltage Channel functional
 *                 verification.
 *Execute time:  13.2ms(1 node in daisy-chain)��19.1ms(2 node in daisy-chain)
 *END**************************************************************************/
bcc_status_t BCC_DiagCellVolt(bcc_drv_config_t* const drvConfig, bcc_diag_cell_volt_res_t* results)
{
    uint8_t i, j;
    /* Measured cells voltage
     * MC33771: [0] CT14, ..., [13] CT1
 */
    uint16_t measVolt[drvConfig->devicesCnt][BCC_MAX_CELLS];
    /* Temporary measured cells voltages in uV
     * MC33771: [0] CT14, ..., [13] CT1
     */
    uint32_t measVoltUV[BCC_MAX_CELLS];
    bcc_status_t error = BCC_STATUS_SUCCESS;
    uint8_t cid = 1;

    /* 1. Enter diagnostic mode. */
    for(cid = 1; cid <= drvConfig->devicesCnt; cid++)
    {
		error = BCC_EnterDiagnostics(drvConfig, cid);
		if (error != BCC_STATUS_SUCCESS)
		{
			return error;
		}
    }

    /*If not Configure 16 bit resolution of ADC1-A, B in init phase,
     * make (ADC_CFG[ADC1_A_DEF] = ADC_CFG[ADC1_B_DEF] = 11). */

/*  error = BCC_ReadRegisters(drvConfig, cid, BCC_REG_ADC_CFG_ADDR, 1U, &adcCfgVal);
    error |= BCC_UpdateRegister(drvConfig, cid, BCC_REG_ADC_CFG_ADDR,
                BCC_RW_ADC1_A_DEF_MASK | BCC_RW_ADC1_B_DEF_MASK,
                BCC_ADC1_A_RES_16BIT | BCC_ADC1_B_RES_16BIT);
    if (error != BCC_STATUS_SUCCESS)
    {
        return error;
    }
*/

    /* 2. Isolate CTx inputs and places reference at amplifier input. */
    error = BCC_WriteRegisterGlobal(drvConfig, BCC_REG_SYS_DIAG_ADDR, BCC_DA_DIAG_CHECK);
    if (error != BCC_STATUS_SUCCESS)
    {
        return error;
    }

    /* To reduce the effect of the noise, it is good practice to cycle through
     * steps 3 and 4 a few times to get an average of the results before
     * proceeding to step 5. */
    for (i = 0; i < BCC_DIAG_CVFV_MEAS_NUM; i++)
    {
        /* 3. Initiate conversion. */
        /* 4. Wait for conversion time. */
        error = BCC_RunCOD(drvConfig, BCC_CID_DEV1);
        if (error != BCC_STATUS_SUCCESS)
        {
            return error;
        }

        /* 5a. The system controller reads MEAS_CELLx results */
        for(cid = 1; cid <= drvConfig->devicesCnt; cid++)
        {
			error = BCC_ReadRegisters(drvConfig, cid, BCC_REG_MEAS_CELLX_ADDR_START_MC33771, BCC_MAX_CELLS_MC33771, measVolt[cid-1]);

			if (error != BCC_STATUS_SUCCESS)
			{
				return error;
			}

			if (i == 0)
			{
				BCC_ConvertVoltageArray(measVolt[cid-1], BCC_MAX_CELLS_MC33771, results[cid-1].measCellVolt);
			}
			else
			{
				BCC_ConvertVoltageArray(measVolt[cid-1], BCC_MAX_CELLS_MC33771, measVoltUV);

				for (j = 0; j < BCC_MAX_CELLS_MC33771; j++)
				{
					results[cid-1].measCellVolt[j] += measVoltUV[j];
				}
			}
        }
    }

    /* Compute the average of measurements. */
    for (j = 0; j < BCC_MAX_CELLS_MC33771; j++)
    {
        for(cid = 1; cid <= drvConfig->devicesCnt; cid++)
        {
            results[cid-1].measCellVolt[j] /= BCC_DIAG_CVFV_MEAS_NUM;
            /* Evaluate / Publish results. */
            results[cid-1].result = 0x00U;
        }
    }

    /* 7. Clear cell OV faults in both the FAULT1_STATUS[CT_OV_FLT] bit and the
     * CELL_OV_FLT register.
     * Note: FAULT1_STATUS[CT_OV_FLT] bit is automatically cleared by reset the
     * CELL_OV_FLT. */
    error = BCC_WriteRegisterGlobal(drvConfig, BCC_REG_CELL_OV_FLT_ADDR, 0x0000U);

    /* 8. Exit diagnostic mode. */
    for(cid = 1; cid <= drvConfig->devicesCnt; cid++)
    {
		error |= BCC_ExitDiagnostics(drvConfig, cid);
		if (error != BCC_STATUS_SUCCESS)
		{
			return error;
		}
    }

    for(cid = 1; cid <= drvConfig->devicesCnt; cid++)
    {
		for (i = 3U; i <= 14U; i++)
		{
			/* NOTE: Checking all channels from 3 to 14 is necessary only if all
			 *  14 cells are used; otherwise, unused cell voltage channels may
			 *  be skipped. */
			if (BCC_IS_CELL_CONN(drvConfig, cid, i))
			{
				/* 5b. The system controller computes errors
				 * V_err_x = (MEAS_CELLx - MEAS_CELL2), for x = 3 to 14. */
				results[cid-1].vErrX[i - 3] = (int32_t)results[cid-1].measCellVolt[14 - i] -
							(int32_t)results[cid-1].measCellVolt[12];

				/* 6. The system controller checks, for any x = 3 to 14, if
				 * inequalities min(VCVFV) <= V_err_x <= max(VCVFV) are true.
				 * If yes, the check is ok, otherwise an error is detected. */
				if ((results[cid-1].vErrX[i - 3] < BCC_DIAG_VCVFV_MIN) ||
						(results[cid-1].vErrX[i - 3] > BCC_DIAG_VCVFV_MAX))
				{
					results[cid-1].result |= (0x0001U << i);
				}
			}
			else
			{
				results[cid-1].vErrX[i - 3] = 0;
			}
		}
    }

    return BCC_STATUS_SUCCESS;
}

/*FUNCTION**********************************************************************
 *
 * Function Name : BCC_DiagCellLeak
 * Description   : This function implements cell terminals and cell balancing
 *                 terminals leakage current diagnostic.
 *Execute time: 55.6ms(1 node in daisy-chain)��69ms(2 node in daisy-chain)
 *END**************************************************************************/
bcc_status_t BCC_DiagCellLeak(bcc_drv_config_t* const drvConfig, bcc_diag_cell_leak_res_t* results)
{
    uint8_t i, k;
    /* Measured voltages in raw values for two configurations. */
    uint16_t measRaw[2][drvConfig->devicesCnt][BCC_MAX_CELLS + 1];
    /* Measured voltages in uV for two configurations. */
    uint32_t measUV[2][drvConfig->devicesCnt][BCC_MAX_CELLS + 1];
    int32_t tmp;
    uint8_t cid = 1;;
    bcc_status_t error = BCC_STATUS_SUCCESS;



    /* 1. Set all CBx drivers to OFF. */
    for(cid = 1; cid <= drvConfig->devicesCnt; cid++)
    {
		error = BCC_UpdateRegister(drvConfig, cid, BCC_REG_SYS_CFG1_ADDR, BCC_RW_CB_DRVEN_MASK, BCC_CB_DRV_DISABLED);
		if (error != BCC_STATUS_SUCCESS)
		{
			return error;
		}
	    /* A. detect a connector having an abnormally high contact resistance. */
	    results[cid-1].conResistance = 0x0000;
    }
    /* Wait for the time interval TCTsettle=5ms (cell balance delay + cell
     * terminal settling time). */
    BCC_WaitMs(BCC_DIAG_TCT_SETTLE);
    /*2. Cycle through all cells, measure all cell voltage before enbale CB*/
    error = BCC_RunCOD(drvConfig, BCC_CID_DEV1);

    for(cid = 1; cid <= drvConfig->devicesCnt; cid++)
    {
    	error |= BCC_ReadRegisters(drvConfig, cid, BCC_REG_MEAS_CELLX_ADDR_START_MC33771, BCC_MAX_CELLS_MC33771, &(measRaw[0][cid-1][0]));
    }

	if (error != BCC_STATUS_SUCCESS)
	{
		return error;
	}

    /*detect high contact resistance of odd channel */
    /* 3. Set the odd CBx to ON; */
    for(cid = 1; cid <= drvConfig->devicesCnt; cid++)
    {
    	error = BCC_UpdateRegister(drvConfig, cid, BCC_REG_SYS_CFG1_ADDR, BCC_RW_CB_DRVEN_MASK, BCC_CB_DRV_ENABLED);
    }

    for (i =0 ; i < BCC_MAX_CELLS_MC33771; i=i+2)
    {
        error = BCC_WriteRegisterGlobal(drvConfig, BCC_REG_CB1_CFG_ADDR+i, BCC_CB_ENABLED);
        error |= BCC_WriteRegisterGlobal(drvConfig, BCC_REG_CB1_CFG_ADDR+i+1, BCC_CB_DISABLED);
        if (error != BCC_STATUS_SUCCESS)
        {
            return error;
        }
    }

    /* Wait for the time interval TCTsettle=5ms (cell balance delay + cell
     * terminal settling time). */
    BCC_WaitMs(BCC_DIAG_TCT_SETTLE);

    /* 3. Read odd channel voltage; */
    error = BCC_RunCOD(drvConfig, BCC_CID_DEV1);
    for(cid = 1; cid <= drvConfig->devicesCnt; cid++)
    {
    	error |= BCC_ReadRegisters(drvConfig, cid, BCC_REG_MEAS_CELLX_ADDR_START_MC33771, BCC_MAX_CELLS_MC33771, &(measRaw[1][cid-1][0]));
    }
    if (error != BCC_STATUS_SUCCESS)
    {
        return error;
    }
    /*4. If |Vcell_meas_x(CB_ON) - Vcell_meas_x(CB_OFF)| > DV_diag, then error detected. */
    for(cid = 1; cid <= drvConfig->devicesCnt; cid++)
    {
		for (i = 1; i <= BCC_MAX_CELLS_MC33771; i=i+2)
		{
			k = BCC_MAX_CELLS_MC33771-i;
			tmp = ((int32_t)(BCC_GET_VOLT(measRaw[1][cid-1][k]))) - ((int32_t)(BCC_GET_VOLT(measRaw[0][cid-1][k])));
			if ((tmp > BCC_DIAG_DV) || (tmp < -1 * ((int32_t)BCC_DIAG_DV)))
			{
				results[cid-1].conResistance |= (1 << (i-1));
			}
		}
    }

    /*detect high contact resistance of even channel */
    /* 5. Set the odd CBx to OFF and Set even CBx ON */
    for (i = 0; i < BCC_MAX_CELLS_MC33771; i=i+2){

        error = BCC_WriteRegisterGlobal(drvConfig, BCC_REG_CB1_CFG_ADDR+i, BCC_CB_DISABLED);
        error |= BCC_WriteRegisterGlobal(drvConfig, BCC_REG_CB1_CFG_ADDR+i+1,  BCC_CB_ENABLED);
        if (error != BCC_STATUS_SUCCESS)
        {
            return error;
        }
    }
    /* Wait for the time interval TCTsettle=5ms (cell balance delay + cell
     * terminal settling time). */
    BCC_WaitMs(BCC_DIAG_TCT_SETTLE);
    /* 6. Read even channel voltage; */
    error = BCC_RunCOD(drvConfig, BCC_CID_DEV1);

    for(cid = 1; cid <= drvConfig->devicesCnt; cid++)
    {
    	error |= BCC_ReadRegisters(drvConfig, cid, BCC_REG_MEAS_CELLX_ADDR_START_MC33771, BCC_MAX_CELLS_MC33771, &(measRaw[1][cid-1][0]));
    }

    /*7. If |Vcell_meas_x(CB_ON) - Vcell_meas_x(CB_OFF)| > DV_diag, then error detected. */
    for(cid = 1; cid <= drvConfig->devicesCnt; cid++)
    {
		for (i = 2; i <= BCC_MAX_CELLS_MC33771; i=i+2)
		{
			k = BCC_MAX_CELLS_MC33771-i;
			tmp = ((int32_t)(BCC_GET_VOLT(measRaw[1][cid-1][k]))) - ((int32_t)(BCC_GET_VOLT(measRaw[0][cid-1][k])));
			if ((tmp > BCC_DIAG_DV) || (tmp < -1 * ((int32_t)BCC_DIAG_DV)))
			{
				results[cid-1].conResistance |= (1 << (i-1));
			}
		}
    }
    /* 8. Set the even CBx to OFF */
    for (i = 0; i < BCC_MAX_CELLS_MC33771; i=i+2)
    {
        error = BCC_WriteRegisterGlobal(drvConfig, BCC_REG_CB1_CFG_ADDR+i+1,  BCC_CB_DISABLED);
        if (error != BCC_STATUS_SUCCESS)
        {
            return error;
        }
    }
    /* Wait for the time interval TCTsettle=5ms (cell balance delay + cell
     * terminal settling time). */
    BCC_WaitMs(BCC_DIAG_TCT_SETTLE);


    /* B. Detect a leakage current. */

    for(cid = 1; cid <= drvConfig->devicesCnt; cid++)
    {
    	/* 1. Enter diagnostic mode. */
    	error = BCC_EnterDiagnostics(drvConfig, cid);
        /* 2. Pause cell balancing. */
        error |= BCC_UpdateRegister(drvConfig, cid, BCC_REG_SYS_CFG1_ADDR,
                    BCC_RW_CB_MANUAL_PAUSE_MASK, BCC_CB_MAN_PAUSE_ENABLED);
        if (error != BCC_STATUS_SUCCESS)
        {
            return error;
        }

        /* Averaging cycle with initialization */
        for (i = 0; i < BCC_MAX_CELLS_MC33771 + 1U; i++)
        {
            results[cid-1].vleak_avx[i] = 0;
        }

        results[cid-1].leakStatus = 0x00U;
    }

    for (k = 0; k < BCC_DIAG_LEAK_MEAS_NUM; k++)
    {
        /* 3. Write SYS_DIAG[CT_LEAK_DIAG,POLARITY] = 10 to route cell terminal and
         * balancing pins according to the logic of the routing table. */
        error = BCC_WriteRegisterGlobal(drvConfig, BCC_REG_SYS_DIAG_ADDR,
        		BCC_CT_LEAK_DIAG_DIFF | BCC_POL_NON_INVERTED);
        if (error != BCC_STATUS_SUCCESS)
        {
            return error;
        }

        /* 4. Initiate conversion. */
        /* 5. Wait for conversion time. */
        error = BCC_RunCOD(drvConfig, BCC_CID_DEV1);
        if (error != BCC_STATUS_SUCCESS)
        {
            return error;
        }

        /* 6. Read measurement results. */

        /* Read MEAS_STACK and MEAS_CELL14 to MEAS_CELL1 */
        for(cid = 1; cid <= drvConfig->devicesCnt; cid++)
        {
		    error |= BCC_ReadRegisters(drvConfig, cid, BCC_REG_MEAS_STACK_ADDR, BCC_MAX_CELLS_MC33771+1U, measRaw[0][cid-1]);
        }

        if (error != BCC_STATUS_SUCCESS)
        {
            return error;
        }

        /* 7. Compute leakage indices IND0x = MEAS_CELLx, for x = 1 to 6/14. */
        /* 8. Compute leakage index IND07/15 = MEAS_STACK. */
        for(cid = 1; cid <= drvConfig->devicesCnt; cid++)
        {
            BCC_ConvertVoltageArray(measRaw[0][cid-1], BCC_MAX_CELLS_MC33771 + 1U, measUV[0][cid-1]);
        }

        /* 9. Write SYS_DIAG[CT_LEAK_DIAG,POLARITY] = 11 to route cell terminal and
         * balancing pins according to the logic of the routing table. */
        error = BCC_WriteRegisterGlobal(drvConfig, BCC_REG_SYS_DIAG_ADDR,
                    BCC_CT_LEAK_DIAG_DIFF | BCC_POL_INVERTED);
        if (error != BCC_STATUS_SUCCESS)
        {
            return error;
        }

        /* 10. Initiate conversion. */
        /* 11. Wait for conversion time. */
        error = BCC_RunCOD(drvConfig, BCC_CID_DEV1);
        if (error != BCC_STATUS_SUCCESS)
        {
            return error;
        }

        /* 12. Read measurement results. */
        /* Read MEAS_STACK and MEAS_CELL14 to MEAS_CELL1*/
        for(cid = 1; cid <= drvConfig->devicesCnt; cid++)
        {
		    error |= BCC_ReadRegisters(drvConfig, cid, BCC_REG_MEAS_STACK_ADDR, BCC_MAX_CELLS_MC33771+1U, measRaw[1][cid-1]);
        }

        if (error != BCC_STATUS_SUCCESS)
        {
            return error;
        }

        /* 13. Compute leakage indices IND1x = MEAS_CELLx, for x = 1 to 6/14. */
        /* 14. Compute leakage index IND17/15 = MEAS_STACK. */
        for(cid = 1; cid <= drvConfig->devicesCnt; cid++)
        {
            BCC_ConvertVoltageArray(measRaw[1][cid-1], BCC_MAX_CELLS_MC33771 + 1U, measUV[1][cid-1]);
        }

        /* 15. MC33771: Compute Vleakx(k) = IND0x + IND1x, for x = 1 to 15.
         */
        /* 16. Calculate Vleak_AVx(k)=1/k*Vleakx(k)+(1-1/k)*Vleak_AVx(k-1),
         * for x=1 to 7/15 (averaging) */
        for(cid = 1; cid <= drvConfig->devicesCnt; cid++)
        {
			for (i = 0; i < BCC_MAX_CELLS_MC33771 + 1U; i++)
			{
				results[cid-1].vleak_avx[BCC_MAX_CELLS_MC33771 - i] += ((measUV[0][cid-1][i] >= measUV[1][cid-1][i]) ? measUV[0][cid-1][i] : measUV[1][cid-1][i]);
			}
        }
    }

    for(cid = 1; cid <= drvConfig->devicesCnt; cid++)
    {
		for (i = 0; i < BCC_MAX_CELLS_MC33771 + 1U; i++)
		{
			/* 16. Calculate Vleak_AVx(k)=1/k*Vleakx(k)+(1-1/k)*Vleak_AVx(k-1),
			 * for x=1 to 7/15 (averaging) */
			results[cid-1].vleak_avx[i] /= BCC_DIAG_LEAK_MEAS_NUM;

			/* 17.Evaluate the decision criterion, for x = 1 to 7/15:
			 *    If Vleak_AVx(k) >= VLEAK then cell x is leaky, else cell x is not leaky. */
			if (results[cid-1].vleak_avx[i] > BCC_DIAG_VLEAK_MC33771)
			{
				results[cid-1].leakStatus |= (1 << i);
			}
		}
    }

    /* 18. Clear OV, UV faults. */
    error = BCC_WriteRegisterGlobal(drvConfig, BCC_REG_CELL_OV_FLT_ADDR, 0x0000U);
    error |= BCC_WriteRegisterGlobal(drvConfig, BCC_REG_CELL_UV_FLT_ADDR, 0x0000U);

    /* 19. Restore cell muxing settings. */
    error |= BCC_WriteRegisterGlobal(drvConfig, BCC_REG_SYS_DIAG_ADDR, 0x0000);

    /* 20. Unpause cell balancing. */
    for(cid = 1; cid <= drvConfig->devicesCnt; cid++)
    {
		error |= BCC_UpdateRegister(drvConfig, cid, BCC_REG_SYS_CFG1_ADDR,
				BCC_RW_CB_MANUAL_PAUSE_MASK, BCC_CB_MAN_PAUSE_DISABLED);

		/* 21. Exit diagnostic mode. */
		error |= BCC_ExitDiagnostics(drvConfig, cid);
    }

    return error;
}

/*FUNCTION**********************************************************************
 *
 * Function Name : BCC_DiagCurrentMeas
 * Description   : This function implements current measurement diagnostics.
 *
 *END**************************************************************************/
bcc_status_t BCC_DiagCurrentMeas(bcc_drv_config_t* const drvConfig,
    bcc_cid_t cid, bcc_diag_current_meas_t sel, int32_t* current, bool* fault)
{
    uint16_t sysCfg1Val;  /* Original value of SYS_CFG1 register. */
    uint16_t sysDiagVal;  /* Original value of SYS_DIAG register. */
    uint16_t adcCfgVal;   /* Original value of ADC_CFG register. */

    /* PGA gain and ADC2 resolution for each bcc_diag_current_meas_t diagnostic
     * measurement. */
    uint16_t adcCfg[3] = {
            BCC_ADC2_RES_16BIT | BCC_ADC2_PGA_256,
            BCC_ADC2_RES_16BIT | BCC_ADC2_PGA_4,
            BCC_ADC2_RES_16BIT | BCC_ADC2_PGA_256
    };
    /* Inputs to PGA for each bcc_diag_current_meas_t diagnostic measurement. */
    uint32_t adcMux[3] = {
            BCC_IMUX_PGA_ZERO,
            BCC_IMUX_DIFF_REF,
            BCC_IMUX_GPIO5_6
    };
    bcc_status_t error;

    if ((cid == BCC_CID_UNASSIG) || (cid > drvConfig->devicesCnt)||(drvConfig->device[cid - 1] != BCC_DEVICE_MC33771))
    {
        return BCC_STATUS_PARAM_RANGE;
    }

    if ((sel != BCC_DCM_AMP_INP_GND) && (sel != BCC_DCM_VREF_GAIN4) && (sel != BCC_DCM_AN5AN6))
    {
        return BCC_STATUS_PARAM_RANGE;
    }
    /*a. config AN5 and AN6 as digital input if used as redundance*/
    if(sel == BCC_DCM_AN5AN6)
    {
        error = BCC_UpdateRegister(drvConfig, cid, BCC_REG_GPIO_CFG1_ADDR,
        		BCC_RW_GPIOX_CFG_MASK(5)|BCC_RW_GPIOX_CFG_MASK(6), BCC_GPIOX_DIG_IN(5)|BCC_GPIOX_DIG_IN(6));
    }

    /* 1. Disable current measurement. */
    error = BCC_UpdateRegister(drvConfig, cid, BCC_REG_SYS_CFG1_ADDR,
                BCC_RW_I_MEAS_EN_MASK, BCC_I_MEAS_DISABLED);

    /* 2. Enter diagnostic mode. */
    error |= BCC_EnterDiagnostics(drvConfig, cid);

    if (error != BCC_STATUS_SUCCESS)
    {
        return error;
    }

    /* Set ADC2 measurement resolution and PGA gain */
    error = BCC_UpdateRegister(drvConfig, cid, BCC_REG_ADC_CFG_ADDR,
                BCC_RW_ADC2_DEF_MASK | BCC_W_PGA_GAIN_MASK, adcCfg[sel]);
    if (error != BCC_STATUS_SUCCESS)
    {
        return error;
    }

    /* 3. Read the Coulomb counter register COULOMB_CNT to retain count information.
     * *** up to user *** */

    /* 4. Configure the current measurement chain for the specific diagnostic
     * source by writing to the SYS_DIAG[I_MUX] bits. */
    error = BCC_UpdateRegister(drvConfig, cid, BCC_REG_SYS_DIAG_ADDR,
                BCC_RW_I_MUX_MASK, adcMux[sel]);
    if (error != BCC_STATUS_SUCCESS)
    {
        return error;
    }

    /* 5. Enable the current measurement by setting SYS_CFG1[I_MEAS_EN] = 1. */
    error = BCC_UpdateRegister(drvConfig, cid, BCC_REG_SYS_CFG1_ADDR,
                BCC_RW_I_MEAS_EN_MASK, BCC_I_MEAS_ENABLED);
    if (error != BCC_STATUS_SUCCESS)
    {
        return error;
    }

    /* 6. Wait for the time to perform auto-zero procedure t_AZC_SETTLE. */
    BCC_WaitUs(200U);

    /* 7. Write ADC_CFG[CC_RST] = 1 and ADC_CFG[SOC] = 1 to reset the coulomb
     * counter COULOMB_CNT and initiate a conversion. */
    /* 8. Wait for the conversion time. */
    /* 9. Read the conversion results in register MEAS_ISENSE1 and MEAS_ISENSE2. */
    error = BCC_DiagRunImeas(drvConfig, cid, current);
    if (error != BCC_STATUS_SUCCESS)
    {
        return error;
    }

    /* 9. Evaluate / Publish results. */
    if (sel == BCC_DCM_AMP_INP_GND)
    {
        /* When the measured value <= Voff_diag -> success. */
       (*fault) = ((*current) <= (int32_t)BCC_DIAG_VOFF_MAX) == false;
    }
    else if (sel == BCC_DCM_VREF_GAIN4)
    {
        /* When the measured value is in the range defined by Vref_diag then success. */
		(*fault) = (((*current) >= BCC_DIAG_VREF_MIN_MC33771) && ((*current) <= BCC_DIAG_VREF_MAX_MC33771)) == false;

    }
    else
    {
        /* sel == BCC_DCM_AN5AN6 */
        /* Result is application dependent. */
        (*fault) = false;
    }

    /* 10. Exit diagnostic mode. */
    error = BCC_ExitDiagnostics(drvConfig, cid);

    /* 11. Reset Coulomb counter. */
    error |= BCC_UpdateRegister(drvConfig, cid, BCC_REG_ADC_CFG_ADDR,
                BCC_W_CC_RST_MASK, BCC_CC_RESET);

    return error;
}

/*FUNCTION**********************************************************************
 *
 * Function Name : BCC_DiagShuntConn
 * Description   : This function verifies whether the shunt resistor is properly
 *                 connected to the current channel low-pass filter.
 *execute time : 45ms
 *END**************************************************************************/
bcc_status_t BCC_DiagShuntConn(bcc_drv_config_t* const drvConfig, bcc_cid_t cid,
    bool* shuntConn)
{
    uint16_t regVal; /* Value of FAULT1_STATUS register. */
    bcc_status_t error;

    if ((cid == BCC_CID_UNASSIG) || (cid > drvConfig->devicesCnt))
    {
        return BCC_STATUS_PARAM_RANGE;
    }

    /* 1. Disable current measurement. */
    error = BCC_UpdateRegister(drvConfig, cid, BCC_REG_SYS_CFG1_ADDR,
                BCC_RW_I_MEAS_EN_MASK, BCC_I_MEAS_DISABLED);

    /* Clear faults. */
    error |= BCC_WriteRegister(drvConfig, cid, BCC_REG_FAULT1_STATUS_ADDR, 0x0000U);

    /* 2. Read the Coulomb counter COULOMB_CNT to retain count information
     * *** up to user *** */

    /* 3. Enter diagnostic mode. */
    error |= BCC_EnterDiagnostics(drvConfig, cid);
    if (error != BCC_STATUS_SUCCESS)
    {
        return error;
    }

    /* 4. Configure current measurement chain for the open detection check. */
    error = BCC_UpdateRegister(drvConfig, cid, BCC_REG_SYS_DIAG_ADDR,
                BCC_RW_ISENSE_OL_DIAG_MASK, BCC_ISENSE_OL_DIAG_ENABLED);
    if (error != BCC_STATUS_SUCCESS)
    {
        return error;
    }

    /* 5. Wait for the diagnostic time constant. */
    BCC_WaitUs(BCC_DIAG_T_DIAG(drvConfig));

    /* 6. Read FAULT1_STATUS[IS_OL_FLT] flag. */
    error = BCC_ReadRegisters(drvConfig, cid, BCC_REG_FAULT1_STATUS_ADDR, 1U, &regVal);
    if (error != BCC_STATUS_SUCCESS)
    {
        return error;
    }

    /* 7. Configure current measurement chain for the open detection check
     * by setting SYS_DIAG[I_SENSE_OL_DIAG] to logic 0. */
    error = BCC_UpdateRegister(drvConfig, cid, BCC_REG_SYS_DIAG_ADDR,
                BCC_RW_ISENSE_OL_DIAG_MASK, BCC_ISENSE_OL_DIAG_DISABLED);
    if (error != BCC_STATUS_SUCCESS)
    {
        return error;
    }

    /* 8. Wait for 10 times the current measurement time constant (tau_i). */
    BCC_WaitUs(10U * BCC_DIAG_TAU_I(drvConfig));

    /* 9. Enable the current measurement and exit diagnostic mode by setting
     * SYS_CFG[I_MEAS_EN] to logic 1 and SYS_CFG1[GO2DIAG] to logic 0. */
    error = BCC_UpdateRegister(drvConfig, cid, BCC_REG_SYS_CFG1_ADDR,
                BCC_RW_I_MEAS_EN_MASK, BCC_I_MEAS_ENABLED);
    error |= BCC_ExitDiagnostics(drvConfig, cid);

    /* 10. Reset Coulomb counter. */
    error |= BCC_UpdateRegister(drvConfig, cid, BCC_REG_ADC_CFG_ADDR,
                BCC_W_CC_RST_MASK, BCC_CC_RESET);

    /* Evaluate / Publish results. */
    (*shuntConn) = (regVal & BCC_RW_IS_OL_FLT_MASK) == 0x00U;

    /* Clear faults. */
    error |= BCC_WriteRegister(drvConfig, cid, BCC_REG_FAULT1_STATUS_ADDR, 0x0000U);

    return error;
}

/*FUNCTION**********************************************************************
 *
 * Function Name : BCC_DiagGPIOxOtUt
 * Description   : This function implements GPIOx OT/UT functional verification.
 *Execute time:  6.35ms(1 node in daisy-chain)��8.9ms(2 node in daisy-chain)
 *END**************************************************************************/
bcc_status_t BCC_DiagGPIOxOtUt(bcc_drv_config_t* const drvConfig, bcc_diag_ot_ut_res_t *results)
{
    uint16_t regVal;          /* Value of a GPIO_CFGx register. */
    uint16_t gpioCfgVal[drvConfig->devicesCnt][2];     /* Original value of GPIO_CFG1 and GPIO_CFG2 register. */
    bcc_status_t error =  BCC_STATUS_SUCCESS;
    uint8_t cid = 1;


    /* 1. Enter diagnostic mode. */
    for(cid = 1; cid <= drvConfig->devicesCnt; cid++)
    {
        error |= BCC_EnterDiagnostics(drvConfig, cid);
        if(error != BCC_STATUS_SUCCESS)
        	return error;
    }

    /* Clear OT/UT faults. */
    error = BCC_WriteRegisterGlobal(drvConfig, BCC_REG_AN_OT_UT_FLT_ADDR, 0x0000U);

    /* Store original value of GPIO_CFG1 and GPIO_CFG2 register. */
    for(cid = 1; cid <= drvConfig->devicesCnt; cid++)
    {
		error |= BCC_ReadRegisters(drvConfig, cid, BCC_REG_GPIO_CFG1_ADDR, 2U, gpioCfgVal[cid-1]);
    }
    if (error != BCC_STATUS_SUCCESS)
    {
        return BCC_STATUS_SUCCESS;
    }

    /* 2. Optionally program GPIOx to be tested as analog through
     * GPIO_CFG1[GPIOx_CFG] register. In Diagnostic mode, only GPIOx configured
     * as analog inputs have buffers activated by the ANx_TEMP_DIAG bit. */
    regVal = BCC_GPIOX_AN_IN_RM_MEAS(0U) | BCC_GPIOX_AN_IN_RM_MEAS(1U) |
                BCC_GPIOX_AN_IN_RM_MEAS(2U) | BCC_GPIOX_AN_IN_RM_MEAS(3U) |
                BCC_GPIOX_AN_IN_RM_MEAS(4U) | BCC_GPIOX_AN_IN_RM_MEAS(5U) |
                BCC_GPIOX_AN_IN_RM_MEAS(6U);
    error = BCC_WriteRegisterGlobal(drvConfig, BCC_REG_GPIO_CFG1_ADDR, regVal);

    /* 4. Enable the GPIOx output buffer through the SYS_DIAG[ANx_TEMP_DIAG]. */
    error |= BCC_WriteRegisterGlobal(drvConfig, BCC_REG_SYS_DIAG_ADDR, BCC_ANX_DIAG_SW_CLOSED);

    if (error != BCC_STATUS_SUCCESS)
    {
        return error;
    }

    /* 3. Drive output high to emulate UT.
     * Note: it is valid to use GPIOx_DR bit in diagnostic mode and analog input
     * mode of GPIOs. */
    /* 5. Request BCC to perform a conversion sequence by writing to ADC_CFG register. */
    /* 6. Conversions below the TH_ANx_OT threshold trigger the ANx_OT fault bit.
     * Conversions above the TH_ANx_UT threshold trigger the ANx_UT fault bit. */

    error = BCC_GpioOtUtPart(drvConfig, BCC_UT_VERIFICATION, results);
    if (error != BCC_STATUS_SUCCESS)
    {
        return error;
    }

    /* 3. Drive output low to emulate OT. */
    /* 5. Request BCC to perform a conversion sequence by writing to ADC_CFG register. */
    /* 6. Conversions below the TH_ANx_OT threshold trigger the ANx_OT fault bit.
     * Conversions above the TH_ANx_UT threshold trigger the ANx_UT fault bit. */
    error = BCC_GpioOtUtPart(drvConfig, BCC_OT_VERIFICATION, results);
    if (error != BCC_STATUS_SUCCESS)
    {
        return error;
    }

    /* Restore original content of registers. */
    for(cid = 1; cid <= drvConfig->devicesCnt; cid++)
    {
    	 error = BCC_WriteRegister(drvConfig, cid, BCC_REG_GPIO_CFG2_ADDR, gpioCfgVal[cid-1][1]);
    	 error |= BCC_WriteRegister(drvConfig, cid, BCC_REG_GPIO_CFG1_ADDR, gpioCfgVal[cid-1][0]);
    }

    /* Disable GPIOx output buffer. */
    error |= BCC_WriteRegisterGlobal(drvConfig, BCC_REG_SYS_DIAG_ADDR, 0x0000);

    if (error != BCC_STATUS_SUCCESS)
    {
        return error;
    }

    /* 7. Exit diagnostic mode. */
    for(cid = 1; cid <= drvConfig->devicesCnt; cid++)
    {
        error |= BCC_ExitDiagnostics(drvConfig, cid);
    }

    return error;
}

/*FUNCTION**********************************************************************
 *
 * Function Name : BCC_DiagGPIOxOpen
 * Description   : This function implements GPIOx open terminal diagnostics.
 * Execute time :  3.3msms(1 node in daisy-chain)��5.3ms(2 node in daisy-chain)
 *END**************************************************************************/
bcc_status_t BCC_DiagGPIOxOpen(bcc_drv_config_t* const drvConfig, uint16_t* openStatus)
{
    uint16_t regVal;       /* Value of GPIO_CFG1 register. */
    uint16_t gpioCfg1Val[drvConfig->devicesCnt];  /* Origin value of GPIO_CFG1 register. */
    bcc_status_t error = BCC_STATUS_SUCCESS;
    uint8_t cid = 1;


    /* 1. Enter diagnostic mode. */
    for(cid = 1; cid <= drvConfig->devicesCnt; cid++)
    {
        error |= BCC_EnterDiagnostics(drvConfig, cid);
    }
    if (error != BCC_STATUS_SUCCESS)
    {
        return error;
    }

    /* Clear GPIOx short flags. */
    error = BCC_WriteRegisterGlobal(drvConfig, BCC_REG_GPIO_SHORT_ADDR, 0x0000U);
    if (error != BCC_STATUS_SUCCESS)
    {
        return error;
    }

    /* Store original value of GPIO_CFG1 register. */
    for(cid = 1; cid <= drvConfig->devicesCnt; cid++)
    {
        error |= BCC_ReadRegisters(drvConfig, cid, BCC_REG_GPIO_CFG1_ADDR, 1U, &gpioCfg1Val[cid-1]);
    }
    if (error != BCC_STATUS_SUCCESS)
    {
        return BCC_STATUS_SUCCESS;
    }

    /* 2. Program GPIOx to be tested as analog input. */
    regVal = BCC_GPIOX_AN_IN_RM_MEAS(0U) | BCC_GPIOX_AN_IN_RM_MEAS(1U) |
                BCC_GPIOX_AN_IN_RM_MEAS(2U) | BCC_GPIOX_AN_IN_RM_MEAS(3U) |
                BCC_GPIOX_AN_IN_RM_MEAS(4U) | BCC_GPIOX_AN_IN_RM_MEAS(5U) |
                BCC_GPIOX_AN_IN_RM_MEAS(6U);
    error = BCC_WriteRegisterGlobal(drvConfig, BCC_REG_GPIO_CFG1_ADDR, regVal);
    if (error != BCC_STATUS_SUCCESS)
    {
        return error;
    }

    /* 3. Activate GPIOx weak pull-down.
     * Note: In Diagnostic mode, only GPIOx configured analog have a weak
     * pull-down activated by the ANx_OL_DIAG bit. */
    error = BCC_WriteRegisterGlobal(drvConfig, BCC_REG_SYS_DIAG_ADDR, BCC_ANX_OL_DIAG_ENABLED);
    if (error != BCC_STATUS_SUCCESS)
    {
        return error;
    }

    /* 4. Voltages below the VOL(TH) threshold set the GPIO_SHORT_AN_OPEN_STS
     * [ANx_OPEN] bit and the FAULT2_STATUS[AN_OPEN_FLT] fault bit. */
    for(cid = 1; cid <= drvConfig->devicesCnt; cid++)
    {
        error |= BCC_ReadRegisters(drvConfig, cid, BCC_REG_GPIO_SHORT_ADDR, 1U, &openStatus[cid-1]);
    }
    if (error != BCC_STATUS_SUCCESS)
    {
        return error;
    }

    /* Restore original content of registers. */
    for(cid = 1; cid <= drvConfig->devicesCnt; cid++)
    {
        error |= BCC_WriteRegister(drvConfig, cid, BCC_REG_GPIO_CFG1_ADDR, gpioCfg1Val[cid-1]);
    }
    if (error != BCC_STATUS_SUCCESS)
    {
        return BCC_STATUS_SUCCESS;
    }

    /* 5. Deactivate GPIOx weak pull-down. */
    error = BCC_WriteRegisterGlobal(drvConfig, BCC_REG_SYS_DIAG_ADDR, 0x0000);
    if (error != BCC_STATUS_SUCCESS)
    {
        return error;
    }

    /* 6. Exit diagnostic mode. */
    for(cid = 1; cid <= drvConfig->devicesCnt; cid++)
    {
        error |= BCC_ExitDiagnostics(drvConfig, cid);
    }
    if (error != BCC_STATUS_SUCCESS)
    {
        return error;
    }

    /* Clear GPIOx short flags. */
    return BCC_WriteRegisterGlobal(drvConfig, BCC_REG_GPIO_SHORT_ADDR, 0x0000U);
}

/*FUNCTION**********************************************************************
 *
 * Function Name : BCC_DiagCBxOpen
 * Description   : This function implements Cell balance open load detection.
 * Execute time  : 5.25msms(1 node in daisy-chain)��9.7ms(2 node in daisy-chain)
 *END**************************************************************************/
bcc_status_t BCC_DiagCBxOpen(bcc_drv_config_t* const drvConfig, bcc_diag_cbx_open_res_t* results)
{
    uint16_t regVal;      /* Value of SYS_CFG2[NUMB_ODD]. */
    bcc_status_t error = BCC_STATUS_SUCCESS;
    uint8_t cid = 1;


    /* Clear CB OL faults. */
    error = BCC_WriteRegisterGlobal(drvConfig, BCC_REG_CB_OPEN_FLT_ADDR, 0x0000U);

    /* 1. Enter diagnostic mode. */
    for(cid = 1; cid <= drvConfig->devicesCnt; cid++)
    {
        error |= BCC_EnterDiagnostics(drvConfig, cid);
    }
    if (error != BCC_STATUS_SUCCESS)
    {
        return error;
    }


    for(cid = 1; cid <= drvConfig->devicesCnt; cid++)
    {
        /* 2. If the number of cells on the cluster is odd, then write the bit
         * SYS_CFG2[NUMB_ODD] to logic 1, else write it to logic 0. */
		regVal = (drvConfig->cellCnt[cid - 1] & 0x01U) ? BCC_ODD_CELLS : BCC_EVEN_CELLS;
		error |= BCC_UpdateRegister(drvConfig, cid, BCC_REG_SYS_CFG2_ADDR, BCC_RW_NUMB_ODD_MASK, regVal);
	    /* 3. Pause cell balancing by setting SYS_CFG1[CB_MANUAL_PAUSE] to logic 1. */
	    error |= BCC_UpdateRegister(drvConfig, cid, BCC_REG_SYS_CFG1_ADDR,
	                BCC_RW_CB_MANUAL_PAUSE_MASK, BCC_CB_MAN_PAUSE_ENABLED);
    }
    if (error != BCC_STATUS_SUCCESS)
    {
        return error;
    }

    for(cid = 1; cid <= drvConfig->devicesCnt; cid++)
	{
		/* 4. Command fault detection switches (close odd, open even). */
		error = BCC_CommandSwitches(drvConfig, cid, BCC_SWITCH_SEL_CB,
					BCC_SWITCH_POS_CLOSED, BCC_SWITCH_POS_OPEN);

		/* 5. Wait for the time delay t_delay. */
		BCC_WaitUs(100);

		/* 6. Read CB_OPEN_FLT to determine all CBx_OPEN_FLT bits. */
		error |= BCC_ReadRegisters(drvConfig, cid, BCC_REG_CB_OPEN_FLT_ADDR, 1U,
					&(results[cid-1].cbxOpenStatusEven));

		/* 7. Command fault detection switches (open odd, close even). */
		error |= BCC_CommandSwitches(drvConfig, cid, BCC_SWITCH_SEL_CB,
					BCC_SWITCH_POS_OPEN, BCC_SWITCH_POS_CLOSED);

		/* 8. Wait for the time delay t_delay. */
		BCC_WaitUs(100);

		/* 9. Read CB_OPEN_FLT to determine all CBx_OPEN_FLT bits. */
		error |= BCC_ReadRegisters(drvConfig, cid, BCC_REG_CB_OPEN_FLT_ADDR, 1U,
					&(results[cid-1].cbxOpenStatusOdd));
		if (error != BCC_STATUS_SUCCESS)
		{
			return error;
		}
	}

    /* Restore original registers settings. */
    /* 10. Command fault detection switches (all opened). */
    error = BCC_WriteRegisterGlobal(drvConfig, BCC_REG_SYS_DIAG_ADDR, 0x0000);
    /* Unpause cell balancing. */
    for(cid = 1; cid <= drvConfig->devicesCnt; cid++)
	{
		error |= BCC_UpdateRegister(drvConfig, cid, BCC_REG_SYS_CFG1_ADDR,
					BCC_RW_CB_MANUAL_PAUSE_MASK, BCC_CB_MAN_PAUSE_DISABLED);
	}

    /* 11. Exit diagnostic mode. */
    for(cid = 1; cid <= drvConfig->devicesCnt; cid++)
	{
        error |= BCC_ExitDiagnostics(drvConfig, cid);
	}
	if (error != BCC_STATUS_SUCCESS)
	{
		return error;
	}
    /* Clear CB OL faults. */
    error = BCC_WriteRegisterGlobal(drvConfig, BCC_REG_CB_OPEN_FLT_ADDR, 0x0000U);

    return error;
}
