/*
 * Copyright 2016 - 2018 NXP
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * o Redistributions of source code must retain the above copyright notice, this list
 *   of conditions and the following disclaimer.
 *
 * o Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * o Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from this
 *   software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*!
 * File: s32k1xx_peripheries.h
 *
 * This file implements functions for LPSPI and GPIO operations required by BCC
 * driver. It is closely related to this demo example.
 */

#ifndef BCC_PERIPHERIES_H_
#define BCC_PERIPHERIES_H_

#include "pins_driver.h"
#include "lpspi_master_driver.h"
#include "lpspi_slave_driver.h"
#include "../mc33771c/mc33771c.h"
#include "device_registers.h"
#include "clock_manager.h"
#include "lpit_driver.h"

/*******************************************************************************
 * Definitions
 ******************************************************************************/

/*!
 * @addtogroup macro_group
 * @{
 */

/*!< Gets needed cycles for specified delay in milliseconds, calculation is based on core clock frequency. */
#define BCC_GET_CYCLES_FOR_MS(ms, freq) (((freq) / 1000U) * (ms))

/*!< Gets needed cycles for specified delay in microseconds, calculation is based on core clock frequency. */
#define BCC_GET_CYCLES_FOR_US(us, freq) (((freq) / 1000U) * (us) / 1000U)
/*! @} */

/*! @brief Timeout used for SPI/TPL communication. */
#define BCC_COM_TIMEOUT           (BCC_GetSystemClockFreq() / 100U)

/* EN - PTC9 (TPL only) */
#define BCC_EN_INSTANCE           PTC
#define BCC_EN_INDEX              9

/* INTB - PTC14 (TPL only) */
#define BCC_INTB_INSTANCE         PTA
#define BCC_INTB_INDEX            7

/* RST - PTD4 (SPI only) */
#define BCC_RST_INSTANCE          PTD
#define BCC_RST_INDEX             4

/* LPSPI configuration (SPI only). */
#define BCC_SPI_LPSPI_INSTANCE    0 /* LPSPI0 */
#define BCC_SPI_LPSPI_BAUD        1000000
#define BCC_SPI_LPSPI_SRCCLK      lpspiCom1_MasterConfig0.lpspiSrcClk
/* CSB - PTB5 */
#define BCC_SPI_CSB_INSTANCE      PTB
#define BCC_SPI_CSB_PIN           5

/* LPSPI_TX configuration (TPL only). */
#define BCC_TPL_TX_LPSPI_INSTANCE 0 /* LPSPI1 */
#define BCC_TPL_TX_LPSPI_BAUD     2000000
#define BCC_TPL_TX_LPSPI_SRCCLK   lpspiCom1_MasterConfig0.lpspiSrcClk
/* CSB_TX - PTE9 */
#define BCC_TPL_TX_CSB_INSTANCE   PTB
#define BCC_TPL_TX_CSB_PIN        5

/* LPSPI_RX configuration (TPL only). */
#define BCC_TPL_RX_LPSPI_INSTANCE 1 /* LPSPI0 */
#define BCC_TPL_RX_CSB            LPSPI_PCS0 /* CSB_RX - PTD3 (LPSPI1_PCS0) */

/*! @} */

/*******************************************************************************
 * API
 ******************************************************************************/
/*!
 * @addtogroup function_group
 * @{
 */

/*!
 * @brief This function configures LPSPI for usage with BCC driver.
 */
status_t BCC_ConfigureLPSPI();

/*!
 * @brief This function sends and receives data via SPI bus. Intended for SPI
 * mode only.
 *
 * @param drvInstance Instance of BCC driver.
 * @param transBuf Pointer to data to be sent.
 * @param recvBuf Pointer to buffer for received data.
 * @param transferByteCount Number of bytes to be transfered.
 *
 * @return bcc_status_t Error code.
 */
bcc_status_t BCC_TransferSpi(uint8_t drvInstance, uint8_t transBuf[],
        uint8_t recvBuf[], uint16_t transferByteCount);

/*!
 * @brief This function sends and receives data via TX and RX SPI buses.
 *  The function does not return until the transfer is complete.
 * Intended for TPL mode only.
 *
 * @param drvInstance Instance of BCC driver.
 * @param transBuf Pointer to data to be sent.
 * @param recvBuf Pointer to buffer for received data.
 * @param transBufLen Number of bytes to be transceived.
 * @param recvBufLen Number of bytes to be received.
 *
 * @return bcc_status_t Error code.
 */
bcc_status_t BCC_TransferTpl_Sync(uint8_t drvInstance, uint8_t transBuf[],
        uint8_t recvBuf[], uint16_t transBufLen, uint16_t recvBufLen);

/*!
 * @brief This function sends data via TX SPI bus, and enable Rx SPI bus to
 * receive data. The function returns immediately after transmit is completed.
 * The user needs to check whether the receiving data is complete using the
 * BCC_GetTransferTpl_Status function.
 * Intended for TPL mode only.
 *
 * @param drvInstance Instance of BCC driver.
 * @param transBuf Pointer to data to be sent.
 * @param recvBuf Pointer to buffer for received data.
 * @param transBufLen Number of bytes to be transceived.
 * @param recvBufLen Number of bytes to be received.
 *
 * @return bcc_status_t Error code.
 */

bcc_status_t BCC_TransferTpl_Async(uint8_t drvInstance, uint8_t transBuf[],
        uint8_t recvBuf[], uint16_t transBufLen, uint16_t recvBufLen);


/*!
 * @brief This function Returns whether the previous TPL transfer is completed.
 * Intended for TPL mode only.
 *
 * @return bcc_status_t Error code.
 */

bcc_status_t BCC_GetTransferTpl_Status(void);

/*!
 * @brief Waits for specified amount of milliseconds.
 *
 * @param delay - Number of milliseconds to wait.
 */
void BCC_WaitMs(uint16_t delay);

/*!
 * @brief Waits for specified amount of microseconds.
 *
 * @param delay - Number of microseconds to wait.
 */
void BCC_WaitUs(uint16_t delay);

/*!
 * @brief Change CS Pin from GPIO mode to CS mode
 *
 */
void BCC_GPIO2CS(void);

/*!
 * @brief Change CS Pin from CS mode to GPIO mode
 *
 */
void BCC_CS2GPIO(void);

/*!
 * @brief Writes logic 0 or 1 to the CSB pin (or CSB_TX in case of TPL mode).
 * This function needs to be implemented by the user.
 *
 * @param drvInstance Instance of BCC driver.
 * @param value - Zero or one to be set to CSB (CSB_TX) pin.
 */
void BCC_WriteCsbPin(uint8_t drvInstance, uint8_t value);

/*!
 * @brief Writes logic 0 or 1 to the RST pin.
 *
 * @param drvInstance Instance of BCC driver.
 * @param value - Zero or one to be set to RST pin.
 */
void BCC_WriteRstPin(uint8_t drvInstance, uint8_t value);

/*!
 * @brief Writes logic 0 or 1 to the EN pin of MC33664.
 *
 * @param drvInstance Instance of BCC driver.
 * @param value - Zero or one to be set to EN pin.
 */
void BCC_WriteEnPin(uint8_t drvInstance, uint8_t value);

/*!
 * @brief Reads logic value of INTB pin of MC33664.
 *
 * @param drvInstance Instance of BCC driver.
 *
 * @return Zero value for logic zero, non-zero value otherwise.
 */
uint32_t BCC_ReadIntbPin(uint8_t drvInstance);
/*! @} */





#endif /* BCC_PERIPHERIES_H_ */
/*******************************************************************************
 * EOF
 ******************************************************************************/
