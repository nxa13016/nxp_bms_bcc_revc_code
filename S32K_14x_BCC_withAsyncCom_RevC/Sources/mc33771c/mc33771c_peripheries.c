/*
 * Copyright 2016 - 2018 NXP
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * o Redistributions of source code must retain the above copyright notice, this list
 *   of conditions and the following disclaimer.
 *
 * o Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * o Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from this
 *   software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * File: s32k1xx_peripheries.c
 *
 * This file implements functions for LPSPI and GPIO operations required by BCC
 * driver. It is closely related to this demo example.
 */

/*******************************************************************************
 * Includes
 ******************************************************************************/

#include "mc33771c/mc33771c_peripheries.h"
#include "Cpu.h"
#include "demo_config.h"

/*******************************************************************************
 * Global variables (constants)
 ******************************************************************************/

/**
 * LPSPI State structure for each LPSPI instance.
 */
lpspi_state_t g_lpspiState[LPSPI_INSTANCE_COUNT];

/*******************************************************************************
 * Prototypes of internal functions
 ******************************************************************************/
/*!
 * @brief Returns SCG system clock frequency.
 *
 * @return SCG system clock frequency.
 */
inline uint32_t BCC_GetSystemClockFreq(void);

/*!
 * @brief Waits for specified amount of cycles which is given by 32bit
 *        value range. Assumption for this function is that target
 *        architecture is using 32bit general purpose registers.
 *
 * @param cycles - Number of cycles to wait.
 */
inline void BCC_WaitCycles(uint32_t cycles);

/*******************************************************************************
 * Code
 ******************************************************************************/

/*FUNCTION**********************************************************************
 *
 * Function Name : BCC_ConfigureLPSPI
 * Description   : This function configures LPSPI for usage with BCC driver.
 *
 *END**************************************************************************/
status_t BCC_ConfigureLPSPI()
{
    lpspi_master_config_t bccSpiSdkMasterConfig;
#ifdef TPL
    lpspi_slave_config_t bccSpiSdkSlaveConfig;
#endif
    status_t status;

    /* Master SPI interface. */
#ifdef TPL
    if ((BCC_TPL_TX_LPSPI_BAUD < BCC_SPI_FREQ_MC33664_MIN) ||
        (BCC_TPL_TX_LPSPI_BAUD > BCC_SPI_FREQ_MC33664_MAX))
    {
        return STATUS_UNSUPPORTED;
    }
#else
    if (BCC_SPI_LPSPI_BAUD > BCC_SPI_FREQ_MC3377x_MAX)
    {
        return STATUS_UNSUPPORTED;
    }
#endif

    bccSpiSdkMasterConfig.bitcount = 8U;
    bccSpiSdkMasterConfig.callback = NULL;
    bccSpiSdkMasterConfig.callbackParam = NULL;
    bccSpiSdkMasterConfig.clkPhase = LPSPI_CLOCK_PHASE_2ND_EDGE;
    bccSpiSdkMasterConfig.clkPolarity = LPSPI_ACTIVE_LOW;
    bccSpiSdkMasterConfig.isPcsContinuous = true;
    bccSpiSdkMasterConfig.lsbFirst= false;
    bccSpiSdkMasterConfig.pcsPolarity = LPSPI_ACTIVE_LOW;
    bccSpiSdkMasterConfig.rxDMAChannel = 2U;
    bccSpiSdkMasterConfig.transferType = LPSPI_USING_DMA;
    bccSpiSdkMasterConfig.txDMAChannel = 0U;
    bccSpiSdkMasterConfig.whichPcs = LPSPI_PCS0;

#ifdef TPL
    bccSpiSdkMasterConfig.bitsPerSec = BCC_TPL_TX_LPSPI_BAUD;
    bccSpiSdkMasterConfig.lpspiSrcClk = BCC_TPL_TX_LPSPI_SRCCLK;

    status = LPSPI_DRV_MasterInit(BCC_TPL_TX_LPSPI_INSTANCE,
                &(g_lpspiState[BCC_TPL_TX_LPSPI_INSTANCE]), &bccSpiSdkMasterConfig);
#else
    bccSpiSdkMasterConfig.bitsPerSec = BCC_SPI_LPSPI_BAUD;
    bccSpiSdkMasterConfig.lpspiSrcClk = BCC_SPI_LPSPI_SRCCLK;

    status = LPSPI_DRV_MasterInit(BCC_SPI_LPSPI_INSTANCE,
                &(g_lpspiState[BCC_SPI_LPSPI_INSTANCE]), &bccSpiSdkMasterConfig);
#endif

    if (status != STATUS_SUCCESS)
    {
        return status;
    }

    /* Set CSB (TX SPI in TPL mode) pin direction and initial value. */
#ifdef TPL
    PINS_DRV_SetPinDirection(BCC_TPL_TX_CSB_INSTANCE, BCC_TPL_TX_CSB_PIN, 1);
    PINS_DRV_SetPins(BCC_TPL_TX_CSB_INSTANCE, 1U << BCC_TPL_TX_CSB_PIN);
#else
    PINS_DRV_SetPinDirection(BCC_SPI_CSB_INSTANCE, BCC_SPI_CSB_PIN, 1);
    PINS_DRV_SetPins(BCC_SPI_CSB_INSTANCE, 1U << BCC_SPI_CSB_PIN);
#endif

    /* Slave SPI interface - only in TPL mode. */
#ifdef TPL
    bccSpiSdkSlaveConfig.bitcount = 8U;
    bccSpiSdkSlaveConfig.callback = BCC_Receive_EndNotification;
    bccSpiSdkSlaveConfig.callbackParam = NULL;
    bccSpiSdkSlaveConfig.clkPhase = LPSPI_CLOCK_PHASE_2ND_EDGE;
    bccSpiSdkSlaveConfig.clkPolarity = LPSPI_ACTIVE_LOW;
    bccSpiSdkSlaveConfig.lsbFirst = false;
    bccSpiSdkSlaveConfig.pcsPolarity = LPSPI_ACTIVE_LOW;
    bccSpiSdkSlaveConfig.rxDMAChannel = 1U;
    bccSpiSdkSlaveConfig.transferType = LPSPI_USING_DMA;
    bccSpiSdkSlaveConfig.txDMAChannel = 3U;
    bccSpiSdkSlaveConfig.whichPcs = BCC_TPL_RX_CSB;

    status = LPSPI_DRV_SlaveInit(BCC_TPL_RX_LPSPI_INSTANCE,
                &(g_lpspiState[BCC_TPL_RX_LPSPI_INSTANCE]), &bccSpiSdkSlaveConfig);
    if (status != STATUS_SUCCESS)
    {
        return status;
    }
#endif

    return STATUS_SUCCESS;
}

/*FUNCTION**********************************************************************
 *
 * Function Name : BCC_TransferSpi
 * Description   : This function sends and receives data via SPI bus. Intended
 *                 for SPI mode only.
 *
 *END**************************************************************************/
bcc_status_t BCC_TransferSpi(uint8_t drvInstance, uint8_t transBuf[],
        uint8_t recvBuf[], uint16_t transferByteCount)
{
    uint32_t timeout = BCC_COM_TIMEOUT; /* Communication timeout. */
    status_t error;

    DEV_ASSERT(transBuf != NULL);
    DEV_ASSERT(recvBuf != NULL);

    PINS_DRV_ClearPins(BCC_SPI_CSB_INSTANCE, 1U << BCC_SPI_CSB_PIN);
    BCC_WaitUs(1U);

    error = LPSPI_DRV_MasterTransfer(BCC_SPI_LPSPI_INSTANCE, transBuf, recvBuf, transferByteCount);
    if (error != STATUS_SUCCESS)
    {
        PINS_DRV_SetPins(BCC_SPI_CSB_INSTANCE, 1U << BCC_SPI_CSB_PIN);
        return BCC_STATUS_COM_TX;
    }

    /* Wait until transfer is finished. */
    while ((LPSPI_DRV_MasterGetTransferStatus(BCC_SPI_LPSPI_INSTANCE, NULL) == STATUS_BUSY) && (timeout > 0U))
    {
        timeout--;
    }

    BCC_WaitUs(1U);
    PINS_DRV_SetPins(BCC_SPI_CSB_INSTANCE, 1U << BCC_SPI_CSB_PIN);

    if (timeout == 0U)
    {
        // Cancel transmission and reception of data.
        LPSPI_DRV_MasterAbortTransfer(BCC_SPI_LPSPI_INSTANCE);
        return BCC_STATUS_COM_TIMEOUT;
    }

    return BCC_STATUS_SUCCESS;
}

/*FUNCTION**********************************************************************
 *
 * Function Name : BCC_TransferTpl_Sync
 * Description   : This function sends and receives data via TX and RX SPI buses.
 *                 The function does not return until the transfer is complete.
 *
 *END**************************************************************************/
bcc_status_t BCC_TransferTpl_Sync(uint8_t drvInstance, uint8_t transBuf[],
        uint8_t recvBuf[], uint16_t transBufLen, uint16_t recvBufLen)
{
    uint32_t timeout = BCC_COM_TIMEOUT/500;    /* Communication timeout. */
    bcc_status_t bccError = STATUS_SUCCESS;
    status_t error;

    DEV_ASSERT(transBuf != NULL);
    DEV_ASSERT(recvBuf != NULL);

    /* Start reading (response) before transmission. The reason is that they
     * occur at the same time (almost).
     * Tx buffer needs to be passed because of the SPI S32K SDK driver. As MOSI
     * is not used by the SPI instance, Rx buffer is used also as a Tx buffer. */
    error = LPSPI_DRV_SlaveTransfer(BCC_TPL_RX_LPSPI_INSTANCE, recvBuf, recvBuf, recvBufLen);
    if (error != STATUS_SUCCESS)
    {
        return BCC_STATUS_COM_TX;
    }

    /* Send data. */
    error = LPSPI_DRV_MasterTransfer(BCC_TPL_TX_LPSPI_INSTANCE, transBuf, transBuf, transBufLen);
//    LPSPI_DRV_MasterTransferBlocking(BCC_TPL_TX_LPSPI_INSTANCE, buf, buf, bufLen, 3);
    if (error == STATUS_BUSY)
    {
    	bccError = BCC_STATUS_COM_TX;
    }
    else if(error == STATUS_SUCCESS)
    {
		while ((LPSPI_DRV_MasterGetTransferStatus(BCC_TPL_TX_LPSPI_INSTANCE, NULL) == STATUS_BUSY) && (timeout > 0U))
		{
			/* Wait until it's finished. */
		   timeout--;
		}
    }

    if (timeout == 0)
    {
        /* Cancel transmission of data. */
        LPSPI_DRV_MasterAbortTransfer(BCC_TPL_TX_LPSPI_INSTANCE);
        LPSPI_DRV_SlaveAbortTransfer(BCC_TPL_RX_LPSPI_INSTANCE);
        return BCC_STATUS_COM_TIMEOUT;
    }

    /*Time between two consecutive message request transmitted*/
    BCC_WaitUs(4U);

    while ((LPSPI_DRV_SlaveGetTransferStatus(BCC_TPL_RX_LPSPI_INSTANCE, NULL) == STATUS_BUSY))
    {
        /* Wait until it's finished.
        * Note: it is not necessary to check length of received data (already
        * done in GetBlockSentStatus function. */
        timeout--;
        if(timeout == 0)
        	break;
    }

    if (timeout == 0U)
    {
        /* Cancel reception of data. */
        LPSPI_DRV_SlaveAbortTransfer(BCC_TPL_RX_LPSPI_INSTANCE);
        return BCC_STATUS_COM_TIMEOUT;
    }

    return BCC_STATUS_SUCCESS;
}

/*FUNCTION**********************************************************************
 *
 * Function Name : BCC_TransferTpl_Async
 * Description   : This function sends data via TX SPI bus, and enable Rx SPI bus to
 *                 receive data. The function returns immediately after transmit is completed.
 *                 The user needs to check whether the receiving data is complete using the
 *                 BCC_GetTransferTpl_Status function.
 *
 *END**************************************************************************/
bcc_status_t BCC_TransferTpl_Async(uint8_t drvInstance, uint8_t transBuf[],
        uint8_t recvBuf[], uint16_t transBufLen, uint16_t recvBufLen)
{
    bcc_status_t bccError;
    status_t error;
    uint32_t timeout = BCC_COM_TIMEOUT/500;    /* Communication timeout. */

    DEV_ASSERT(transBuf != NULL);
    DEV_ASSERT(recvBuf != NULL);

    /* Start reading (response) before transmission. The reason is that they
     * occur at the same time (almost).
     * Tx buffer needs to be passed because of the SPI S32K SDK driver. As MOSI
     * is not used by the SPI instance, Rx buffer is used also as a Tx buffer. */
    error = LPSPI_DRV_SlaveTransfer(BCC_TPL_RX_LPSPI_INSTANCE, recvBuf, recvBuf, recvBufLen);
    if (error != STATUS_SUCCESS)
    {
    	LPSPI_DRV_SlaveAbortTransfer(BCC_TPL_RX_LPSPI_INSTANCE);
        return BCC_STATUS_COM_TX;
    }

    /* Send data. */
    error = LPSPI_DRV_MasterTransfer(BCC_TPL_TX_LPSPI_INSTANCE, transBuf, transBuf, transBufLen);
    if (error == STATUS_BUSY)
    {
        return BCC_STATUS_COM_TX;
    }

    /*Time between two consecutive message request transmitted, do not decrease this value*/
//    BCC_WaitUs(20U);

    while ((LPSPI_DRV_MasterGetTransferStatus(BCC_TPL_TX_LPSPI_INSTANCE, NULL) == STATUS_BUSY))
    {
        /* Wait until it's finished.
        * Note: it is not necessary to check length of received data (already
        * done in GetBlockSentStatus function. */
        timeout--;
        if(timeout == 0)
        	break;
    }

    if (timeout == 0)
    {
        /* Cancel transmission of data. */
        LPSPI_DRV_MasterAbortTransfer(BCC_TPL_TX_LPSPI_INSTANCE);
        LPSPI_DRV_SlaveAbortTransfer(BCC_TPL_RX_LPSPI_INSTANCE);
        return BCC_STATUS_COM_TIMEOUT;
    }

    return BCC_STATUS_SUCCESS;
}

/*FUNCTION**********************************************************************
 *
 * Function Name : BCC_GetTransferTpl_Status
 * Description   : This function Returns whether the previous TPL transfer is completed
 *                 If TPL transfer did not success, abort the transfer.
 *
 *END**************************************************************************/
bcc_status_t BCC_GetTransferTpl_Status(void)
{
	status_t error;

	if((LPSPI_DRV_SlaveGetTransferStatus(BCC_TPL_RX_LPSPI_INSTANCE, NULL))!=STATUS_SUCCESS)
    {
		LPSPI_DRV_SlaveAbortTransfer(BCC_TPL_RX_LPSPI_INSTANCE);
		return BCC_STATUS_COM_TIMEOUT;
	}

	return BCC_STATUS_SUCCESS;
}

/*FUNCTION**********************************************************************
 *
 * Function Name : BCC_WriteCsbPin
 * Description   : Writes logic 0 or 1 to the CSB pin (or CSB_TX in case of TPL
 *                 mode).
 *
 *END**************************************************************************/
inline void BCC_WriteCsbPin(uint8_t drvInstance, uint8_t value)
{
#ifdef TPL
    PINS_DRV_WritePin(BCC_TPL_TX_CSB_INSTANCE, BCC_TPL_TX_CSB_PIN, value);
#else
    PINS_DRV_WritePin(BCC_SPI_CSB_INSTANCE, BCC_SPI_CSB_PIN, value);
#endif
}

/*FUNCTION**********************************************************************
 *
 * Function Name : BCC_WriteRstPin
 * Description   : Writes logic 0 or 1 to the RST pin.
 *
 *END**************************************************************************/
void BCC_WriteRstPin(uint8_t drvInstance, uint8_t value)
{
#ifndef TPL
    PINS_DRV_WritePin(BCC_RST_INSTANCE, BCC_RST_INDEX, value);
#endif
}

/*FUNCTION**********************************************************************
 *
 * Function Name : BCC_WriteEnPin
 * Description   : Writes logic 0 or 1 to the EN pin of MC33664.
 *
 *END**************************************************************************/
void BCC_WriteEnPin(uint8_t drvInstance, uint8_t value)
{
    PINS_DRV_WritePin(BCC_EN_INSTANCE, BCC_EN_INDEX, value);
}

/*FUNCTION**********************************************************************
 *
 * Function Name : BCC_WriteEnPin
 * Description   : Reads logic value of INTB pin of MC33664.
 *
 *END**************************************************************************/
uint32_t BCC_ReadIntbPin(uint8_t drvInstance)
{
    return (PINS_DRV_ReadPins(BCC_INTB_INSTANCE) >> BCC_INTB_INDEX) & 1;
}


#if defined(__thumb__) && !defined(__thumb2__) /* Thumb instruction set only */
/**
 * @brief Waits for exact number of cycles which can be expressed as multiple of 4.
 *
 * MOV - 1 cycle
 * SUB - 1 cycle
 * BNE - 1 cycle or 2 cycles if jump is realized
 *
 * Output list (empty) - which registers are output and how to map them to C code.
 * Input list (Cycles) - which registers are input and how to map them to C code.
 * Clobber list (r0, r1, cc) - which registers might have changed during
 * execution of asm code (compiler will have to reload them).
 *
 * @param Cycles | Number of cycles to wait.
 */
#define BCC_WAIT_FOR_MUL4_CYCLES(cycles) \
  __asm( \
    "mov r0, %[cycles] \n\t" \
    "0: \n\t"                \
      "sub r0, #4 \n\t"      \
      "nop \n\t"             \
    "bne 0b \n\t"            \
     :                       \
     : [cycles] "r" (cycles) \
     : "r0", "r1", "cc"      \
  )

#else /* Thumb2 or A32 instruction set */

/**
 * @brief Waits for exact number of cycles which can be expressed as multiple of 4.
 *
 * @param Cycles | Number of cycles to wait.
 */
#define BCC_WAIT_FOR_MUL4_CYCLES(cycles) \
  __asm( \
    "movs r0, %[cycles] \n"  \
    "0: \n"                  \
      "subs r0, r0, #4 \n"   \
      "nop \n\t"             \
    "bne 0b \n"              \
     :                       \
     : [cycles] "r" (cycles) \
     : "r0", "r1", "cc"      \
  )

#endif

/*******************************************************************************
 * Code
 ******************************************************************************/

/*FUNCTION**********************************************************************
 *
 * Function Name : BCC_GetSystemClockFreq
 * Description   : Returns SCG system clock frequency.
 *
 *END**************************************************************************/
uint32_t BCC_GetSystemClockFreq(void)
{
    uint32_t freq;
    CLOCK_SYS_GetFreq(0, &freq);
    return freq;
}

/*FUNCTION**********************************************************************
 *
 * Function Name : BCC_WaitCycles
 * Description   : Waits for specified amount of cycles which is given by 32bit
 *                 value range. Assumption for this function is that target
 *                 architecture is using 32bit general purpose registers.
 *
 *END**************************************************************************/
void BCC_WaitCycles(uint32_t cycles)
{
    /* Advance to next multiple of 4. Value 0x04U ensures that the number
     * is not zero. */
    cycles = (cycles & 0xFFFFFFFCU) | 0x04U;

    BCC_WAIT_FOR_MUL4_CYCLES(cycles);

}

/*FUNCTION**********************************************************************
 *
 * Function Name : BCC_WaitMs
 * Description   : Waits for specified amount of milliseconds.
 *
 *END**************************************************************************/
void BCC_WaitMs(uint16_t delay)
{
//   uint32_t cycles = (uint32_t) BCC_GET_CYCLES_FOR_MS(1U, BCC_GetSystemClockFreq());
	uint32_t cycles = 48*1000; //System clock frequency = 48MHz
    /* Advance to multiple of 4. */
    cycles = cycles & 0xFFFFFFFCU;

    for (; delay > 0U; delay--) {
        BCC_WAIT_FOR_MUL4_CYCLES(cycles);
    }

}

/*FUNCTION**********************************************************************
 *
 * Function Name : BCC_WaitUs
 * Description   : Waits for specified amount of microseconds.
 *
 *END**************************************************************************/
void BCC_WaitUs(uint16_t delay)
{
//    uint32_t cycles = (uint32_t) BCC_GET_CYCLES_FOR_US(delay, BCC_GetSystemClockFreq());

	uint32_t cycles = delay*48; //System clock frequency = 48MHz
    /* Advance to next multiple of 4. Value 0x04U ensures that the number
     * is not zero. */
    cycles = (cycles & 0xFFFFFFFCU) | 0x04U;
    BCC_WAIT_FOR_MUL4_CYCLES(cycles);
}

/*FUNCTION**********************************************************************
 *
 * Function Name : BCC_CS2GPIO
 * Description   : Change TX SPI CS pin from CS mode to GPIO mode
 *
 *END**************************************************************************/
void BCC_CS2GPIO(void)
{
	pin_settings_config_t g_pin_mux_InitConfig =
	{
        .base          = PORTB,
        .pinPortIdx    = 5u,
        .pullConfig    = PORT_INTERNAL_PULL_NOT_ENABLED,
        .passiveFilter = false,
        .driveSelect   = PORT_LOW_DRIVE_STRENGTH,
        .mux           = PORT_MUX_AS_GPIO,
        .pinLock       = false,
        .intConfig     = PORT_DMA_INT_DISABLED,
        .clearIntFlag  = false,
        .gpioBase      = PTD,
        .direction     = GPIO_OUTPUT_DIRECTION,
        .digitalFilter = false,
        .initValue     = 0u,
    };

    PINS_Init(&g_pin_mux_InitConfig);
}

/*FUNCTION**********************************************************************
 *
 * Function Name : BCC_GPIO2CS
 * Description   : Change TX SPI CS pin from GPIO mode to CS mode
 *
 *END**************************************************************************/

void BCC_GPIO2CS(void)
{
	pin_settings_config_t g_pin_mux_InitConfig =
    {
        .base          = PORTB,
        .pinPortIdx    = 5u,
        .pullConfig    = PORT_INTERNAL_PULL_NOT_ENABLED,
        .passiveFilter = false,
        .driveSelect   = PORT_LOW_DRIVE_STRENGTH,
        .mux           = PORT_MUX_ALT4,
        .pinLock       = false,
        .intConfig     = PORT_DMA_INT_DISABLED,
        .clearIntFlag  = false,
        .gpioBase      = NULL,
        .digitalFilter = false,
    };

    PINS_Init(&g_pin_mux_InitConfig);
}

/*******************************************************************************
 * EOF
 ******************************************************************************/
