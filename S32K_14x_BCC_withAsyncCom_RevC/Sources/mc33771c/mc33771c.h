/*
 * Copyright 2016 - 2018 NXP
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * o Redistributions of source code must retain the above copyright notice, this list
 *   of conditions and the following disclaimer.
 *
 * o Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * o Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from this
 *   software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*!
 * @file bcc.h
 *
 * Battery cell controller SW driver.
 * Supports boards based on MC33771C.
 *
 * This module is common for all supported models.
 */

#ifndef __BCC_H__
#define __BCC_H__

/*******************************************************************************
 * Includes
 ******************************************************************************/

#include <stdbool.h>
#include <stdint.h>
#include <stddef.h>

#include "bcc_mc33771c.h"

/*******************************************************************************
 * User definitions
 ******************************************************************************/

/*! @brief Timeout used for BCC wake-up (in BCC_EnableTPL function). */
#define BCC_WAKEUP_TIMEOUT        480000

/*! @brief Use #define BCC_MSG_BIGEND for big-endian format of the TX/RX SPI
 *  buffer ([0] DATA_H, [1] DATA_L, ..., [4] CRC). If BCC_MSG_BIGEND is not
 *  defined, little-endian is used ([0] CRC, ..., [3] DATA_L, [4] DATA_H) */
//#define BCC_MSG_BIGEND

/*! @brief Max. number of frames that can be read at once (one request and
 *  more responses) in the TPL mode. The admission range is from 0x01U to
 *  0x7FU value (see NRT in datasheet). It directly influences the size
 *  of RX buffer in bcc_drv_data_t.
 *
 *  Note that it was shown there are problems with reading more than two
 *  registers at once at 48 MHz S32K144 core clock and 24 MHz bus clock with
 *  current implementation of the S32K144 LPSPI driver (First bytes of
 *  receive buffer are filled by proper values, the others contain 0x0000
 *  and reading ends with BCC_STATUS_CRC error code).
 *
 *  When the core clock speed is increased enough, it is possible to read
 *  all registers at once. In this case you can change BCC_RX_LIMIT_TPL
 *  to up to 0x7FU.
 */
#define BCC_RX_LIMIT_TPL          0x70U

/*! @brief Number of averaged cell voltage measurements in Cell voltage channel
 *  functional verification. BCC_DIAG_CVFV_MEAS_NUM should be at least 6U
 *  according to the safety manual. */
#define BCC_DIAG_CVFV_MEAS_NUM    6U

/*! @brief Number of averaged cell voltage measurements in Cell Terminal Leakage
 *  Diagnostics. BCC_DIAG_LEAK_MEAS_NUM should be at least 4 according to the
 *  safety manual. */
#define BCC_DIAG_LEAK_MEAS_NUM    4U

/*! @brief Minimal measured voltage [mV] of contacts with abnormally high
 *  resistance. To avoid false alarms the following constraint shall be met:
 *  BCC_DIAG_DV >= R_connector(max) * I_cell_balance(max). */
#define BCC_DIAG_DV               30

/* NTC precomputed table configuration. */
/*! @brief Minimal temperature in NTC table.
 *
 * Admissible range is from -50 to 100.
 *
 * It directly influences size of the NTC table (number of precomputed values).
 * Specifically lower boundary.
 */
#define BCC_NTC_MINTEMP           (-40)

/*! @brief Maximal temperature in NTC table.
 *
 * Admissible range is from 100 to 200.
 *
 * It directly influences size of the NTC table (number of precomputed values).
 * Specifically higher boundary.
 */
#define BCC_NTC_MAXTEMP           (120)

/*******************************************************************************
 * Definitions
 ******************************************************************************/

/*! @brief Maximal number of Battery Cell Controller devices in SPI mode. */
#define BCC_DEVICE_CNT_MAX_SPI    1U
/*! @brief Maximal number of Battery Cell Controller devices in TPL mode. */
#define BCC_DEVICE_CNT_MAX_TPL    63U
/*! @brief Maximal number of Battery Cell Controller devices. */
#define BCC_DEVICE_CNT_MAX        BCC_DEVICE_CNT_MAX_TPL

/*! @brief Minimal battery cell count connected to MC33771. */
#define BCC_MIN_CELLS_MC33771     7U
/*! @brief Maximal battery cell count connected to MC33771. */
#define BCC_MAX_CELLS_MC33771     14U
/*! @brief Maximal battery cell count connected to any BCC device. */
#define BCC_MAX_CELLS             14U

/*! @brief Maximal frequency of SPI clock in SPI mode. */
#define BCC_SPI_FREQ_MC3377x_MAX  4200000U
/*! @brief Minimal frequency of SPI_TX clock in TPL mode. */
#define BCC_SPI_FREQ_MC33664_MIN  1900000U
/*! @brief Maximal frequency of SPI_TX clock in TPL mode. */
#define BCC_SPI_FREQ_MC33664_MAX  2100000U

/*! @brief  Number of MC33771 registers configured by the user
 * values at the initialization.
 */
#define BCC_INIT_CONF_REG_CNT     46U

/*! @brief  Number of MC33771 measurement registers.
 *
 */
#define BCC_MEAS_CNT              30U

/*! @brief  Number of BCC status registers. */
#define BCC_STAT_CNT              11U

/*! @brief Message size in bytes. */
#define BCC_MSG_SIZE              6U

/*! @brief Max. number of frames that can be read at once in SPI mode. */
#define BCC_RX_LIMIT_SPI          0x7FU

/*! @brief Size of buffer that is used for receiving via SPI in TPL mode. */
#define BCC_RX_BUF_SIZE_TPL \
    (BCC_MSG_SIZE * (BCC_RX_LIMIT_TPL + 1U))

/*! @brief Number of GPIO/temperature sensor inputs. */
#define BCC_GPIO_INPUT_CNT        7U

/*! @brief R_PD (CT open load detection pull-down resistor) resistance
 *  in [Ohm]. */
#define BCC_RPD                   950U

/** Diagnostic thresholds. **/
/*! @brief ISENSE diagnostic common mode offset voltage in [uV],
 *  maximal value. */
#define BCC_DIAG_VOFF_MAX         37U
/*! @brief MC33771 ISENSE diagnostic reference in [uV]
 *  with PGA having gain 4. */
#define BCC_DIAG_VREF_MC33771     127000
/*! @brief MC33771 ISENSE diagnostic reference in [uV]
 *  with PGA having gain 4, minimal value. */
#define BCC_DIAG_VREF_MIN_MC33771 124000
/*! @brief MC33771 ISENSE diagnostic reference in [uV]
 *  with PGA having gain 4, maximal value. */
#define BCC_DIAG_VREF_MAX_MC33771 130000

/*! @brief Cell terminal open load V detection threshold [mV],
 *  Type T (1.5 V <= V_CELL <= 2.7 V). */
#define BCC_V_OL_DETECT_T         50U
/*! @brief Cell terminal open load V detection threshold [mV],
 *  Type F (2.5 V <= V_CELL <= 3.7 V). */
#define BCC_V_OL_DETECT_F         100U
/*! @brief Cell terminal open load V detection threshold [mV],
 *  Type N (2.5 V <= V_CELL <= 4.3 V). */
#define BCC_V_OL_DETECT_N         150U
/*! @brief Undervoltage functional verification threshold [mV] in diagnostic mode,
 *  Type T (1.5 V <= V_CELL <= 2.7 V). */
#define BCC_CTX_UV_TH_T           390U
/*! @brief Undervoltage functional verification threshold [mV] in diagnostic mode,
 *  Type F (2.5 V <= V_CELL <= 3.7 V). */
#define BCC_CTX_UV_TH_F           650U
/*! @brief Undervoltage functional verification threshold [mV] in diagnostic mode,
 *  Type N (2.5 V <= V_CELL <= 4.3 V). */
#define BCC_CTX_UV_TH_N           1200U
/*! @brief Overvoltage functional verification threshold [mV] in diagnostic mode,
 *  Type T (1.5 V <= V_CELL <= 2.7 V). */
#define BCC_CTX_OV_TH_T           1800U
/*! @brief Overvoltage functional verification threshold [mV] in diagnostic mode,
 *  Type F (2.5 V <= V_CELL <= 3.7 V). */
#define BCC_CTX_OV_TH_F           4000U
/*! @brief Overvoltage functional verification threshold [mV] in diagnostic mode,
 *  Type N (2.5 V <= V_CELL <= 4.3 V). */
#define BCC_CTX_OV_TH_N           4000U

/*! @brief Number of measurements in ADC1-A and ADC1-B functional verification. */
#define BCC_DIAG_ADC1_MEAS_NUM    6U
/*! @brief Voltage reference [mV] used in ADC1-A,B functional verification. */
#define BCC_V_BGP                 1180U
/*! @brief Maximum tolerance [mV] (5.25mV �� 5mV) between ADC1-A, B and diagnostic reference
 *  (1.5 V <= VCELL <= 4.3 V) in ADC1-A,B functional verification, absolute value. */
#define BCC_ADC1X_FV              5U

/*! @brief Cell voltage channel functional verification allowable error
 *  in CT verification measurement, minimal value in [mV]. */
#define BCC_DIAG_VCVFV_MIN        -22
/*! @brief Cell voltage channel functional verification allowable error
 *  in CT verification measurement, maximal value in [mV]. */
#define BCC_DIAG_VCVFV_MAX        6

/*! @brief Waiting time in [ms] in Cell Terminal Leakage diagnostics containing
 *  the cell balance delay and cell terminal settling time. */
#define BCC_DIAG_TCT_SETTLE       5
/*! @brief MC33771 Cell terminal leakage detection level in [mV].
 *  This value can be used also as negative. */
#define BCC_DIAG_VLEAK_MC33771    27U
/*! @brief ISENSE Open Load injected current [uA]. */
#define BCC_DIAG_ISENSE_OL        130U
/*! @brief ISENSE Open Load detection threshold [uV]. */
#define BCC_DIAG_V_ISENSE_OL      460000U

/*! @brief Vpower line issue detection threshold [mV]. */
#define BCC_V_stack_thre          1000u
/*******************************************************************************
 * Initial register configuration
 ******************************************************************************/

/* Note: INIT register is initialized automatically by the BCC driver. */
/* Note: SYS_CFG_GLOBAL register contains only command GO2SLEEP (no initialization needed). */
/* Note: EEPROM_CTRL, FUSE_MIRROR_DATA and FUSE_MIRROR_CNTL registers are not initialized. */

#define BCC_CONF1_SYS_CFG1_VALUE ( \
	BCC_CYCLIC_TIMER_DISABLED | \
    BCC_DIAG_TIMEOUT_1S | \
	BCC_I_MEAS_DISABLED | \
	BCC_CB_AUTO_PAUSE_DISABLED | \
	BCC_CB_DRV_ENABLED | \
    BCC_DIAG_MODE_DISABLED | \
    BCC_CB_MAN_PAUSE_DISABLED | \
    BCC_SW_RESET_DISABLED | \
    BCC_FAULT_WAVE_DISABLED | \
    BCC_WAVE_DC_500US |\
    Reserved_BIT_1\
)

/* Initial value of SYS_CFG2 register. */
#define BCC_CONF1_SYS_CFG2_VALUE ( \
	BCC_FLT_RST_CFG_COM_OSC | \
	BCC_TIMEOUT_COMM_256MS | \
    BCC_EVEN_CELLS | \
	BCC_HAMM_ENCOD_DISABLED \
)

/* Initial value of SYS_DIAG register. */
#define BCC_CONF1_SYS_DIAG_VALUE ( \
    BCC_FAULT_PIN_FORCED_HIGH | \
    BCC_IMUX_ISENSE | \
    BCC_ISENSE_OL_DIAG_DISABLED | \
    BCC_ANX_OL_DIAG_DISABLED | \
    BCC_ANX_DIAG_SW_OPEN | \
    BCC_DA_DIAG_NO_CHECK | \
    BCC_POL_NON_INVERTED | \
    BCC_CT_LEAK_DIAG_NORMAL | \
    BCC_CT_OV_UV_DISABLED | \
    BCC_CT_OL_ODD_OPEN | \
    BCC_CT_OL_EVEN_OPEN | \
    BCC_CB_OL_ODD_OPEN | \
    BCC_CB_OL_EVEN_OPEN \
)

/* Initial value of ADC_CFG register. */
#define BCC_CONF1_ADC_CFG_VALUE ( \
    /* Note: TAG_ID is zero. */ \
    /* Note: SOC is disable (i.e. do not initiate on-demand conversion now). */ \
    BCC_ADC2_PGA_AUTO | \
    /* Note: CC_RST is not set (do not reset CC now). */ \
	BCC_ADC1_A_RES_16BIT | \
    BCC_ADC1_B_RES_16BIT | \
    BCC_ADC2_RES_16BIT \
)

/* Initial value of ADC2_OFFSET_COMP register. */
#define BCC_CONF1_ADC2_OFFSET_COMP_VALUE (\
    BCC_READ_CC_RESET | \
	BCC_FREE_CC_ROLL_OVER | \
    BCC_GET_ADC2_OFFSET(0) /* ADC2 offset compensation value. */ \
)

#ifdef MC33771
/* Initial value of OV_UV_EN register. */
#define BCC_CONF1_OV_UV_EN_VALUE ( \
	BCC_CTX_OV_TH_INDIVIDUAL | \
	BCC_CTX_OV_TH_INDIVIDUAL | \
    /* CTs OV and UV enable (bit is 1) or disable (bit is 0). */ \
    0x3FFFU \
)
#else
/* Initial value of OV_UV_EN register. */
#define BCC_CONF1_OV_UV_EN_VALUE ( \
      BCC_CTX_OVUV_ENABLED(14) | \
      BCC_CTX_OVUV_ENABLED(13) | \
      BCC_CTX_OVUV_ENABLED(12) | \
	  BCC_CTX_OVUV_ENABLED(11) | \
	  BCC_CTX_OVUV_ENABLED(10) | \
	  BCC_CTX_OVUV_ENABLED(9) | \
	  BCC_CTX_OVUV_ENABLED(8) | \
	  BCC_CTX_OVUV_ENABLED(7) | \
	  BCC_CTX_OVUV_ENABLED(6) | \
	  BCC_CTX_OVUV_ENABLED(5) | \
	  BCC_CTX_OVUV_ENABLED(4) | \
	  BCC_CTX_OVUV_ENABLED(3) | \
	  BCC_CTX_OVUV_ENABLED(2) | \
	  BCC_CTX_OVUV_ENABLED(1) \
)
#endif

/* Initial value of CELL_OV_FLT register. */
#define BCC_CONF1_CELL_OV_FLT_VALUE 0x0000U

/* Initial value of CELL_UV_FLT register. */
#define BCC_CONF1_CELL_UV_FLT_VALUE 0x0000U


/* Initial value of TPL_CFG register. */
#define BCC_CONF1_CELL_TPL_CFG_VALUE (\
		BCC_TX_IN_STRTH(3) | \
		BCC_TX_OUT_STRTH(3)| \
		BCC_TX_IN_DIFLOAD_120Ohm| \
		BCC_TX_OUT_DIFLOAD_120Ohm| \
		BCC_TX_IN_GNDLOAD_DIS | \
		BCC_TX_OUT_GNDLOAD_DIS /* ADC2 offset compensation value. */ \
	)


/* Initial value of CB1_CFG register. */
#define BCC_CONF1_CB1_CFG_VALUE ( \
	BCC_CB_DISABLED | \
    0xFFU /* Cell balance timer in minutes. */ \
)

/* Initial value of CB2_CFG register. */
#define BCC_CONF1_CB2_CFG_VALUE ( \
	BCC_CB_DISABLED | \
    0xFFU /* Cell balance timer in minutes. */ \
)

/* Initial value of CB3_CFG register. */
#define BCC_CONF1_CB3_CFG_VALUE ( \
	BCC_CB_DISABLED | \
    0xFFU /* Cell balance timer in minutes. */ \
)

/* Initial value of CB4_CFG register. */
#define BCC_CONF1_CB4_CFG_VALUE ( \
	BCC_CB_DISABLED | \
    0xFFU /* Cell balance timer in minutes. */ \
)

/* Initial value of CB5_CFG register. */
#define BCC_CONF1_CB5_CFG_VALUE ( \
	BCC_CB_DISABLED | \
    0xFFU /* Cell balance timer in minutes. */ \
)

/* Initial value of CB6_CFG register. */
#define BCC_CONF1_CB6_CFG_VALUE ( \
	BCC_CB_DISABLED | \
    0xFFU /* Cell balance timer in minutes. */ \
)

/* Initial value of CB7_CFG register. */
#define BCC_CONF1_CB7_CFG_VALUE ( \
	BCC_CB_DISABLED | \
    0xFFU /* Cell balance timer in minutes. */ \
)

/* Initial value of CB8_CFG register. */
#define BCC_CONF1_CB8_CFG_VALUE ( \
	BCC_CB_DISABLED | \
    0xFFU /* Cell balance timer in minutes. */ \
)

/* Initial value of CB9_CFG register. */
#define BCC_CONF1_CB9_CFG_VALUE ( \
	BCC_CB_DISABLED | \
    0xFFU /* Cell balance timer in minutes. */ \
)

/* Initial value of CB10_CFG register. */
#define BCC_CONF1_CB10_CFG_VALUE ( \
	BCC_CB_DISABLED | \
    0xFFU /* Cell balance timer in minutes. */ \
)

/* Initial value of CB11_CFG register. */
#define BCC_CONF1_CB11_CFG_VALUE ( \
	BCC_CB_DISABLED | \
    0xFFU /* Cell balance timer in minutes. */ \
)

/* Initial value of CB12_CFG register. */
#define BCC_CONF1_CB12_CFG_VALUE ( \
	BCC_CB_DISABLED | \
    0xFFU /* Cell balance timer in minutes. */ \
)

/* Initial value of CB13_CFG register. */
#define BCC_CONF1_CB13_CFG_VALUE ( \
	BCC_CB_DISABLED | \
    0xFFU /* Cell balance timer in minutes. */ \
)

/* Initial value of CB14_CFG register. */
#define BCC_CONF1_CB14_CFG_VALUE ( \
	BCC_CB_DISABLED | \
    0xFFU /* Cell balance timer in minutes. */ \
)

/* Initial value of CB_OPEN_FLT register. */
#define BCC_CONF1_CB_OPEN_FLT_VALUE 0x0000U

/* Initial value of CB_SHORT_FLT register. */
#define BCC_CONF1_CB_SHORT_FLT_VALUE 0x0000U

/* Initial value of GPIO_CFG1 register. */
#define BCC_CONF1_GPIO_CFG1_VALUE ( \
    BCC_GPIOX_AN_IN_RM_MEAS(6U) | \
    BCC_GPIOX_AN_IN_RM_MEAS(5U) | \
    BCC_GPIOX_AN_IN_RM_MEAS(4U) | \
    BCC_GPIOX_AN_IN_RM_MEAS(3U) | \
    BCC_GPIOX_AN_IN_RM_MEAS(2U) | \
    BCC_GPIOX_AN_IN_RM_MEAS(1U) | \
    BCC_GPIOX_AN_IN_RM_MEAS(0U) \
)

/* Initial value of GPIO_CFG2 register. */
#define BCC_CONF1_GPIO_CFG2_VALUE ( \
    BCC_GPIO2_ADC_TRG_DISABLED | \
    BCC_GPIO0_NO_WAKE_UP | \
    BCC_GPIO0_INP_HIGH_FP_NACT \
    /* Note: GPIOx_DR are initialized to zero (low output level). */ \
)

/* Initial value of GPIO_STS register. */
#define BCC_CONF1_GPIO_STS_VALUE 0x0000U

/* Initial value of AN_OT_UT_FLT register. */
#define BCC_CONF1_AN_OT_UT_FLT_VALUE 0x0000U

/* Initial value of GPIO_SHORT_ANx_OPEN_STS register. */
#define BCC_CONF1_GPIO_SHORT_VALUE 0x0000U

/* Initial value of FAULT3_STATUS register. */
#define BCC_CONF1_FAULT1_STATUS_VALUE 0x0000U

/* Initial value of FAULT3_STATUS register. */
#define BCC_CONF1_FAULT2_STATUS_VALUE 0x0000U

/* Initial value of FAULT3_STATUS register. */
#define BCC_CONF1_FAULT3_STATUS_VALUE 0x0000U

/* Initial value of FAULT_MASK1 register. */
#define BCC_CONF1_FAULT_MASK1_VALUE ( \
    BCC_VPWR_OV_FLT_DIS | \
    BCC_VPWR_LV_FLT_DIS | \
    BCC_COM_LOSS_FLT_DIS | \
    BCC_COM_ERR_FLT_DIS | \
    BCC_CSB_WUP_FLT_DIS | \
    BCC_GPIO0_WUP_FLT_DIS | \
    BCC_I2C_ERR_FLT_DIS | \
    BCC_IS_OL_FLT_DIS | \
    BCC_IS_OC_FLT_DIS | \
    BCC_AN_OT_FLT_DIS | \
    BCC_AN_UT_FLT_DIS | \
    BCC_CT_OV_FLT_DIS | \
    BCC_CT_UV_FLT_DIS \
)

/* Initial value of FAULT_MASK2 register. */
#define BCC_CONF1_FAULT_MASK2_VALUE ( \
    BCC_VCOM_OV_FLT_DIS | \
    BCC_VCOM_UV_FLT_DIS | \
    BCC_VANA_OV_FLT_DIS | \
    BCC_VANA_UV_FLT_DIS | \
    BCC_ADC1_B_FLT_DIS | \
    BCC_ADC1_A_FLT_DIS | \
    BCC_GND_LOSS_FLT_DIS | \
    BCC_AN_OPEN_FLT_DIS | \
    BCC_GPIO_SHORT_FLT_DIS | \
    BCC_CB_SHORT_FLT_DIS | \
    BCC_CB_OPEN_FLT_DIS | \
    BCC_OSC_ERR_FLT_DIS | \
    BCC_DED_ERR_FLT_DIS | \
    BCC_FUSE_ERR_FLT_DIS \
)

#ifdef MC33771
/* Initial value of FAULT_MASK3 register. */
#define BCC_CONF1_FAULT_MASK3_VALUE ( \
    BCC_CC_OVR_FLT_DIS | \
    BCC_DIAG_TO_FLT_DIS | \
    /* CBx timeout detection (EOT_CBx bits). */ \
    BCC_EOT_CBX_FLT_DIS(1U) |                  /* CB1. */  \
    BCC_EOT_CBX_FLT_DIS(2U) |                  /* CB2. */  \
    BCC_EOT_CBX_FLT_DIS(3U) |                  /* CB3. */  \
    BCC_EOT_CBX_FLT_DIS(4U) |                  /* CB4. */  \
    BCC_EOT_CBX_FLT_DIS(5U) |                  /* CB5. */  \
    BCC_EOT_CBX_FLT_DIS(6U) |                  /* CB6. */  \
    BCC_EOT_CBX_FLT_DIS(7U) |                  /* CB7. */  \
    BCC_EOT_CBX_FLT_DIS(8U) |                  /* CB8. */  \
    BCC_EOT_CBX_FLT_DIS(9U) |                  /* CB9. */  \
    BCC_EOT_CBX_FLT_DIS(10U) |                  /* CB10. */  \
    BCC_EOT_CBX_FLT_DIS(11U) |                  /* CB11. */  \
    BCC_EOT_CBX_FLT_DIS(12U) |                  /* CB12. */  \
    BCC_EOT_CBX_FLT_DIS(13U) |                  /* CB13. */  \
    BCC_EOT_CBX_FLT_DIS(14U)                    /* CB14. */ \
)
#else
/* Initial value of FAULT_MASK3 register. */
#define BCC_CONF1_FAULT_MASK3_VALUE ( \
    BCC_CC_OVR_FLT_EN | \
    BCC_DIAG_TO_FLT_EN | \
    /* CBx timeout detection (EOT_CBx bits). */ \
    BCC_EOT_CBX_FLT_DIS(1U) |                  /* CB1. */  \
    BCC_EOT_CBX_FLT_DIS(2U) |                  /* CB2. */  \
    BCC_EOT_CBX_FLT_DIS(3U) |                  /* CB3. */  \
    BCC_EOT_CBX_FLT_DIS(4U) |                  /* CB4. */  \
    BCC_EOT_CBX_FLT_DIS(5U) |                  /* CB5. */  \
    BCC_EOT_CBX_FLT_DIS(6U)                    /* CB6. */  \
)
#endif

/* Initial value of WAKEUP_MASK1 register. */
#define BCC_CONF1_WAKEUP_MASK1_VALUE ( \
    BCC_VPWR_OV_WAKEUP_DIS | \
    BCC_VPWR_LV_WAKEUP_DIS | \
    BCC_CSB_WUP_WAKEUP_DIS | \
    BCC_GPIO0_WUP_WAKEUP_DIS | \
    BCC_IS_OC_WAKEUP_DIS | \
    BCC_AN_OT_WAKEUP_DIS | \
    BCC_AN_UT_WAKEUP_DIS | \
    BCC_CT_OV_WAKEUP_DIS | \
    BCC_CT_UV_WAKEUP_DIS \
)

/* Initial value of WAKEUP_MASK2 register. */
#define BCC_CONF1_WAKEUP_MASK2_VALUE ( \
    BCC_VCOM_OV_WAKEUP_DIS | \
    BCC_VCOM_UV_WAKEUP_DIS | \
    BCC_VANA_OV_WAKEUP_DIS | \
    BCC_VANA_UV_WAKEUP_DIS | \
    BCC_ADC1_B_WAKEUP_DIS | \
    BCC_ADC1_A_WAKEUP_DIS | \
    BCC_GND_LOSS_WAKEUP_DIS | \
    BCC_IC_TSD_WAKEUP_DIS | \
    BCC_GPIO_SHORT_WAKEUP_DIS | \
    BCC_CB_SHORT_WAKEUP_DIS | \
    BCC_OSC_ERR_WAKEUP_DIS | \
    BCC_DED_ERR_WAKEUP_DIS \
)

#ifdef MC33771
/* Initial value of WAKEUP_MASK3 register. */
#define BCC_CONF1_WAKEUP_MASK3_VALUE ( \
    BCC_CC_OVR_FLT_DIS | \
    BCC_DIAG_TO_FLT_DIS | \
    /* CBx timeout detection (EOT_CBx bits). */ \
    BCC_EOT_CBX_WAKEUP_DIS(1U) |                  /* CB1. */  \
    BCC_EOT_CBX_WAKEUP_DIS(2U) |                  /* CB2. */  \
    BCC_EOT_CBX_WAKEUP_DIS(3U) |                  /* CB3. */  \
    BCC_EOT_CBX_WAKEUP_DIS(4U) |                  /* CB4. */  \
    BCC_EOT_CBX_WAKEUP_DIS(5U) |                  /* CB5. */  \
    BCC_EOT_CBX_WAKEUP_DIS(6U) |                  /* CB6. */  \
    BCC_EOT_CBX_WAKEUP_DIS(7U) |                  /* CB7. */  \
    BCC_EOT_CBX_WAKEUP_DIS(8U) |                  /* CB8. */  \
    BCC_EOT_CBX_WAKEUP_DIS(9U) |                  /* CB9. */  \
    BCC_EOT_CBX_WAKEUP_DIS(10U) |                  /* CB10. */  \
    BCC_EOT_CBX_WAKEUP_DIS(11U) |                  /* CB11. */  \
    BCC_EOT_CBX_WAKEUP_DIS(12U) |                  /* CB12. */  \
    BCC_EOT_CBX_WAKEUP_DIS(13U) |                  /* CB13. */  \
    BCC_EOT_CBX_WAKEUP_DIS(14U)                    /* CB14. */ \
)
#else
/* Initial value of WAKEUP_MASK3 register. */
#define BCC_CONF1_WAKEUP_MASK3_VALUE ( \
    BCC_CC_OVR_FLT_DIS | \
    BCC_DIAG_TO_FLT_DIS | \
    /* CBx timeout detection (EOT_CBx bits). */ \
    BCC_EOT_CBX_WAKEUP_DIS(1U) |                  /* CB1. */  \
    BCC_EOT_CBX_WAKEUP_DIS(2U) |                  /* CB2. */  \
    BCC_EOT_CBX_WAKEUP_DIS(3U) |                  /* CB3. */  \
    BCC_EOT_CBX_WAKEUP_DIS(4U) |                  /* CB4. */  \
    BCC_EOT_CBX_WAKEUP_DIS(5U) |                  /* CB5. */  \
    BCC_EOT_CBX_WAKEUP_DIS(6U)                    /* CB6. */  \
)
#endif

/* Initial value of TH_ALL_CT register. */
#define BCC_CONF1_TH_ALL_CT_VALUE ( \
    BCC_SET_ALL_CT_OV_TH(4000U)  /* CT OV threshold is 1510 mV. It is enabled/disabled through OV_UV_EN register. */ | \
    BCC_SET_ALL_CT_UV_TH(2500U)  /* CT UV threshold is 1000 mV. It is enabled/disabled through OV_UV_EN register. */ \
)

/* Initial value of TH_CT14 register. */
#define BCC_CONF1_TH_CT14_VALUE ( \
    BCC_SET_CTX_OV_TH(4195U)  /* CT OV threshold is 4195 mV. It is enabled/disabled through OV_UV_EN register. */ | \
    BCC_SET_CTX_UV_TH(2509U)  /* CT UV threshold is 2509 mV. It is enabled/disabled through OV_UV_EN register. */ \
)

/* Initial value of TH_CT13 register. */
#define BCC_CONF1_TH_CT13_VALUE ( \
    BCC_SET_CTX_OV_TH(4195U)  /* CT OV threshold is 4195 mV. It is enabled/disabled through OV_UV_EN register. */ | \
    BCC_SET_CTX_UV_TH(2509U)  /* CT UV threshold is 2509 mV. It is enabled/disabled through OV_UV_EN register. */ \
)

/* Initial value of TH_CT12 register. */
#define BCC_CONF1_TH_CT12_VALUE ( \
    BCC_SET_CTX_OV_TH(4195U)  /* CT OV threshold is 4195 mV. It is enabled/disabled through OV_UV_EN register. */ | \
    BCC_SET_CTX_UV_TH(2509U)  /* CT UV threshold is 2509 mV. It is enabled/disabled through OV_UV_EN register. */ \
)

/* Initial value of TH_CT11 register. */
#define BCC_CONF1_TH_CT11_VALUE ( \
    BCC_SET_CTX_OV_TH(4195U)  /* CT OV threshold is 4195 mV. It is enabled/disabled through OV_UV_EN register. */ | \
    BCC_SET_CTX_UV_TH(2509U)  /* CT UV threshold is 2509 mV. It is enabled/disabled through OV_UV_EN register. */ \
)

/* Initial value of TH_CT10 register. */
#define BCC_CONF1_TH_CT10_VALUE ( \
    BCC_SET_CTX_OV_TH(4195U)  /* CT OV threshold is 4195 mV. It is enabled/disabled through OV_UV_EN register. */ | \
    BCC_SET_CTX_UV_TH(2509U)  /* CT UV threshold is 2509 mV. It is enabled/disabled through OV_UV_EN register. */ \
)

/* Initial value of TH_CT9 register. */
#define BCC_CONF1_TH_CT9_VALUE ( \
    BCC_SET_CTX_OV_TH(4195U)  /* CT OV threshold is 4195 mV. It is enabled/disabled through OV_UV_EN register. */ | \
    BCC_SET_CTX_UV_TH(2509U)  /* CT UV threshold is 2509 mV. It is enabled/disabled through OV_UV_EN register. */ \
)

/* Initial value of TH_CT8 register. */
#define BCC_CONF1_TH_CT8_VALUE ( \
    BCC_SET_CTX_OV_TH(4195U)  /* CT OV threshold is 4195 mV. It is enabled/disabled through OV_UV_EN register. */ | \
    BCC_SET_CTX_UV_TH(2509U)  /* CT UV threshold is 2509 mV. It is enabled/disabled through OV_UV_EN register. */ \
)

/* Initial value of TH_CT7 register. */
#define BCC_CONF1_TH_CT7_VALUE ( \
    BCC_SET_CTX_OV_TH(4195U)  /* CT OV threshold is 4195 mV. It is enabled/disabled through OV_UV_EN register. */ | \
    BCC_SET_CTX_UV_TH(2509U)  /* CT UV threshold is 2509 mV. It is enabled/disabled through OV_UV_EN register. */ \
)

/* Initial value of TH_CT6 register. */
#define BCC_CONF1_TH_CT6_VALUE ( \
    BCC_SET_CTX_OV_TH(4195U)  /* CT OV threshold is 4195 mV. It is enabled/disabled through OV_UV_EN register. */ | \
    BCC_SET_CTX_UV_TH(2509U)  /* CT UV threshold is 2509 mV. It is enabled/disabled through OV_UV_EN register. */ \
)

/* Initial value of TH_CT5 register. */
#define BCC_CONF1_TH_CT5_VALUE ( \
    BCC_SET_CTX_OV_TH(4195U)  /* CT OV threshold is 4195 mV. It is enabled/disabled through OV_UV_EN register. */ | \
    BCC_SET_CTX_UV_TH(2509U)  /* CT UV threshold is 2509 mV. It is enabled/disabled through OV_UV_EN register. */ \
)

/* Initial value of TH_CT4 register. */
#define BCC_CONF1_TH_CT4_VALUE ( \
    BCC_SET_CTX_OV_TH(4195U)  /* CT OV threshold is 4195 mV. It is enabled/disabled through OV_UV_EN register. */ | \
    BCC_SET_CTX_UV_TH(2509U)  /* CT UV threshold is 2509 mV. It is enabled/disabled through OV_UV_EN register. */ \
)

/* Initial value of TH_CT3 register. */
#define BCC_CONF1_TH_CT3_VALUE ( \
    BCC_SET_CTX_OV_TH(4195U)  /* CT OV threshold is 4195 mV. It is enabled/disabled through OV_UV_EN register. */ | \
    BCC_SET_CTX_UV_TH(2509U)  /* CT UV threshold is 2509 mV. It is enabled/disabled through OV_UV_EN register. */ \
)

/* Initial value of TH_CT2 register. */
#define BCC_CONF1_TH_CT2_VALUE ( \
    BCC_SET_CTX_OV_TH(4195U)  /* CT OV threshold is 4195 mV. It is enabled/disabled through OV_UV_EN register. */ | \
    BCC_SET_CTX_UV_TH(2509U)  /* CT UV threshold is 2509 mV. It is enabled/disabled through OV_UV_EN register. */ \
)

/* Initial value of TH_CT1 register. */
#define BCC_CONF1_TH_CT1_VALUE ( \
    BCC_SET_CTX_OV_TH(4195U)  /* CT OV threshold is 4195 mV. It is enabled/disabled through OV_UV_EN register. */ | \
    BCC_SET_CTX_UV_TH(2509U)  /* CT UV threshold is 2509 mV. It is enabled/disabled through OV_UV_EN register. */ \
)

/* Initial value of TH_AN6_OT register. */
#define BCC_CONF1_TH_AN6_OT_VALUE ( \
    BCC_SET_ANX_OT_TH(1162U)  /* AN OT threshold is 1162 mV. It is enabled/disabled through FAULT_MASK1 register. */ \
)

/* Initial value of TH_AN5_OT register. */
#define BCC_CONF1_TH_AN5_OT_VALUE ( \
    BCC_SET_ANX_OT_TH(1162U)  /* AN OT threshold is 1162 mV. It is enabled/disabled through FAULT_MASK1 register. */ \
)

/* Initial value of TH_AN4_OT register. */
#define BCC_CONF1_TH_AN4_OT_VALUE ( \
    BCC_SET_ANX_OT_TH(1162U)  /* AN OT threshold is 1162 mV. It is enabled/disabled through FAULT_MASK1 register. */ \
)

/* Initial value of TH_AN3_OT register. */
#define BCC_CONF1_TH_AN3_OT_VALUE ( \
    BCC_SET_ANX_OT_TH(1162U)  /* AN OT threshold is 1162 mV. It is enabled/disabled through FAULT_MASK1 register. */ \
)

/* Initial value of TH_AN2_OT register. */
#define BCC_CONF1_TH_AN2_OT_VALUE ( \
    BCC_SET_ANX_OT_TH(1162U)  /* AN OT threshold is 1162 mV. It is enabled/disabled through FAULT_MASK1 register. */ \
)

/* Initial value of TH_AN1_OT register. */
#define BCC_CONF1_TH_AN1_OT_VALUE ( \
    BCC_SET_ANX_OT_TH(1162U)  /* AN OT threshold is 1162 mV. It is enabled/disabled through FAULT_MASK1 register. */ \
)

/* Initial value of TH_AN0_OT register. */
#define BCC_CONF1_TH_AN0_OT_VALUE ( \
    BCC_SET_ANX_OT_TH(1162U)  /* AN OT threshold is 1162 mV. It is enabled/disabled through FAULT_MASK1 register. */ \
)

/* Initial value of TH_AN6_UV register. */
#define BCC_CONF1_TH_AN6_UT_VALUE ( \
    BCC_SET_ANX_UT_TH(3822U)  /* AN UT threshold is 3822 mV. It is enabled/disabled through FAULT_MASK1 register. */ \
)

/* Initial value of TH_AN5_UV register. */
#define BCC_CONF1_TH_AN5_UT_VALUE ( \
    BCC_SET_ANX_UT_TH(3822U)  /* AN UT threshold is 3822 mV. It is enabled/disabled through FAULT_MASK1 register. */ \
)

/* Initial value of TH_AN4_UV register. */
#define BCC_CONF1_TH_AN4_UT_VALUE ( \
    BCC_SET_ANX_UT_TH(3822U)  /* AN UT threshold is 3822 mV. It is enabled/disabled through FAULT_MASK1 register. */ \
)

/* Initial value of TH_AN3_UV register. */
#define BCC_CONF1_TH_AN3_UT_VALUE ( \
    BCC_SET_ANX_UT_TH(3822U)  /* AN UT threshold is 3822 mV. It is enabled/disabled through FAULT_MASK1 register. */ \
)

/* Initial value of TH_AN2_UV register. */
#define BCC_CONF1_TH_AN2_UT_VALUE ( \
    BCC_SET_ANX_UT_TH(3822U)  /* AN UT threshold is 3822 mV. It is enabled/disabled through FAULT_MASK1 register. */ \
)

/* Initial value of TH_AN1_UV register. */
#define BCC_CONF1_TH_AN1_UT_VALUE ( \
    BCC_SET_ANX_UT_TH(3822U)  /* AN UT threshold is 3822 mV. It is enabled/disabled through FAULT_MASK1 register. */ \
)

/* Initial value of TH_AN0_UV register. */
#define BCC_CONF1_TH_AN0_UT_VALUE ( \
    BCC_SET_ANX_UT_TH(3822U)  /* AN UT threshold is 3822 mV. It is enabled/disabled through FAULT_MASK1 register. */ \
)

/* Initial value of TH_ISENSE_OC register. */
#define BCC_CONF1_TH_ISENSE_OC_VALUE ( \
    /* ISENSE OC threshold is 24576 mA (2458 uV using 100 uOhm resistor). It is enabled/disabled through FAULT_MASK1 and WAKEUP_MASK1 register. */ \
BCC_SET_TH_ISENSE_OC(2458U) \
)

/* Initial value of TH_COULOMB_CNT_MSB register. */
#define BCC_CONF1_TH_COULOMB_CNT_MSB_VALUE ( \
    BCC_SET_TH_COULOMB_CNT_MSB(0U) /* Higher 16 bits of over Coulomb threshold (2's complement representation). */ \
)

/* Initial value of TH_COULOMB_CNT_LSB register. */
#define BCC_CONF1_TH_COULOMB_CNT_LSB_VALUE ( \
    BCC_SET_TH_COULOMB_CNT_LSB(0U) /* Lower 16 bits of over Coulomb threshold (2's complement representation). */ \
)

/*!
 * @brief Calculates ISENSE value in [uV]. Resolution is
 * 0.6 uV/LSB. Result is int32_t type.
 * Note: Vind (Differential Input Voltage Range) is min. -150 mV
 * and max. 150 mV (see datasheet).
 *
 * @param iSense1 Content of register MEAS_ISENSE1.
 * @param iSense2 Content of register MEAS_ISENSE2.
 * @return ISENSE voltage in [mV].
 */
#define BCC_GET_ISENSE_VOLT(iRawValue) \
    ((BCC_GET_ISENSE_RAW_SIGN(iRawValue) * 6) / 10/1000)

/*!
 * @brief This macro calculates ISENSE value in [mA]. Resolution is
 * (600/R_SHUNT) mA/LSB (V2Res / Rshunt = 1000 * 0.6 uV / rShunt uOhm).
 *
 * @param rShunt Resistance of Shunt resistor in [uOhm].
 * @param iSense1 Content of register MEAS_ISENSE1.
 * @param iSense2 Content of register MEAS_ISENSE2.
 * @return ISENSE current in [mA].
 */
#define BCC_GET_ISENSE_AMP(rShunt, iSense1, iSense2) ( \
    (BCC_GET_ISENSE_RAW_SIGN(BCC_GET_ISENSE_RAW(iSense1, iSense2)) * 600) / \
    (int32_t)(rShunt)                                                       \
)

/*!
 * @brief This macro converts value of the MEAS_STACK register to [uV].
 * Resolution is 2.4414 mV/LSB. Result is in range 0 - 80 000 000 uV.
 *
 * @param reg Value of the MEAS_STACK register.
 * @return Converted value in [mV].
 */
#define BCC_GET_STACK_VOLT(reg) \
    ((uint32_t)BCC_GET_MEAS_RAW(reg) * 24414U / 10000U)

/*!
 * @brief Converts value of a register to [uV]. Resolution is 152.59 uV/LSB.
 * Result is in range 0 - 5 000 000 uV.
 * This macro is intended for the following registers: MEAS_CELLx, MEAS_ANx,
 * MEAS_IC_TEMP, MEAS_VBG_DIAG_ADC1A and MEAS_VBG_DIAG_ADC1B.
 *
 * @param reg Value of a measurement register.
 * @return Converted value in [mV].
 */
#define BCC_GET_VOLT(reg) \
    (((uint32_t)BCC_GET_MEAS_RAW(reg) * 15259U) / 100000U)

/*!
 * @brief Converts value of the MEAS_IC_TEMP register to degrees Celsius.
 * Resolution is 0.032 Kelvin/LSB. Result is in range 0 - 775 deg C.
 *
 * @param reg Value of the MEAS_IC_TEMP register.
 * @return Converted value in [deg C].
 */
#define BCC_GET_IC_TEMP(reg) \
    ((((uint32_t)BCC_GET_MEAS_RAW(reg) * 32) / 1000U) - 273U)
/*! @} */

/* Enum types definition. */
/*!
 * @addtogroup enum_group
 * @{
 */
/*! @brief Error codes. */
typedef enum
{
    BCC_STATUS_SUCCESS        = 0x00U,   /*!< No error. */
	BCC_STATUS_COM_TAG_ID     = 0xA1U,   /*!< Response Tag ID does not match with provided ID. */
	BCC_STATUS_COM_RC         = 0xA2U,   /*!< Response Rolling Counter (RC) value does not matchwith expected RC. */
	BCC_STATUS_COM_TIMEOUT    = 0xA3U,   /*!< Communication timeout. */
	BCC_STATUS_PARAM_RANGE    = 0xA4U,   /*!< Parameter out of range. */
    BCC_STATUS_CRC            = 0xA5U,   /*!< Wrong CRC. */
	BCC_STATUS_COM_TX       =   0xA6U,   /*!< SPI TX/RX failure. */
    BCC_STATUS_DIAG_FAIL = 0xA9U,        /*!< Enter diag mode failed It is not allowed
                                           to enter diagnostic mode. */
    BCC_STATUS_NULL_RESP      = 0xAAU   /*!< Response frame of BCC device is equal to zero (except
                                           CRC). This occurs only in SPI communication mode during
                                           the very first message. */
} bcc_status_t;

/*! @brief Bms Status
 *
 * In main loop, BCC in these state*/
typedef enum BCCMODE{
	BCC_INIT  		= 1,	                 /*!< bms is in init phase and assigning CID. */
	BCC_Measure    	= 2,					 /*!< bms is doing measurement and status checking. */
	BCC_Refresh    	= 3,                     /*!< bms is refreshing BCC register map */
	BCC_Diag		= 4,                     /*!< bms is in diagnostic phase and doing some SM*/
	BCC_Idle     	= 5,					 //!< bms is waiting for initialization*/
	BCC_Sleeping     	= 6,					 //!< bms is sleeping*/
}TYPE_BCCmode;


/*! @brief Cluster Identification Address.
 *
 * Note that SPI communication mode uses one cluster/device only.
 * The maximum number of clusters/devices for TPL mode is 63 in one daisy-chain.  */
typedef enum
{
    BCC_CID_UNASSIG           = 0U,   /*!< ID of uninitialized BCC device. */
    BCC_CID_DEV1              = 1U,   /*!< Cluster ID of device 1. In TPL mode, this is the first
                                           device in daisy chain (connected directly to MC33664). */
    BCC_CID_DEV2              = 2U,   /*!< Cluster ID of device 2. */
    BCC_CID_DEV3              = 3U,   /*!< Cluster ID of device 3. */
    BCC_CID_DEV4              = 4U,   /*!< Cluster ID of device 4. */
    BCC_CID_DEV5              = 5U,   /*!< Cluster ID of device 5. */
    BCC_CID_DEV6              = 6U,   /*!< Cluster ID of device 6. */
    BCC_CID_DEV7              = 7U,   /*!< Cluster ID of device 7. */
    BCC_CID_DEV8              = 8U,   /*!< Cluster ID of device 8. */
    BCC_CID_DEV9              = 9U,   /*!< Cluster ID of device 9. */
    BCC_CID_DEV10             = 10U,  /*!< Cluster ID of device 10. */
    BCC_CID_DEV11             = 11U,  /*!< Cluster ID of device 11. */
    BCC_CID_DEV12             = 12U,  /*!< Cluster ID of device 12. */
    BCC_CID_DEV13             = 13U,  /*!< Cluster ID of device 13. */
    BCC_CID_DEV14             = 14U,  /*!< Cluster ID of device 14. */
    BCC_CID_DEV15             = 15U   /*!< Cluster ID of device 15. */
} bcc_cid_t;

/*! @brief BCC communication mode.  */
typedef enum
{
    BCC_MODE_SPI              = 0U,   /*!< SPI communication mode. */
    BCC_MODE_TPL              = 1U    /*!< TPL communication mode. */
} bcc_mode_t;

/*! @brief BCC device.  */
typedef enum
{
    BCC_DEVICE_MC33771        = 0U,   /*!< MC33771C. */
} bcc_device_t;

/*! @brief Measurements provided by Battery Cell Controller.
 *
 * Function BCC_GetRawMeasurements returns 0x0000 at these positions.
 */
typedef enum
{
    BCC_MSR_CC_NB_SAMPLES     = 0U,   /*!< Number of samples in Coulomb counter (register CC_NB_SAMPLES). */
    BCC_MSR_COULOMB_CNT1      = 1U,   /*!< Coulomb counting accumulator (register COULOMB__CNT1). */
    BCC_MSR_COULOMB_CNT2      = 2U,   /*!< Coulomb counting accumulator (register COULOMB__CNT2). */
    BCC_MSR_ISENSE1           = 3U,   /*!< ISENSE measurement (register MEAS_ISENSE1). */
    BCC_MSR_ISENSE2           = 4U,   /*!< ISENSE measurement (register MEAS_ISENSE2). */
    BCC_MSR_STACK_VOLT        = 5U,   /*!< Stack voltage measurement (register MEAS_STACK). */
    BCC_MSR_CELL_VOLT14       = 6U,   /*!< Cell 14 voltage measurement (register MEAS_CELL14). */
    BCC_MSR_CELL_VOLT13       = 7U,   /*!< Cell 13 voltage measurement (register MEAS_CELL13). */
    BCC_MSR_CELL_VOLT12       = 8U,   /*!< Cell 12 voltage measurement (register MEAS_CELL12). */
    BCC_MSR_CELL_VOLT11       = 9U,   /*!< Cell 11 voltage measurement (register MEAS_CELL11). */
    BCC_MSR_CELL_VOLT10       = 10U,  /*!< Cell 10 voltage measurement (register MEAS_CELL10). */
    BCC_MSR_CELL_VOLT9        = 11U,  /*!< Cell 9 voltage measurement (register MEAS_CELL9). */
    BCC_MSR_CELL_VOLT8        = 12U,  /*!< Cell 8 voltage measurement (register MEAS_CELL8). */
    BCC_MSR_CELL_VOLT7        = 13U,  /*!< Cell 7 voltage measurement (register MEAS_CELL7). */
    BCC_MSR_CELL_VOLT6        = 14U,  /*!< Cell 6 voltage measurement (register MEAS_CELL6). */
    BCC_MSR_CELL_VOLT5        = 15U,  /*!< Cell 5 voltage measurement (register MEAS_CELL5). */
    BCC_MSR_CELL_VOLT4        = 16U,  /*!< Cell 4 voltage measurement (register MEAS_CELL4). */
    BCC_MSR_CELL_VOLT3        = 17U,  /*!< Cell 3 voltage measurement (register MEAS_CELL3). */
    BCC_MSR_CELL_VOLT2        = 18U,  /*!< Cell 2 voltage measurement (register MEAS_CELL2). */
    BCC_MSR_CELL_VOLT1        = 19U,  /*!< Cell 1 voltage measurement (register MEAS_CELL1). */
    BCC_MSR_AN6               = 20U,  /*!< Analog input 6 voltage measurement (register MEAS_AN6). */
    BCC_MSR_AN5               = 21U,  /*!< Analog input 5 voltage measurement (register MEAS_AN5). */
    BCC_MSR_AN4               = 22U,  /*!< Analog input 4 voltage measurement (register MEAS_AN4). */
    BCC_MSR_AN3               = 23U,  /*!< Analog input 3 voltage measurement (register MEAS_AN3). */
    BCC_MSR_AN2               = 24U,  /*!< Analog input 2 voltage measurement (register MEAS_AN2). */
    BCC_MSR_AN1               = 25U,  /*!< Analog input 1 voltage measurement (register MEAS_AN1). */
    BCC_MSR_AN0               = 26U,  /*!< Analog input 0 voltage measurement (register MEAS_AN0). */
    BCC_MSR_ICTEMP            = 27U,  /*!< IC temperature measurement (register MEAS_IC_TEMP). */
    BCC_MSR_VBGADC1A          = 28U,  /*!< ADCIA Band Gap Reference measurement
                                           (register MEAS_VBG_DIAG_ADC1A). */
    BCC_MSR_VBGADC1B          = 29U   /*!< ADCIB Band Gap Reference measurement
                                           (register MEAS_VBG_DIAG_ADC1B). */
} bcc_measurements_t;

/*! @brief Status provided by Battery Cell Controller. */
typedef enum
{
    BCC_FS_CELL_OV            = 0U,   /*!< CT overvoltage fault (register CELL_OV_FLT). */
    BCC_FS_CELL_UV            = 1U,   /*!< CT undervoltage fault (register CELL_UV_FLT). */
    BCC_FS_CB_OPEN            = 2U,   /*!< Open CB fault (register CB_OPEN_FLT). */
    BCC_FS_CB_SHORT           = 3U,   /*!< Short CB fault (register CB_SHORT_FLT). */
    BCC_FS_AN_OT_UT           = 4U,   /*!< AN undertemperature and overtemperature
                                           (register AN_OT_UT_FLT). */
    BCC_FS_GPIO_SHORT         = 5U,   /*!< GPIO short and analog inputs open load
                                           detection (register GPIO_SHORT_Anx_OPEN_STS). */
    BCC_FS_COMM               = 6U,   /*!< Number of communication errors detected
                                           (register COM_STATUS). */
    BCC_FS_FAULT1             = 7U,   /*!< Fault status (register FAULT1_STATUS). */
    BCC_FS_FAULT2             = 8U,   /*!< Fault status (register FAULT2_STATUS). */
    BCC_FS_FAULT3             = 9U   /*!< Fault status (register FAULT3_STATUS). */
} bcc_fault_status_t;

/*! @brief Selection between Cell terminal and Cell balancing diagnostic
    switches. */
typedef enum
{
    BCC_SWITCH_SEL_CT         = 0U,   /*!< Cell terminal switches. */
    BCC_SWITCH_SEL_CB         = 1U    /*!< Cell balancing switches. */
} bcc_diag_switch_sel_t;

/*! @brief Selection between opened and closed diagnostic switches. */
typedef enum
{
    BCC_SWITCH_POS_OPEN       = 0U,   /*!< Opened switches. */
    BCC_SWITCH_POS_CLOSED     = 1U    /*!< Closed switches. */
} bcc_diag_switch_pos_t;

/*! @brief Selection between UT or OT verification */
typedef enum
{
    BCC_UT_VERIFICATION     = 0U,   /*!<  */
	BCC_OT_VERIFICATION     = 1U    /*!< Closed switches. */
} bcc_diag_otut;

/*! @brief Selection of diagnostic type and source of ADC2 for Current
 *  measurement diagnostics. */
typedef enum
{
    BCC_DCM_AMP_INP_GND       = 0U,   /*!< Diagnostic of measurement chain offset,
                                           amplifier inputs are grounded. */
    BCC_DCM_VREF_GAIN4        = 1U,   /*!< Diagnostic of measurement chain with a gain 4,
                                           ADC is set to calibrated internal reference. */
    BCC_DCM_AN5AN6            = 2U    /*!< Diagnostic of external open and short or leaking
                                           devices, ADC is set to GPIO5 and GPIO6. */
} bcc_diag_current_meas_t;

/*! @brief Fault pin behavior. */
typedef enum
{
    BCC_BATT_T                = 0U,   /*!< Type T (1.5 V <= V_CELL <= 2.7 V). */
    BCC_BATT_F                = 1U,   /*!< Type F (2.5 V <= V_CELL <= 3.7 V). */
    BCC_BATT_N                = 2U    /*!< Type N (2.5 V <= V_CELL <= 4.3 V). */
} bcc_battery_type_t;

/*! @} */

/* Configure struct types definition. */
/*!
 * @addtogroup struct_group
 * @{
 */

/*!
 * @brief structure to store BCC ADC result and BCC status information
 */

typedef struct {
	uint16_t u16VoltCell[15][22];				/*!<1 stack voltage, 14 CT voltage and 7 AN voltage measured values in [mV]. */
	uint16_t u16RawCell[15][25];				/*!<1 stack voltage, 14 CT voltage , 7 AN voltage, 1 IC temperature
	                                                ADC1-A and ADC1-B voltage raw values from register*/
	uint16_t u16Temp[15];                       /*!<1 IC temperature measured value in [��]  */
	uint16_t u16VbgADC1[15][2];                 /*!<ADC1-A and ADC1-B voltage measured value in [mV]  */
	int32_t i32Current[15];                     /*!signed current shunt voltage measured value in [mV]  */
	int32_t i32RawCurrent[15];                 /*!unsigned current shunt voltage raw value from register*/
	uint16_t u16CCSamples[15];                  /*!Number of samples in coulomb counter */
	uint32_t s32CCCounter[15];                  /*!signed Coulomb counting accumulator*/
	uint16_t u16SiliconRev[15];                 /*!BCC IC Silicon revision */
	uint16_t u16FautStatus[15][11];             /*!BCC IC fault and status register data */
}TYPE_BatteryPack;

extern TYPE_BatteryPack BatPack;

/*!
 * @brief Result of ADC1-A and ADC1-B functional verification.
 */
typedef struct
{
    uint32_t adc1aAvg;                    /*!< Average of ADC1-A measured values in [mV]. */
    uint32_t adc1bAvg;                    /*!< Average of ADC1-B measured values in [mV]. */
    bool error;                           /*!< True if error detected, False otherwise. */
} bcc_adc1x_res_t;

/*!
 * @brief Result of VPWR comparison to sum of cell voltages.
 */
typedef struct
{
    uint32_t stackmeas;                    /*!< Measurements of stack voltages in [uV]. */
    uint32_t cellmeas[BCC_MAX_CELLS];      /*!< Measurements of cell voltages in [mV]. */
    bool error;                           /*!< True if error detected, False otherwise. */
} bcc_vpwr_res_t;

/*!
 * @brief Result of overvoltage and undervoltage functional verification.
 */
typedef struct
{
    uint16_t ovOdd;                       /*!< Content of CELL_OV_FLT register, OV fault is expected on odd cells. */
    uint16_t uvEven;                      /*!< Content of CELL_UV_FLT register, UV fault is expected on even cells. */
    uint16_t ovEven;                      /*!< Content of CELL_OV_FLT register, OV fault is expected on even cells. */
    uint16_t uvOdd;                       /*!< Content of CELL_UV_FLT register, UV fault is expected on odd cells. */
} bcc_diag_ov_uv_res_t;

/*!
 * @brief Result of the CTx open detect and open detect functional verification.
 */
typedef struct
{
    uint32_t measPreClosure[BCC_MAX_CELLS];  /*!< Measurements of cell voltages in [mV]. This is the preclosure
                                                  switch status containing MEAS_CELLx registers.
                                                  MC33771: [0] CT14, .., [13] CT1.
                                                  */
    uint32_t measOddClosed[BCC_MAX_CELLS];   /*!< Measurements of cell voltages in [mV]. This is the postclosure
                                                  switch status containing MEAS_CELLx registers when odd CT open
                                                  terminal switches are closed.
                                                  MC33771: [0] CT14, .., [13] CT1.
                                                 */
    uint32_t measEvenClosed[BCC_MAX_CELLS];  /*!< Measurements of cell voltages in [mV]. This is the postclosure
                                                  switch status containing MEAS_CELLx registers, when even CT open
                                                  terminal switches are closed.
                                                  MC33771: [0] CT14, .., [13] CT1.
                                                 */
    uint16_t ctxOpen;                        /*!< Bit map representing open terminal status.
                                                  MC33771: 0th bit: CT1, ..., 13th bit: CT14.
                                                  Bit value 0: normal condition. Bit value 1: open condition. */
} bcc_diag_ctx_open_res_t;

/*!
 * @brief Result of the cell voltage channel functional verification.
 */
typedef struct {
    uint32_t measCellVolt[BCC_MAX_CELLS];  /*!< Measurements of diagnostics cell voltages in [uV].
                                                MC33771: [0] CT14, .., [13] CT1.
                                                 */
    int32_t vErrX[BCC_MAX_CELLS - 2];      /*!< Computed errors V_err_x in [mV].
                                                MC33771: [0] V_err_3, .., [11] V_err_14.
                                                */
    uint16_t result;                       /*!< Bit map representing error detection in V_err_x.
                                                MC33771: 0th bit: V_err_3, .., 11th bit: V_err_14.
                                                Bit value 0: OK. Bit value 1: Error detected. */
} bcc_diag_cell_volt_res_t;

/*!
 * @brief Result of the cell terminal leakage diagnostics.
 */
typedef struct
{
    uint16_t conResistance;                 /*!< Bit map representing connectors having abnormally high contact resistance.
                                                 MC33771: 0th bit: connector 1, .., 13th bit: connector 14.
                                                 Bit value 0: Normal resistance. Bit value 1: High resistance. */
    uint32_t vleak_avx[BCC_MAX_CELLS + 1U]; /*!< Average of Vleak_x in [mV].
                                                 MC33771: [0] CT_REF (Vleak_av1), [1] CT1 (Vleak1), .., [14] CT14 (Vleak_av15).
                                                  */
    uint16_t leakStatus;                    /*!< Bit map representing leakage status on CTx and CBx.
                                                 MC33771: 0th bit: CT_REF, 1st bit: CT_1, .., 14th bit: CT14.
                                                 Bit value 0: Cell is not leaky. Bit value 1: Cell is leaky. */
} bcc_diag_cell_leak_res_t;


/*!
 * @brief Result of overtemperature and undertemperature functional verification.
 */
typedef struct
{
    uint16_t untStat;                       /*!< Under-temperature status for GPIOs */
    uint16_t ovtStat;                      /*!< Over-temperature status for GPIOs*/
} bcc_diag_ot_ut_res_t;

/*!
 * @brief Result of the cell balance fault diagnostics.
 */
typedef struct
{
    uint16_t cbxOpenStatusEven;    /*!< OL fault expected on even cells. It contains CB_OPEN_FLT
                                        register when even CB open detection switches are closed. */
    uint16_t cbxOpenStatusOdd;     /*!< OL fault expected on odd cells. It contains CB_OPEN_FLT
                                        register when odd CB open detection switches are closed. */
} bcc_diag_cbx_open_res_t;


/*!
* @brief NTC Configuration.
*
* The device has seven GPIOs which enable temperature measurement.
* NTC thermistor and fixed resistor are external components and must be set
* by the user. These values are used to calculate temperature. Beta parameter
* equation is used to calculate temperature. GPIO port of BCC device must be
* configured as Analog Input to measure temperature.
* This configuration is common for all GPIO ports and all devices (in case of
* daisy chain).
*/
typedef struct
{
    uint32_t beta;         /*!< Beta parameter of NTC thermistor in [K].
                                Admissible range is from 1 to 1000000. */
    uint32_t rntc;         /*!< R_NTC - NTC fixed resistance in [Ohm].
                                Admissible range is from 1 to 1000000. */
    uint32_t refRes;       /*!< NTC Reference Resistance in [Ohm].
                                Admissible range is from 1 to 1000000. */
    uint8_t refTemp;       /*!< NTC Reference Temperature in degrees [Celsius].
                                Admissible range is from 0 to 200. */
} bcc_ntc_config_t;

/*!
* @brief CT Filters Components.
*
* Values of external components required for OV & UV functional verification
* and CTx open detection functional verification.
*/
typedef struct
{
    uint32_t rLpf1;        /*!< R_LPF-1 low-pass filter resistor in [Ohm].
                                Admissible range is from 1 to 5000. */
    uint32_t rLpf2;        /*!< R_LPF-2 low-pass filter resistor in [Ohm]. To withstand with hot
                                plug, the constraint R_LPF1 + R_LPF2 = 5 kOhm should be met.
                                Admissible range is from 1 to 5000. */
    uint32_t cLpf;         /*!< C_LPF capacitance in [nF]. Admissible range is from 1 to 100000. */
    uint32_t cIn;          /*!< C_IN capacitance in [nF]. Admissible range is from 1 to 100000. */
} bcc_ct_filter_t;

/*!
* @brief ISENSE Filters Components.
*
* Values of external components required for current measurements and related
* diagnostics.
*/
typedef struct
{
    uint16_t rLpfi;        /*!< R_LPFI resistor (between C_HFI and C_LPFI) in [Ohm].
                                Admissible range is from 1 to 1000. */
    uint32_t cD;           /*!< C_D capacitor (between ISENSE+ and ISENSE-) in [nF].
                                Admissible range is from 1 to 100000. */
    uint16_t cLpfi;        /*!< C_LPFI capacitor used to cut off common mode disturbances
                                on ISENSE+/- [nF]. Admissible range is from 0 to 1000. */
    uint32_t rShunt;       /*!< Shunt resistor for ISENSE in [uOhm].
                                Admissible range is from 1 to 1000000. */
    uint16_t iMax;         /*!< Maximum shunt resistor current in [mA]. Minimal allowed
                                value is 1 mA. Maximal value depends on "rShunt" -
                                iMax should be lower than (4294967295 / rShunt). */
} bcc_isense_filter_t;

/*!
* @brief Configuration of BCC external components.
*/
typedef struct
{
    bcc_ntc_config_t *ntcConfig;     /*!< NTC configuration. Cannot be NULL if you use the
                                          GetNtcCelsius function. */
    bcc_ct_filter_t *ctFilterComp;   /*!< CT Filter Components. Cannot be NULL if you use
                                          DiagOvUv or DiagCTxOpen function. */
    bcc_isense_filter_t isenseComp;  /*!< ISENSE Filter Components. */
} bcc_comp_config_t;

/*!
 * @brief Driver internal data.
 *
 * Note that it is initialized in BCC_Init function by the driver
 * and the user mustn't change it at any time.
 */
typedef struct
{
    uint16_t cellMap[BCC_DEVICE_CNT_MAX]; /*!< Bit map of used cells of each BCC device. */
    uint8_t rcTbl[BCC_DEVICE_CNT_MAX];    /*!< Rolling counter index (0-4). */
    uint8_t tagId;    /*!< TAG IDs of Battery Cell Controllers. */
    uint8_t rxBuf[BCC_RX_BUF_SIZE_TPL];   /*!< Buffer for receiving data in TPL mode. */
    uint16_t tauDiagn;                     /*!< Diagnostic time constant tau_diag in [us] for SM1 UV/OV verify. */
    uint16_t tauDiag;                     /*!< Diagnostic time constant tau_diag in [us] for SM2 CT open detection. */
} bcc_drv_data_t;

/*!
 * @brief Driver configuration.
 *
 * This structure contains all information needed for proper functionality of
 * the driver, such as used communication mode, BCC device(s) configuration or
 * internal driver data.
 */
typedef struct {
    uint8_t drvInstance;                     /*!< BCC driver instance. Passed to the external functions
                                                  defined by the user. */
    bcc_mode_t commMode;                     /*!< BCC communication mode. */
    uint8_t devicesCnt;                      /*!< Number of BCC devices. SPI mode allows one device only,
                                                  TPL mode allows up to 15 devices. */
    bcc_device_t device[BCC_DEVICE_CNT_MAX]; /*!< BCC device type of
                                                  [0] BCC with CID 1, [1] BCC with CID 2, etc. */
    uint16_t cellCnt[BCC_DEVICE_CNT_MAX];    /*!< Number of connected cells to each BCC.
                                                  [0] BCC with CID 1, [1] BCC with CID 2, etc. */

    bcc_comp_config_t compConfig;            /*!< Configuration of external components. */
    bcc_drv_data_t drvData;                  /*!< Internal driver data. */
} bcc_drv_config_t;

/*!
 * @brief structure to store Read command for Asynchronous TPL transfer
 */

typedef struct {

	bcc_drv_config_t *drvConfig;   /*!< Driver configuration inherit global variable defined in main*/
	bcc_cid_t cid;                /*!< current cluster ID need to check*/
	uint8_t   regAddr;            /*!< current register address need to check*/
	uint8_t   regCnt;             /*!< Current number of registers to read*/

	uint16_t  *regVal;            /*!< Pointer to memory where registers content is stored.*/

	uint8_t   auto_enable;          /*!< if set 1, bcc register is refresh automatically in call back function*/

	uint8_t   NoResponse_Cnt;     /*!< Used to record the number of no-response, if it exceeds threshold, abort transfer*/
	uint8_t   Async_Transfer_enbale;/*!< if using async mode TPL transfer, set it 1*/
	uint8_t   Async_Transfer_running;/*!< Async_transfer is running*/
	bcc_status_t error[15];           /*!< return Error codes*/

}bcc_rdcmd_pack;

extern bcc_rdcmd_pack RDCMD_Pack;
/*! @} */

#define BIT(x)             				(1<<(x))

/*******************************************************************************
 * API
 ******************************************************************************/

/*!
 * @addtogroup function_group
 * @{
 */

/*!
 * @brief This function initializes the Battery Cell Controller device(s),
 * configures its registers, assigns CID and initializes internal driver data.
 *
 * @param drvConfig Pointer to driver instance configuration.
 * @param devConf Initialization values of BCC device registers specified
 *                by BCC_INIT_CONF_REG_ADDR. If NULL, registers are not
 *                initialized.
 *                devConf[0][x] belongs to device with CID 1,
 *                devConf[1][x] belongs to device with CID 2, etc.
 *
 * @return bcc_status_t Error code.
 */
bcc_status_t BCC_Init(bcc_drv_config_t* const drvConfig,
    const uint16_t devConf[][BCC_INIT_CONF_REG_CNT]);

/*!
 * @brief This function writes a value to addressed register of selected Battery
 * Cell Controller device.
 *
 * @param drvConfig Pointer to driver instance configuration.
 * @param cid Cluster Identification Address.
 * @param regAddr Register address. See BCC header file with register map for
 *                possible values.
 * @param regVal New value of selected register.
 *
 * @return bcc_status_t Error code.
 */
bcc_status_t BCC_WriteRegister(bcc_drv_config_t* const drvConfig, bcc_cid_t cid, uint8_t regAddr, uint16_t regVal);

/*!
 * @brief This function writes a value to addressed register of all configured
 * BCC devices. Intended for TPL mode only!
 *
 * @param drvConfig Pointer to driver instance configuration.
 * @param regAddr Register address. See BCC header file with register map for
 *                possible values.
 * @param regVal New value of selected register.
 *
 * @return bcc_status_t Error code.
 */
bcc_status_t BCC_WriteRegisterGlobal(bcc_drv_config_t* const drvConfig,
    uint8_t regAddr, uint16_t regVal);

/*!
 * @brief This function reads a value from addressed register (or desired
 * number of registers) of selected Battery Cell Controller device.
 *
 * In case of simultaneous read of more registers, address is incremented
 * in ascending manner.
 *
 * @param drvConfig Pointer to driver instance configuration.
 * @param cid Cluster Identification Address.
 * @param regAddr Register address. See BCC header file with register map for
 *                possible values.
 * @param regCnt Number of registers to read.
 * @param regVal Pointer to memory where content of selected 16 bit registers
 *               is stored.
 *
 * @return bcc_status_t Error code.
 */
bcc_status_t BCC_ReadRegisters(bcc_drv_config_t* const drvConfig, bcc_cid_t cid,
    uint8_t regAddr, uint8_t regCnt, uint16_t* regVal);


/*!
 * @brief This function sends read command to get a value from addressed register
 * (or desired number of registers) of selected Battery Cell Controller device.
 * And return result after command sent immediately.
 *
 * In case of simultaneous read of more registers, address is incremented
 * in ascending manner.
 *
 * @param drvConfig Pointer to driver instance configuration.
 * @param cid Cluster Identification Address.
 * @param regAddr Register address. See BCC header file with register map for
 *                possible values.
 * @param regCnt Number of registers to read.
 * @param regVal Pointer to memory where content of selected 16 bit registers
 *               is stored.
 *
 * @return bcc_status_t Error code.
 */
bcc_status_t BCC_ReadRegisters_Async(bcc_drv_config_t* const drvConfig, bcc_cid_t cid,
	    uint8_t regAddr, uint8_t regCnt, uint16_t regVal[][112]);


bcc_status_t BCC_ReadRegisters_AsyncStatus(void);

/*!
 * @brief This function updates content of a selected register. It affects bits
 * specified by a bit mask only.
 *
 * This function is required by the diagnostic functions, PauseCBDrivers
 * function or heart-beat function in daisy chain configuration.
 *
 * @param drvConfig Pointer to driver instance configuration.
 * @param cid Cluster Identification Address.
 * @param regAddr Register address. See BCC header file with register map for
 *                possible values.
 * @param regMask Bit mask. Bits set to 1 will be updated.
 * @param regVal  New value of register bits defined by bit mask.
 *
 * @return bcc_status_t Error code.
 */
bcc_status_t BCC_UpdateRegister(bcc_drv_config_t* const drvConfig,
    bcc_cid_t cid, uint8_t regAddr, uint16_t regMask, uint16_t regVal);

/*!
 * @brief This function sets sleep mode to all Battery Cell Controller devices.
 *
 * In case of TPL mode MC33664TL goes to sleep mode automatically.
 *
 * @param drvConfig Pointer to driver instance configuration.
 *
 * @return bcc_status_t Error code.
 */
bcc_status_t BCC_Sleep(bcc_drv_config_t* const drvConfig);

/*!
 * @brief This function sets normal mode to all Battery Cell Controller devices.
 *
 * In case of TPL mode, MC33664 goes to normal mode automatically.
 *
 * @param drvConfig Pointer to driver instance configuration.
 *
 * @return bcc_status_t Error code.
 */
bcc_status_t BCC_WakeUp(bcc_drv_config_t *drvConfig);

/*!
 * @brief This function starts ADC conversion. It sets Start of Conversion bit
 * and new value of TAG ID in ADC_CFG register.
 *
 * TAG ID is incremented for each conversion. You can use method IsConverting
 * to check conversion status.
 *
 * @param drvConfig Pointer to driver instance configuration.
 * @param cid Cluster Identification Address.
 *
 * @return bcc_status_t Error code.
 */
bcc_status_t BCC_StartConversion(bcc_drv_config_t* const drvConfig);

/*!
 * @brief This function checks status of conversion defined by End of Conversion
 * bit in ADC_CFG register.
 *
 * @param drvConfig Pointer to driver instance configuration.
 * @param cid Cluster Identification Address.
 * @param completed Pointer to check result. True if a conversion is complete.
 *
 * @return bcc_status_t Error code.
 */
bcc_status_t BCC_IsConverting(bcc_drv_config_t* const drvConfig, bcc_cid_t cid,
    bool *completed);

/*!
 * @brief This function reads the measurement registers and returns raw values.
 * Macros defined in BCC header file can be used to perform correct unit
 * conversion.
 *
 * @param drvConfig Pointer to driver instance configuration.
 * @param cid Cluster Identification Address.
 *
 * @return bcc_status_t Error code.
 */
bcc_status_t BCC_GetRawMeasurements(bcc_drv_config_t* const drvConfig, bcc_cid_t cid);

void BCC_DecodeRawMeasurements(bcc_cid_t cid);
/*!
 * @brief This function reads the status registers and returns raw values.
 * You can use constants defined in bcc_mc3377x.h file.
 *
 * @param drvConfig Pointer to driver instance configuration.
 * @param cid Cluster Identification Address.
 * @param status Array containing all fault status information provided by BCC.
 *               Indexes into the array are defined in bcc_fault_status_t
 *               enumeration placed in BCC header file. Required size of the
 *               array is 11. You can use macro BCC_STAT_CNT defined in BCC
 *               header file, which contains appropriate value.
 *
 * @return bcc_status_t Error code.
 */
bcc_status_t BCC_GetFaultStatus(bcc_drv_config_t* const drvConfig,
    bcc_cid_t cid, uint16_t status[][11]);

/*!
 * @brief This function clears selected fault status register.
 *
 * @param drvConfig Pointer to driver instance configuration.
 * @param cid Cluster Identification Address.
 * @param statSel Selection of a fault status register to be cleared. See
 *                definition of this enumeration in BCC header file.
 *                COM_STATUS register is read only and cannot be cleared.
 *
 * @return bcc_status_t Error code.
 */
bcc_status_t BCC_ClearFaultStatus(bcc_drv_config_t* const drvConfig, bcc_cid_t cid);

/*!
 * @brief This function resets BCC device using software reset. It enters reset
 * via SPI or TPL interface.
 *
 * @param drvConfig Pointer to driver instance configuration.
 * @param cid Cluster Identification Address.
 *
 * @return bcc_status_t Error code.
 */
bcc_status_t BCC_SoftwareReset(bcc_drv_config_t* const drvConfig);

/*!
 * @brief This function resets BCC device using GPIO pin.
 *
 * @param drvConfig Pointer to driver instance configuration.
 */
void BCC_HardwareReset(bcc_drv_config_t* const drvConfig);

/*!
 * @brief This function sets output value of one BCC GPIO pin. This function
 * should be used only when at least one GPIO is in output mode. Resets BCC
 * device using GPIO pin.
 *
 * @param drvConfig Pointer to driver instance configuration.
 * @param cid Cluster Identification Address.
 * @param gpioSel Index of GPIO output to be set. Index starts at 0 (GPIO 0).
 * @param val Output value. Possible values are FALSE (logical 0, low level)
 *            and TRUE (logical 1, high level).
 *
 * @return bcc_status_t Error code.
 */
bcc_status_t BCC_SetGPIOOutput(bcc_drv_config_t* const drvConfig, uint8_t cid,
    uint8_t gpioSel, bool val);

/*!
 * @brief This function sets state of individual cell balancing driver.
 *
 * @param drvConfig Pointer to driver instance configuration.
 * @param cid Cluster Identification Address.
 * @param cellIndex Index of the cell. Note the cells are indexed from 1.
 * @param enable Drivers state. False (all drivers are disabled) or true
 *               (drivers are enabled).
 * @param timer  timer of cell balance driver keep on (0.5min-511min),
 *               minimum value 0-0.5min��maximum value 511 -511min
 *
 * @return bcc_status_t Error code.
 */
bcc_status_t BCC_SetCBIndividually(bcc_drv_config_t* const drvConfig,
    uint8_t cid, uint8_t cellIndex, bool enable, uint16_t timer);

/* @brief This function checks status of each cell balance by reading
 * CB_DRV_STS register.
*
* @param drvConfig Pointer to driver instance configuration.
* @param cid Cluster Identification Address.
* @param status Pointer to each CB status. Bit 0 is cell1 balance status,
*               Bit13 is cell14 balance status
* @return bcc_status_t Error code.
*/
bcc_status_t BCC_GetCBStatus(bcc_drv_config_t* const drvConfig, bcc_cid_t cid,
		uint16_t *status);

/*!
 * @brief This function calculates temperature from raw value of MEAS_ANx
 * register. It uses precalculated values stored in g_ntcTable table.
 * You can use function GetRawMeasurements to get values of measurement
 * registers.
 *
 * @param drvConfig Pointer to driver instance configuration.
 * @param regVal Value of MEAS_ANx register.
 * @param temp Temperature value in deg. of Celsius * 10.
 *
 * @return bcc_status_t Error code.
 */
bcc_status_t BCC_GetNtcCelsius(bcc_drv_config_t* const drvConfig,
    uint16_t regVal, int16_t* temp);

/*!
 * @brief This function can be used to manual pause cell balancing before on
 * demand conversion. As a result more precise measurement can be done. Note
 * that it is user obligation to re-enable cell balancing after measurement
 * ends.
 *
 * @param drvConfig Pointer to driver instance configuration.
 * @param cid Cluster Identification Address.
 * @param pause True (pause) / false (unpause).
 *
 * @return bcc_status_t Error code.
 */
bcc_status_t BCC_PauseCBDrivers(bcc_drv_config_t* const drvConfig,
    bcc_cid_t cid, bool pause);

/*!
 * @brief This function uses verify NO.cid device communication
 * by reading its INIT register .
 *
 * @param drvConfig Pointer to driver instance configuration.
 * @param cid Cluster Identification Address.
 *
 * @return bcc_status_t Error code.
 */
bcc_status_t BCC_CheckCid(bcc_drv_config_t* const drvConfig, bcc_cid_t cid);

/*!
 * @brief This function reads BCC configurable register data
 * by reading their register .
 *
 * @param drvConfig Pointer to driver instance configuration.
 * @param cid Cluster Identification Address.
 * @param StatusPtr Pointer to store register content.
 * @param AddrMap Pointer to store register address.
 *
 * @return bcc_status_t Error code.
 */
bcc_status_t BCC_GetCtrlRegMap(bcc_drv_config_t* const drvConfig, bcc_cid_t cid, uint16_t StatusPtr[][31], uint8_t *AddrMap);

/*!
 * @brief This function reads BCC status register data
 * by reading their register .
 *
 * @param drvConfig Pointer to driver instance configuration.
 * @param cid Cluster Identification Address.
 * @param StatusPtr Pointer to store register content.
 * @param AddrMap Pointer to store register address.
 * @return bcc_status_t Error code.
 */
/*@brief This function uses Get BCC status related register data*/
bcc_status_t BCC_GetSTSRegMap(bcc_drv_config_t* const drvConfig, bcc_cid_t cid, uint16_t StatusPtr[][15], uint8_t *AddrMap);

/*!
 * @brief This function is a example function to
 * modify calibrate data in fuse mirror for I_PGA
 *
 * @param drvConfig Pointer to driver instance configuration.
 * @param cid Cluster Identification Address.
 * @param cali_data variation used for I_PGA.
 *
 * @return bcc_status_t Error code.
 */
bcc_status_t BCC_Configfuse_I(bcc_drv_config_t* const drvConfig, bcc_cid_t cid, uint8_t cali_data);


/*!
 * @brief This function implements the ADC1-A and ADC1-B functional verification.
 * Six on-demand conversions are performed and measured values from
 * MEAS_VBG_DIAG_ADC1A and MEAS_VBG_DIAG_ADC1B registers are compared with the
 * voltage reference.
 *
 * @param drvConfig Pointer to driver instance configuration.
 * @param cid Cluster Identification Address.
 * @param result False (functional verification OK), True (error).
 *
 * @return bcc_status_t Error code.
 */
bcc_status_t BCC_DiagADC1(bcc_drv_config_t* const drvConfig,
    bcc_adc1x_res_t *result);


/*!
 * @brief This function implements the Vpower line diagnostic.
 * Compare the sum of all cell voltages and the stack voltage
 * which is measured from the IC.
 *
 * @param drvConfig Pointer to driver instance configuration.
 * @param cid Cluster Identification Address.
 * @param result False (functional verification OK), True (error).
 *
 * @return bcc_status_t Error code.
 */
bcc_status_t BCC_DiagVPWR(bcc_drv_config_t* const drvConfig,
	bcc_vpwr_res_t *result);


/*!
 * @brief This function implements OV/UV functional verification through digital
 * comparators against tunable thresholds. This can be done by forcing an OV/UV
 * condition on cell terminal pins with use of diagnostic switches.
 *
 * @param drvConfig Pointer to driver instance configuration.
 * @param cid Cluster Identification Address.
 * @param battType Battery type.
 * @param results Pointer to structure with results of diagnostic. See
 *                definition of this structure in BCC header file.
 *
 * @return bcc_status_t Error code.
 */
bcc_status_t BCC_DiagOvUv(bcc_drv_config_t* const drvConfig,
    bcc_battery_type_t battType, bcc_diag_ov_uv_res_t* results);

/*!
 * @brief This function implements CTx open detection and functional
 * verification. Open detection is achieved by taking an ADC reading of the cell
 * terminal voltages before and after closing the open detection switches.
 * Comparison of measured values with expected results gives status of cell
 * terminals. It checks normal condition and open condition (not the CTx
 * terminated and SWx Failed Short, CTx terminated and SWx Failed Open).
 *
 * @param drvConfig Pointer to driver instance configuration.
 * @param cid Cluster Identification Address.
 * @param battType Battery type.
 * @param results Pointer to structure with results of diagnostic. See
 *                definition of this structure in BCC header file.
 *
 * @return bcc_status_t Error code.
 */
bcc_status_t BCC_DiagCTxOpen(bcc_drv_config_t* const drvConfig,
	    bcc_battery_type_t battType, bcc_diag_ctx_open_res_t* results);

/*!
 * @brief This function implements Cell Voltage Channel functional verification.
 * Purpose of this verification is to check that gain variations introduced by
 * multiplexers (used to route CTx pins to ADC1-A,B) are small compared to the
 * unity. The diagnostic disconnects the cell terminal input circuitry and
 * places a precision zener reference on the input to each differential
 * amplifier attenuator to verify the integrity of the level shifting
 * differential amplifier, attenuator and multiplexer chain. Unused cell
 * voltage channels are skipped.
 *
 * See the datasheet for assumption of minimum cell voltage for this
 * verification.
 *
 * @param drvConfig Pointer to driver instance configuration.
 * @param cid Cluster Identification Address.
 * @param results Pointer to structure with results of diagnostic. See
 *                definition of this structure in BCC header file.
 *
 * @return bcc_status_t Error code.
 */
bcc_status_t BCC_DiagCellVolt(bcc_drv_config_t* const drvConfig, bcc_diag_cell_volt_res_t* results);

/*!
 * @brief This function implements Cell Terminal Leakage diagnostic.
 * The present safety mechanism is made up of two procedures. One of them, is
 * thought to detect a connector having an abnormally high contact resistance
 * The other is to detect cell terminals and cell balancing terminals
 * leakage current. Leakage detection is achieved by taking an ADC
 * reading of the cell terminals referenced to cell balance terminals. Inverted
 * and non-inverted measurement is used to detect possible current sourcing or
 * sinking.
 *
 * @param drvConfig Pointer to driver instance configuration.
 * @param cid Cluster Identification Address.
 * @param results Pointer to structure with results of diagnostic. See
 *                definition of this structure in BCC header file.
 *
 * @return bcc_status_t Error code.
 */
bcc_status_t BCC_DiagCellLeak(bcc_drv_config_t* const drvConfig, bcc_diag_cell_leak_res_t* results);

/*!
 * @brief This function implements current measurement diagnostics. It verifies
 * integrity of current measurement chain. It consists of three different
 * diagnostic types, namely: Amplifier inputs grounded (measurement chain
 * offset), VREF_DIAG reference (measurement chain with known reference and
 * a gain of 4) and GPIO5, GPIO6 (external open and short or leaking devices).
 *
 * Note that this diagnostic resets the coulomb counter. Read coulomb counter
 * COULOMB_CNT to retain count information before calling this function.
 *
 * @param drvConfig Pointer to driver instance configuration.
 * @param cid Cluster Identification Address.
 * @param sel Selection between different diagnostic types related to
 *            current measurement. See definition of this enumeration
 *            in bcc.h header file.
 * @param current Measured ISENSE voltage in [uV].
 * @param fault True (faulted condition) / false (un-faulted condition).
 *
 * @return bcc_status_t Error code.
 */
bcc_status_t BCC_DiagCurrentMeas(bcc_drv_config_t* const drvConfig,
    bcc_cid_t cid, bcc_diag_current_meas_t sel, int32_t* current, bool* fault);

/*!
 * @brief This function verifies whether the shunt resistor is properly
 * connected to the current channel low-pass filter.
 *
 * Note that this diagnostic resets the coulomb counter. Read coulomb counter
 * COULOMB_CNT to retain count information before calling this function.
 *
 * @param drvConfig Pointer to driver instance configuration.
 * @param cid Cluster Identification Address.
 * @param shuntConn True (shunt resistor is connected) / false (not connected).
 *
 * @return bcc_status_t Error code.
 */
bcc_status_t BCC_DiagShuntConn(bcc_drv_config_t* const drvConfig, bcc_cid_t cid,
    bool *shuntConn);

/*!
 * @brief This function implements GPIOx OT/UT functional verification. All
 * GPIOs are forced to analog input RM mode. Driving pin to low/high simulates
 * OT/UT condition. Note that programmed OT/UT thresholds are used to verify
 * functionality.
 *
 * @param drvConfig Pointer to driver instance configuration.
 * @param cid Cluster Identification Address.
 * @param results Pointer to structure with results of diagnostic. See
 *                definition of this structure in BCC header file.
 * @return bcc_status_t Error code.
 */
bcc_status_t BCC_DiagGPIOxOtUt(bcc_drv_config_t* const drvConfig, bcc_diag_ot_ut_res_t *results);

/*!
 * @brief This function implements GPIOx open terminal diagnostics. To detect
 * open terminals on the GPIO pins, a weak internal pull-down is commanded ON
 * and OFF. Voltages below the VOL(TH) threshold are considered open terminals.
 *
 * @param drvConfig Pointer to driver instance configuration.
 * @param cid Cluster Identification Address.
 * @param openStatus Open terminal status for each GPIO.
 *
 * @return bcc_status_t Error code.
 */
bcc_status_t BCC_DiagGPIOxOpen(bcc_drv_config_t* const drvConfig, uint16_t* openStatus);

/*!
 * @brief This function implements Cell balance open load detection. To detect
 * open load on the cell balance terminals, Rpd_cb resistor is applied between
 * the CBx outputs and their common terminal. Voltages below the Vout(FLT_TH)
 * activate the CB_OPEN_FLT register bits. Note that results for short detection
 * are not part of this diagnostic. It is diagnosed continuously with the cell
 * balance FET active.
 *
 * @param drvConfig Pointer to driver instance configuration.
 * @param cid Cluster Identification Address.
 * @param results Pointer to structure with results of diagnostic. See
 *                definition of this structure in BCC header file.
 *
 * @return bcc_status_t Error code.
 */
bcc_status_t BCC_DiagCBxOpen(bcc_drv_config_t* const drvConfig, bcc_diag_cbx_open_res_t* results);

/*! @} */

#endif /* __BCC_H__ */
/*******************************************************************************
 * EOF;
 ******************************************************************************/
