/*
 * demo_config.h
 *
 *  Created on: 2018��11��21��
 *      Author: nxa13016
 */

#ifndef DEMO_CONFIG_H_
#define DEMO_CONFIG_H_

#include "cpu.h"
#include "mc33771c/mc33771c.h"
#include "canCom1.h"

/*******************************************************************************
 * Global variables
 ******************************************************************************/

/* BCC driver configuration. */
extern bcc_drv_config_t* bccDrvConfig;

/*******************************************************************************
 * Definitions (depends on utilized device/board/battery)
 ******************************************************************************/

/* Used BCC device */
#define MC33771     /* Define MC33771 if FRDM33771BSPIEVB or FRDM33771BTPLEVB is used. */
//#define MC33772   /* Define MC33771 if FRDM33772BSPIEVB or FRDM33772BTPLEVB is used. */

/* Used BCC board/communication. */
#define TPL         /* Define TPL if S32K144EVB-Q100 + X-DEVKIT-TRANSLT + FRDM33664BEVB + FRDM33771BTPLEVB (resp. FRDM33772BTPLEVB) is used. */
//#define SPI         /* Define SPI if S32K144EVB-Q100 + FRDM33771BSPIEVB (resp. FRDM33772BSPIEVB) is used. */
//#define SPI_TRANSLT /* Define SPI_TRANSLT if S32K144EVB-Q100 + X-DEVKIT-TRANSLT + FRDM33771BSPIEVB (resp. FRDM33772BSPIEVB) is used. */

#ifndef MC33771
    #ifndef MC33772
        #error "Select used BCC device by defining MC33771 or MC33772."
    #endif
#endif

#ifndef TPL
    #ifndef SPI
        #ifndef SPI_TRANSLT
            #error "Select used type of communication by defining SPI or TPL."
        #endif
    #endif
#endif

/* Battery type. */
#define BCC_DEMO_BATTERY_TYPE   BCC_BATT_T

#define BCC_DEVICECNT     4U

/*******************************************************************************
 * Definitions (other)
 ******************************************************************************/

#define BCC_DEMO_CELLS(dev)  ((dev == BCC_DEVICE_MC33771) ? BCC_MAX_CELLS_MC33771 : BCC_MAX_CELLS_MC33772)

#define BCC_CELL_OV_FLT_NOEVENT   0x0000U   /* Value of CELL_OV_FLT register (event has not occurred) . */
#define BCC_CELL_UV_FLT_NOEVENT   0x0000U   /* Value of CELL_UV_FLT register (event has not occurred) . */
#define BCC_CB_OPEN_FLT_NOEVENT   0x0000U   /* Value of CB_OPEN_FLT register (event has not occurred) . */
#define BCC_CB_SHORT_FLT_NOEVENT  0x0000U   /* Value of CB_SHORT_FLT register (event has not occurred) . */
#define BCC_AN_OT_UT_FLT_NOEVENT  0x0000U   /* Value of AN_OT_UT_FLT register (event has not occurred) . */
#define BCC_GPIO_SHORT_NOEVENT    0x0000U   /* Value of GPIO_SHORT register (event has not occurred) . */
#define BCC_FAULT1_STATUS_NOEVENT 0x0000U   /* Value of FAULT1_STATUS register (event has not occurred) . */
#define BCC_FAULT2_STATUS_NOEVENT 0x0000U   /* Value of FAULT2_STATUS register (event has not occurred) . */
#define BCC_FAULT3_STATUS_NOEVENT 0x0000U   /* Value of FAULT3_STATUS register (event has not occurred) . */

/**
 * Prints formated string with register name, value and whether an event
 * occurred. It is intended for the status registers.
 *
 * @param regName Name of a status register.
 * @param regVal Value of a status register.
 * @param event "yes" when an event is signalized by register value.
 */
#define PRINT_STATUS_REG(regName, regVal, event) \
    (PRINTF("  | %s\t| 0x%02x %02x\t| %s\t\t     |\r\n", \
            regName, regVal >> 8, regVal & 0x00FF, event))

/**
 * Prints formated string with register name, value and whether an event
 * occurred. It is intended for the status registers.
 *
 * @param regName Name of a status register.
 * @param regVal Value of a status register.
 * @param defVal Value of a status register when has not occurred any event.
 */
#define PRINT_STATUS_REG_COMP(regName, regVal, defVal) \
    (PRINT_STATUS_REG(regName, regVal, (regVal != defVal) ? "yes" : "no"))


#define TX_MailBox     0x08
#define RX_MailBox     0x09

#define GuiCmdID       0x18800000

extern flexcan_msgbuff_t RecvBuff;
/*******************************************************************************
 * Function prototypes
 ******************************************************************************/

/*!
 * @brief This function loads default driver configuration depends on user application.
 */
void fillBccDriverConfiguration(bcc_drv_config_t* bccDrvConfig, uint8_t DeviceNodes);
/*!
 * @brief Waits for specified amount of microseconds.
 *
 * @param uscnt - Number of microseconds to wait.
 */
void Delayus(uint16_t uscnt);
/*!
 * @brief Waits for specified amount of milliseconds.
 *
 * @param mscnt - Number of milliseconds to wait.
 */
void Delayms(uint16_t mscnt);
/*!
 * @brief S32K1xx FlexCAN module configuration depends on user aplication
 *
 */
void FlexCANInit(void);
/*!
 * @brief S32K1xx Sends a CAN frame using the specified message buffer.
 *
 * @param   msg_id     ID of the message to transmit
 * @param   mb_data    Bytes of the FlexCAN message.
 *  *
 */
void SendCANData(uint32_t messageId, uint8_t * data);

/*!
 * @brief S32K1xx Receives a CAN frame using the specified message buffer
 * @param   data       The FlexCAN receive message buffer data, including ID and data.
 * @return  STATUS_SUCCESS if successful;
 *          STATUS_BUSY if a resource is busy
 */
status_t ReceiveCANData(flexcan_msgbuff_t *recvBuff);

#endif /* DEMO_CONFIG_H_ */
