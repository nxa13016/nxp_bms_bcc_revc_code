/*
 * uja1169.c
 *
 *  Created on: 2018��12��4��
 *      Author: nxa13016
 */


#include "uja1169.h"


/**
 * LPSPI State structure for each LPSPI instance.
 */
lpspi_state_t g_lpspiState[LPSPI_INSTANCE_COUNT];

status_t UJA_ConfigureLPSPI()
{
    lpspi_master_config_t ujaSpiSdkMasterConfig;
    status_t status;

    ujaSpiSdkMasterConfig.bitcount = 16U;
    ujaSpiSdkMasterConfig.callback = NULL;
    ujaSpiSdkMasterConfig.callbackParam = NULL;
    ujaSpiSdkMasterConfig.clkPhase = LPSPI_CLOCK_PHASE_2ND_EDGE;
    ujaSpiSdkMasterConfig.clkPolarity = LPSPI_ACTIVE_LOW;
    ujaSpiSdkMasterConfig.isPcsContinuous = false;
    ujaSpiSdkMasterConfig.lsbFirst= false;
    ujaSpiSdkMasterConfig.pcsPolarity = LPSPI_ACTIVE_LOW;
    ujaSpiSdkMasterConfig.rxDMAChannel = 2U;
    ujaSpiSdkMasterConfig.transferType = LPSPI_USING_INTERRUPTS;
    ujaSpiSdkMasterConfig.txDMAChannel = 2U;
    ujaSpiSdkMasterConfig.whichPcs = LPSPI_PCS0;

    ujaSpiSdkMasterConfig.bitsPerSec = 2000000;
    ujaSpiSdkMasterConfig.lpspiSrcClk = lpspiCom3_MasterConfig0.lpspiSrcClk;

    status = LPSPI_DRV_MasterInit(2,
                &(g_lpspiState[2]), &ujaSpiSdkMasterConfig);

    if (status != STATUS_SUCCESS)
    {
        return status;
    }



    return STATUS_SUCCESS;
}


void UJA_SPITransfer(uint8_t *Command, uint8_t *Response){

    uint16_t timeout = 5000;

	LPSPI_DRV_MasterTransferBlocking(LPSPICOM3, Command, Response, UJA_CMD_Length, timeout);


}


void UJA_SPICommand(uint8_t Address, uint8_t RW_Flag, uint8_t Write_data, uint8_t *Read_data){

	uint8_t Transferdata[2];
	uint8_t Receivedata[2];

	Transferdata[1] = (Address<<1)|(RW_Flag);
	Transferdata[0] = Write_data;

	UJA_SPITransfer(Transferdata, Receivedata);
	Delayus(10);
	UJA_SPITransfer(Transferdata, Receivedata);
	Delayus(10);
	Read_data[1] = Receivedata[1];
	Read_data[0] = Receivedata[0];
}

void UJA_FeedWD(void){

	uint8_t Transferdata[2];
	uint8_t Receivedata[2];

	Transferdata[1] = 0x00;
	Transferdata[0] = 0x8E;

	UJA_SPITransfer(Transferdata, Receivedata);
	Delayus(10);
}

void UJA_WD_WindowMode(void){
	uint8_t RxData[2];

	/***Config WD: Window Mode, 1024ms timeout**/
	UJA_SPICommand(0x00, WriteCmd,0x8E, RxData);
	UJA_SPICommand(0x00, ReadCmd, 0x00, RxData);

}

void UJA_enterNomalMode(void){
	uint8_t RxData[2];
	uint8_t RxData1[2];
    /*enter Normal Mode*/
	UJA_SPICommand(0x01, WriteCmd, 0x07, RxData);
	/*Turn on V2*/
	UJA_SPICommand(0x10, WriteCmd, 0x04, RxData1);
	/*Active CAN Transceiver*/
	UJA_SPICommand(0x20, WriteCmd, 0x01, RxData1);
	/*config data rate 500kb/s, option*/
	UJA_SPICommand(0x26, WriteCmd, 0x04, RxData);
	RxData[0]&=0x0F;

}

void UJA_WDStatus(void){
	uint8_t RxData[2];

	/*if not in normal mode, force uja enter normal mode*/
	UJA_SPICommand(0x01, ReadCmd, 0x00, RxData);
	if((RxData[0]&0x0F)!=0x07)
		UJA_SPICommand(0x01, WriteCmd, 0x07, RxData);
	/*if watchdog is in second half of the nominal period, feed watchdog*/
	UJA_SPICommand(0x05, ReadCmd, 0x00, RxData);
	if((RxData[0]&0x03)==0x02)
		UJA_FeedWD();
}

void UJA_Setting(void){
	uint8_t RxData[2];
	uint8_t WriteData[3] = {0x20, 0x04, 0x00};

	/*exit force normal mode */
    Delayms(40);
	UJA_SPICommand(0x73, WriteCmd, WriteData[0], RxData);
	UJA_SPICommand(0x74, WriteCmd, WriteData[1], RxData);
	WriteData[2] = sbc_get_factories_crc(WriteData);
	UJA_SPICommand(0x75, WriteCmd, WriteData[2], RxData);

}

uint8_t sbc_get_factories_crc(uint8_t* data)
{
    uint8_t crc = 0xFF, i,j;

    for(i = 0; i<= 1U; i++ )
    {
        data[i] = (uint8_t)(data[i] ^ crc);
        for(j = 0; j<=7U; j++)
        {
            if(data[i] >= 128U)
            {
                data[i] = (uint8_t)( (uint8_t)data[i] << 1U);
                data[i] = (uint8_t)(data[i] ^ 0x2FU);
            }
            else
            {
                data[i] = (uint8_t)(data[i] << 1U);
            }
        }
        crc = data[i];
    }
    crc = (uint8_t)(crc ^ 0xFFU);
    return crc;
}
