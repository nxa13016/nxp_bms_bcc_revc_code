/*
 * uja1169.h
 *
 *  Created on: 2018��12��4��
 *      Author: nxa13016
 */

#ifndef UJA1169_H_
#define UJA1169_H_

#include "lpspiCom1.h"
#include "demo_config.h"

#define UJA_SPI_CSB_INSTANCE      PTC
#define UJA_SPI_CSB_PIN           14

#define UJA_CMD_Length            2U

#define WriteCmd                 0U
#define ReadCmd                  1U

status_t UJA_ConfigureLPSPI();

void UJA_SPITransfer(uint8_t *Command, uint8_t *Response);

void UJA_SPICommand(uint8_t Address, uint8_t RW_Flag, uint8_t Write_data, uint8_t *Read_data);
void UJA_FeedWD(void);

void UJA_WD_WindowMode(void);
void UJA_enterNomalMode(void);
void UJA_WDStatus(void);

void UJA_Setting(void);

uint8_t sbc_get_factories_crc(uint8_t* data);
#endif /* UJA1169_H_ */
